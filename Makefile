javaSource = src/proj/*.java src/proj/*/*.java src/proj/*/*/*.java
listFile = $(shell find -name *.java)

all : clean compile

compile : $(javaSource)
	javac -d . -encoding UTF8 $(javaSource)

clean :
	rm -rf proj/

buildingdata : src/proj/util/BuildingWriter.java src/proj/map/building/BuildingData.java clean
	javac -d . -encoding UTF8 src/proj/util/BuildingWriter.java src/proj/map/building/BuildingData.java

runbuildingdata :
	java -cp . proj.util.BuildingWriter

runitemdata :
	java -cp . proj.util.ItemWriter

runplayerdata :
	java -cp . proj.util.PlayerWriter

runGUItest:
	java -cp . proj.util.GUITest

doc:	cleandoc
	javadoc -d doc/ $(javaSource) -encoding UTF8

cleandoc:
	rm -rf doc/

run:
	java -cp . proj.GameDemo

root:
	java -cp . proj.GameRoot

lineCount:
	wc $(javaSource)

mapEditor:
	java proj.util.MapEditor

makeJar :
	rm -rf jar
	mkdir jar jar/data jar/proj
	cp -r data jar
	cp -r proj jar
	jar cvfm ./game.jar manifest.txt -C jar .
 