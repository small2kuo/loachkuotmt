UltraEdit
5
記事本的功能太陽春，WORD 太耗資源了(程式大又佔寶貴的記憶體空間)，小作家又用不順手，想要找個可以處理文字檔和二進位檔，又方便又好用的文書處理程式嗎？用最新的 UltraEdit 就對了！更重要的是它支援 DBCS，中文編輯再也不成問題了！
根據磁碟的文字編輯和大檔案處理 - 支援超過 4GB 的檔案，即使是數兆字節的檔案也只佔用極少的記憶體
在所有搜索操作（尋找、替換、在檔案中尋找、在檔案中替換）中，支援多行尋 找和替換對話框
內建 FTP 客戶端以存取 FTP 伺服器，可設定多個帳戶，並自動登錄和保存。（僅 32 位）包括 SFTP（SSH2）支援
環境選擇器 - 提供預定義的或使用者建立的編輯「環境」，能記住 UltraEdit 的所有可停靠視窗、工具欄等的狀態，方便使用者使用。
1000 240
700 500
1200 750
2400 1700
3600 2100
data/software_icons/IDE/ultraedit1.png
data/software_icons/IDE/ultraedit2.png
data/software_icons/IDE/ultraedit3.png
data/software_icons/IDE/ultraedit4.png
data/software_icons/IDE/ultraedit5.png
