package ntu.csie.oop10spring;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import javax.imageio.ImageIO;

public class POOPanel extends JPanel{
  
	POOArenaAdv arena;
	public final static int pix=50;
  Image bgImage,bufferedImage,attackImage;
	Graphics buffered,bg;
	boolean first=true;
	int x,y;
	int attack_x=-1,attack_y=-1;
	int now_ID;
	
	public POOPanel(POOArenaAdv arena){
	  super();
	  this.arena=arena;  
		try{
		  attackImage=ImageIO.read(new File("Attack.jpg"));
		}catch(Exception e){}
	}
	   // protected POOPet[] pets;
    //protected POOCoordinate[] coordinate;
		
	public void paint(Graphics g){
	  if(first){
				bgImage=createImage(arena.width*pix+1,arena.height*pix+1);
		  bg=bgImage.getGraphics();
		  bg.setColor(Color.BLACK);
	    for(int i=0;i<=arena.width;i++)
		    bg.drawLine(i*pix,0,i*pix,arena.height*pix);
	  	for(int i=0;i<=arena.height;i++)
		    bg.drawLine(0,i*pix,arena.width*pix,i*pix);
		
		
		  bufferedImage=createImage(arena.width*pix+1,arena.height*pix+1);
      buffered=bufferedImage.getGraphics();
			first=false;
		}
	
    buffered.drawImage(bgImage,0,0,null);
		POOPetAdv tmp;
		
		if(arena.coordinate!=null){
		  for(int i=0;i<arena.coordinate.length;i++){
		    if(arena.coordinate[i]==null)continue;
			  tmp=(POOPetAdv)arena.pets[i];
				if(i==now_ID){
			    buffered.drawImage(tmp.im,x,y,null);
				  //buffered.drawImage(petInfor(tmp,null),x+40,y+40,null);
				}else{
			    buffered.drawImage(tmp.im,arena.coordinate[i].x*pix,arena.coordinate[i].y*pix,null);
				  buffered.drawImage(petInfor(tmp,arena.coordinate[i]),arena.coordinate[i].x*pix+40,arena.coordinate[i].y*pix+40,null);
				}
		  }
			if(attack_x!=-1)buffered.drawImage(attackImage,attack_x,attack_y,null);
		}
		g.drawImage(bufferedImage,0,0,null);
	}
	
	void move(int ID,POOCoordinate pass,POOCoordinate dest){
  	
		now_ID=ID;
		
		double step=10;
	  double dx=(dest.x-pass.x)/step;
		double dy=(dest.y-pass.y)/step;
    //System.out.println("ID:"+ID+"\t("+pass.x+","+pass.y+")\t("+dest.x+","+dest.y+")");
		//System.out.println("dx="+dx+"\t\tdy="+dy);
		for(int i=0;i<step;i++){
		  x=(int)((pass.x+dx*i)*pix);
		  y=(int)((pass.y+dy*i)*pix);
			//System.out.println("Coordinate:("+x/pix+","+y/pix+")");
			//System.out.println("Co:("+x+","+y+")");
			repaint();
			try{
			  Thread.sleep((int)(400/step));
			}catch(Exception e){}
		}
		arena.coordinate[ID]=dest;
		now_ID=-1;
	}
	
	Image petInfor(POOPetAdv pet,POOCoordinate co){
	  Image im=createImage(200,35);
		Graphics graphic=im.getGraphics();
		graphic.setColor(Color.orange);
		graphic.fillRect(0,0,200,35);
 	  graphic.setColor(Color.BLACK);
		graphic.drawString(pet+"  ("+co.x+","+co.y+")",5,20);
		//graphic.drawString();
		return im;
	}
	
	void attack(int from,int dest){
		if(from==dest)return;
		//System.out.println("from:"+from+"\t\tdest:"+dest);
		double step=15;
	  double dx=(arena.coordinate[dest].x-arena.coordinate[from].x)/step;
	  double dy=(arena.coordinate[dest].y-arena.coordinate[from].y)/step;
		
    //System.out.println("ID:"+ID+"\t("+pass.x+","+pass.y+")\t("+dest.x+","+dest.y+")");
		//System.out.println("dx="+dx+"\t\tdy="+dy);
		for(int i=0;i<step;i++){
		  attack_x=(int)((arena.coordinate[from].x+dx*i)*pix);
		  attack_y=(int)((arena.coordinate[from].y+dy*i)*pix);
			//System.out.println("Coordinate:("+x/pix+","+y/pix+")");
			//System.out.println("Co:("+x+","+y+")");
			repaint();
			try{
			  Thread.sleep((int)(500/step));
			}catch(Exception e){}
		}
		attack_x=-1;
	}
	
  public void update(Graphics g) {
    paint(g);
  }
}