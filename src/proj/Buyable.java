package proj;

import proj.player.*;

/**
 * 只要是可以買的東西，都可以實作 Buyable。
 */
public interface Buyable{

	/** 購買價格，可能與地段加成、物價有關，或者與購買者有關 */
	public int calcBuyingCost(Player p);

	/** 賣出獲利，可能與地段加成、物價有關，或者與賣出者有關 */
	public int calcSellingProfit(Player p);

	/** 只要是可以買賣的，都應該可以設定擁有者 */
	public void setOwner(Player p);

	/** 取得目前該物件的擁有者。 */
	public Player getOwner();

	/** 取得目前的物件價值 */
	public int getValue();

	//==========以下是配合UI的部分============
}
