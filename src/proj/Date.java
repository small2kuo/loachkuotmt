package proj;
import java.util.Calendar;
import java.io.Serializable;

/** 紀錄日期跟天氣.
 * 年:不限 月:1-12 日:1-當月日數
 * TODO 天氣!!!! */
public class Date implements Serializable{
   //INSTANCE VARIABLE
   private int year,month,day;
   private int weather;
   //INSTANCE METHOD
   
   //defaut to today
   public Date(){
     this(Calendar.getInstance().get(Calendar.YEAR),
           Calendar.getInstance().get(Calendar.MONTH)+1,
           Calendar.getInstance().get(Calendar.DATE));
   }
   
   public Date( int year, int month, int day ){
      setYear(year);
      setMonth(month);
      setDay(day);
   }

   public int getYear(){return year;}
   public int getMonth(){return month;}
   public int getDay(){return day;}
   /** IsLeapYear()： send year, and ask if it's a leap year
    * @param queryYear the year you want to ask
    * @return it's a leap year or not */
   public static boolean IsLeapYear(int queryYear){
      return queryYear%4==0 && ( queryYear%400==0 || queryYear%100!=0 );
   }
   /** MonthDay()：ask for the number of days in certain month
    * @param queryYear the year you want to ask
    * @param queryMonth the month you want to ask
    * @return the number of days in that month */
   public static int monthDay(int queryYear,int queryMonth){
      int[] monthDay = {31,28,31,30,31,30,31,31,30,31,30,31};
      //if it is Feburary, you should discuss the situation if it's in leap year
      if( queryMonth==2 )return monthDay[1]+(IsLeapYear(queryYear)?1:0); 
      else return monthDay[queryMonth-1];
   }
   public boolean setYear(int year){
      this.year = year;
      return true;
   }
   public boolean setMonth(int month){
      this.month = month;
      return 1<=this.month && this.month<=12;
   }
   public boolean setDay(int day){
      this.day = day;
      return ( 1<=this.day && this.day<=this.monthDay(this.year,this.month) ) ;
   }
   /** CompareTo()：compare two date which happened earlier.
    * @param obj the date which will be compared
    * @return if this date happened earlier, then return -1<br>
    *         if the two dates happened simutaneously, then return 0<br>
    *         if this date happened later, then return 1                          
    */
   public int compareTo(Date obj){
      if( year == obj.year ){//compare to month if the years are the same
         if( month == obj.month ){//compare to day if the months are the same
            if( day == obj.day )
               return 0;
            else
               return day < obj.day ? -1 : 1;
         }else
            return month < obj.month ? -1 : 1;
      }else
         return year < obj.year ? -1 : 1;
   }
   public boolean equals( Date obj ){
      return compareTo( obj )==0;
   }
   /** 日期加一個月 */
   public void addMonths(int addend){
      month = month-1 + addend;
      year += month/12;
      month = month%12+1;

      if( day > monthDay(year,month) )
         day = monthDay(year,month);
   }
   /** 日期減一個月 */
   public void subMonths(int subtrahend){
      month = month-1 -subtrahend;
      year += (month-11)/12;
      month = (month%12+12)%12 +1;

      if( day > monthDay(year,month) )
         day = monthDay(year,month);
   }
   /** 日期減一天 */
   public void subDays(int subtrahend){
      while( subtrahend-- > 0 )
         if( --day < 1 ){        //back to last month, if day<1 after substracted
            if( --month < 1 ){   //back to last year, if month<1 after substracted
               year--;
               month=12;
            }
            day = monthDay(year,month);
         }
   }
   /** 日期加一天 */
   public void addDays(int days){
	   while(days-- > 0)
		   if( ++day > monthDay(year, month)){
			   day = 1;
			   if(++month > 12){
				   ++year;
				   month = 1;
			   }
		   }
   }
}
