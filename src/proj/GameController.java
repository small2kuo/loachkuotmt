package proj;

import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.routine.*;
import java.io.*;

/** 遊戲的控制核心 */
public class GameController{
   private GameEnv env;
   private UserInterface ui;

   private EventCheckDead ecd = new EventCheckDead();

   public GameController( Map map , Player[] players){
      Date date=new Date();
      System.err.println("載入遊戲環境... [地圖: " + map + "] [遊戲人數: " + players.length + "]");
      env=new GameEnv(map,date);

      Street p_sts;
      Street[] sts = env.getMap().getStreets();
      System.err.println("sts = " + sts);
      for(int i=0;i<players.length;i++){
         System.err.println("載入玩家 [" + i + "]");

         p_sts = sts[ GameTool.rnd(0,sts.length) ];
         //把人丟到地圖上
         players[i].setBlock( p_sts.getBlockByIndex( GameTool.rnd(0,p_sts.getBlockSize()) ) );
         //在地圖上加上人
         players[i].getBlock().addStandable( players[i] );
         env.addPlayer(players[i]);
      }
      System.err.println("載入玩家完成!");
      GameTool.setGameEnv(env);//讓GameTool知道現在是哪個env

      ui=new GUIRoot(env); //把ui放到這裡 new 是因為想要先偷偷載入所有圖片~
      
      //===這裡是加一些會一直持續的Routine===
      env.addRoutine( new RoutineAddChestBox( 2 ) );
      //=====================================
      env.next(); //一開始的index是-1, 要讓他變成 0
   }
   public GameController(GameEnv loaded_env){
      env = loaded_env;
	  GameTool.setGameEnv(env);
      ui = new GUIRoot(env);
   }

   /** TODO 是不是開始遊戲前要有個前製作業?? */
   public void preStart(){
      //空的前製作業
   }

   public void start(){
      //preStart();

      int preRounds=0,nowRounds;
      Player p=null;
	  do{
         //getNowPlayer跟next要分開始因為讀檔的時候會多跑一步= =+
         p = env.getNowPlayer();

         //執行routines
         nowRounds=env.getRounds();
         if(nowRounds!=preRounds){
            Routine rs[]=env.getAllRoutine();
            Event event;
            for(int i=0;i<rs.length;i++)
               if((event=rs[i].execute( ui , env ))!=null){
                  event.act( ui , env );
                  ecd.act( ui , env );
               }
            preRounds=nowRounds;
         }
         //沒死才能跑呀~~
         if( p.getStatus().getState() != Status.DEAD ){
            int st = startRound( p );
            if( st == -1 )          //quitGame
               break;
            else if( st == -2 ){     //loadGame
				GameTool.setGameEnv( env ); //重新設定GameTool的env
				ui.getAnimator().fadeOut(300); //淡出 應該是變成黑色
               continue;
			}

			//短暫休息一下
			ui.getAnimator().gamePause( GameTool.GAME_DELAY_NORMAL );
			ui.getAnimator().fadeOut(300); //淡出 應該是變成黑色
         }
		 env.next(); //這邊要放到這裡
      }while(true);
   }

   /** 如果選擇結束遊戲則return -1. */
   public int startRound(Player p){
      //通知UI這是新的回合窩~~
      ui.updateNewRound();
	  ui.getAnimator().fadeIn(300); //淡入 移除遮罩
      //通知使用者這是新的回合窩~~
      p.newRound();

      //先看看使用者的狀態吧!
      Status status = p.getStatus();
      if( status.cantMove() ){
         ui.showMessage( p + "目前處於"+ status );
         return 0;
      }
      // 在擲骰子之前 可以對選單做的操作.
      Event event = null;
      while( !( (event=ui.getEvent()) instanceof EventThrowDices ) ){
         event.act( ui , env );
         ecd.act( ui , env );

         //如果選擇離開 就離開吧XDD.  (謎: 留下來,或者我跟你走...)
         if( event instanceof EventQuitGame && ((EventQuitGame)event).getReply() ){
            System.exit(0);//TODO 這個好暴力喔= =a
            return -1;
         }
         //如果選擇載入遊戲 就載入吧XDD.
         if( event instanceof EventLoadGame){
            GameEnv nextEnv = ((EventLoadGame)event).getNextEnv();
            if( nextEnv != null ){
               //TODO 關掉現在的ui, 改成新的ui. 要有轉場動畫XD -> "Loading..."
			   ui.turnOff();
               ui = new GUIRoot( env = nextEnv );
               return -2;
            }
         }
      }
      // 丟骰子>.^
      event.act( ui , env );

      // Walking and do event...
      Block nowBlock = null; 
      int blockIndex;
      Event walking=new EventPlayerMove(p);
      while( status.isWalking() ){
         walking.act( ui , env );   //這裡執行步數才會減一喔!
         nowBlock = p.getBlock();
         while( (event = nowBlock.passBy( env.getRounds(), p ))!=null ){
            event.act( ui , env );
            ecd.act( ui , env );

            if( status.cantMove() ){
               ui.showMessage( p + "目前處於"+ status );
               return 0;
            }

         }
      }
      return 0;
   }
}

