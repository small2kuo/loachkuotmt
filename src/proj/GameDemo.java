package proj;

import proj.ui.*;
import proj.event.*;
import proj.item.*;
import proj.map.*;
import proj.player.*;
import java.util.*;

/** Game爹某&gt;.^ */
public class GameDemo{
   static public void main(String[] args){
      if( args.length >= 1 && args[0].equals("r") ){
         main2();
         return;
      }
      PlayerData[] pds = GameTool.getPlayerDataList();

      ArrayList<Player> players = new ArrayList<Player>();
      
      Player p = new Player("小小郭", 200000, pds[0] ); 
      p.addItem( new ItemBuyLandCard(p) );
      p.addItem( new ItemChangeBuildingCard(p) );
      p.addItem( new ItemChangeLandCard(p) );
      p.addItem( new ItemDreamCard(p) );
      p.addItem( new ItemEvilCard(p) );
      p.addItem( new ItemFreeCard(p) );
      p.addItem( new ItemHouseBreakCard(p) );
      p.addItem( new ItemHousingCard(p) );
      p.addItem( new ItemMineCard(p) );
      p.addItem( new ItemMonsterCard(p) );
      p.addItem( new ItemPoorCard(p) );
      p.addItem( new ItemRichCard(p) );
      p.addItem( new ItemSleepCard(p) );
      p.addItem( new ItemStopCard(p) );
      p.addItem( new ItemSuperMineCard(p) );
      p.addItem( new ItemTaxCard(p) );
      p.addItem( new ItemTurnCard(p) );
      players.add( p );

      p = new Player("泥鰍" , 200000 , pds[1] );
      p.addItem( new ItemBuyLandCard(p) );
      p.addItem( new ItemChangeBuildingCard(p) );
      p.addItem( new ItemChangeLandCard(p) );
      p.addItem( new ItemDreamCard(p) );
      p.addItem( new ItemEvilCard(p) );
      p.addItem( new ItemFreeCard(p) );
      p.addItem( new ItemHouseBreakCard(p) );
      p.addItem( new ItemHousingCard(p) );
      p.addItem( new ItemMineCard(p) );
      p.addItem( new ItemMonsterCard(p) );
      p.addItem( new ItemPoorCard(p) );
      p.addItem( new ItemRichCard(p) );
      p.addItem( new ItemSleepCard(p) );
      p.addItem( new ItemStopCard(p) );
      p.addItem( new ItemSuperMineCard(p) );
      p.addItem( new ItemTaxCard(p) );
      p.addItem( new ItemTurnCard(p) );
      players.add(p);

      p = new Player("好弱卡嗯" , 200000 , pds[2] );
      p.addItem( new ItemBuyLandCard(p) );
      p.addItem( new ItemChangeBuildingCard(p) );
      p.addItem( new ItemChangeLandCard(p) );
      p.addItem( new ItemDreamCard(p) );
      p.addItem( new ItemEvilCard(p) );
      p.addItem( new ItemFreeCard(p) );
      p.addItem( new ItemHouseBreakCard(p) );
      p.addItem( new ItemHousingCard(p) );
      p.addItem( new ItemMineCard(p) );
      p.addItem( new ItemMonsterCard(p) );
      p.addItem( new ItemPoorCard(p) );
      p.addItem( new ItemRichCard(p) );
      p.addItem( new ItemSleepCard(p) );
      p.addItem( new ItemStopCard(p) );
      p.addItem( new ItemSuperMineCard(p) );
      p.addItem( new ItemTaxCard(p) );
      p.addItem( new ItemTurnCard(p) );
      players.add(p);

      GameController gc;
      proj.map.Map map = GameTool.getMap( GameTool.getMapInfoList()[0] );
      System.err.println("map: " + map);
      gc = new GameController(map, players.toArray(new Player[0]));
      System.err.println("遊戲載入完成...... 遊戲開始!");
      gc.start();
   }
   static public void main2(){
      GameRoot gr = new GameRoot();
      gr.start();
   }
}
