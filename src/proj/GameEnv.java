package proj;

import proj.routine.*;
import proj.map.*;
import proj.player.*;
import java.util.ArrayList;
import java.io.*;

public class GameEnv implements Serializable{

   private Date date;
   private Map gameMap;
   private ArrayList<Player> playerList;
   private ArrayList<Routine> routineList;
   private int now;
   private int rounds;//紀錄現在第幾回合

   public GameEnv(Map gameMap, Date date){
      this.date=date;
      this.gameMap=gameMap;
      playerList=new ArrayList<Player>(0);
      routineList=new ArrayList<Routine>(0);
      rounds=1;
      now = -1; //手癢移到Constructor這邊XD
   }

   public Map getMap(){
      return gameMap;
   }

   public Date getDate(){
      return date;
   }
   //=========== Routine相關 ==========
   /** 取得所有 Routine */
   public Routine[] getAllRoutine(){
      Routine r[]=new Routine[0];
      return routineList.toArray(r);
   }
   /** 新增(註冊) Routine */
   public void addRoutine(Routine r){
      if(r==null)return;
      routineList.add(r);
   }
   /** 移除 Routine */
   public void removeRoutine(Routine r){
      routineList.remove(r);
   }
   //========== Player相關 ==========
   public void addPlayer(Player p){
      if(p==null)return;
      playerList.add(p);
	  System.err.println("加入玩家: " + p);
   }
   public Player[] getAllPlayers(){
      return playerList.toArray( new Player[0] );
   }
   //========== Round相關 ==========
   public void next(){
      now++;
      if(now >= playerList.size()){
         now = 0;
         rounds++;
		 date.addDays(1);
      }
   }
   public Player getNowPlayer(){
      if(now < 0)
         return null;
      return playerList.get(now);
   }
   /**顯示現在第幾回合*/
   public int getRounds(){
      return rounds;
   }
}
