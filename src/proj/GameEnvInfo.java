package proj;

import proj.routine.*;
import proj.map.*;
import proj.player.*;
import java.util.ArrayList;
import java.io.Serializable;

/** 用來紀錄一個遊戲的簡單資訊.
 * 在存讀檔選單的時候選擇.<p>
 * 
 * 預設檔案路徑是{@literal data/record/<GameEnvInfo.num>}.info<br>
 * 對應的GameEnv檔案路徑是{@literal data/record/<GameEnvInfo.num>}.dat<br>
 * 預設的GameEnvInfo name是GameEnv的hashCode;<br>
 * @see GameEnv */
public class GameEnvInfo implements Serializable{
   /** GameEnv名稱. */
   public int num;
   /** Map相關資料. */
   public MapInfo mapInfo;
   /** Player相關資料. */
   public PlayerInfo[] pInfo;
   /** 存檔時間. */
   public long saveTime;
   public GameEnvInfo( GameEnv env , int num ){
      this.num = num;
      this.mapInfo = new MapInfo( env.getMap() );
      
      Player[] ps = env.getAllPlayers();
      pInfo = new PlayerInfo[ ps.length ];
      for( int i=0; i<ps.length; ++i )
         pInfo[i] = new PlayerInfo( ps[i] );

      saveTime = System.currentTimeMillis();
   }
   public String toString(){ return String.format("%03d",num); }
}
