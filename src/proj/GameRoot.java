package proj;

import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.routine.*;
import java.io.*;
/** 主畫面選單: 開新遊戲選單,載入遊戲選單,結束遊戲. */
public class GameRoot{
   private GUIGameRoot ui;
   private GameController gamec;

   public GameRoot(){
      ui = new GUIGameRoot();
   }
   /** 開始遊戲囉. */
   public void start(){
      ui.setVisible(true);
      gamec = null;
      while(true){//這裡是主選單~~
         String[] menuOpts = { "開心遊戲" , "載入遊戲" , "結束遊戲" };
		 String[] menuImgSrc = { "data/sysimg/newgame.png", "data/sysimg/contgame.png", "data/sysimg/exitgame.png" };
         switch( ui.startMenu( menuOpts, menuImgSrc ) ){
            case 0:
               gamec = newGameMenu();
               break;
            case 1:
               gamec = loadGameMenu();
               break;
            case 2:
               quitGame();
               break;
         }
         if( gamec!=null )
            gamec.start();
      }
   }
   /** 開新遊戲.
    * <pre>
    *    getMapInfoList:    {@literal data/map/<mapName>.info}
    *    getPlayerDatalist: {@literal data/player/<playerDataName>.dat}
    *    getMap( MapInfo ): {@literal data/map/<mapName>.dat}
    * </pre>
    * @see GameRootUI#getNewGameListOption() */
   private GameController newGameMenu(){
      int DEFAULT_CASH = 100000;  //TODO 可選擇Cash or 勝利條件??
      MapInfo[] maps = GameTool.getMapInfoList();
      PlayerData[] ps = GameTool.getPlayerDataList();

      Object[] cmd = ui.getNewGameListOption( maps , ps );

      if( ((Integer)cmd[0]).intValue() == 1 )
         return null;

      MapInfo      mi   = (MapInfo)cmd[1]; 
      PlayerData[] pi   = (PlayerData[])cmd[2]; 
      String[]     name = (String[])cmd[3]; 

      Map choMap = GameTool.getMap( mi );
      Player[] choPs = new Player[ pi.length ];
      for( int i=0; i<choPs.length ; ++i )
         choPs[i] = new Player( name[i] , DEFAULT_CASH , pi[i] );
      ui.setVisible(false);
      return new GameController( choMap , choPs );
   }
   /** 載入遊戲.
    * <pre>
    *    getGameEnvInfoList:       {@literal data/record/<gameEnv.name>.info}
    *    getGameEnv(GameInfoList): {@literal data/record/<gameEnv.name>.dat}
    * </pre>
    * */
   private GameController loadGameMenu(){
      GameEnvInfo[] geis = GameTool.getGameEnvInfoList( );
      int cho = ui.showLoadGameList( geis );
      ui.setVisible(false);
      return new GameController( GameTool.getGameEnv( geis[cho] ) );
   }
   /** 結束遊戲 */
   private void quitGame(){
      System.exit(0);   
   }
   public static void main(String[] args){
      GameRoot gr = new GameRoot();
      gr.start();
   }
}
