package proj;

import proj.player.*;
import proj.item.*;
import proj.map.*;
import proj.map.building.*;
import proj.ui.*;

import java.io.*;

import java.util.*;
/** 提供遊戲的一些工具. */
public class GameTool{
   /** GameEnv儲存目錄({@value #DIR_GameEnv}) */
   static final public String DIR_GameEnv = "data/record/";
   /** Map/MapInfo儲存目錄({@value #DIR_Map}) */
   static final public String DIR_Map     = "data/map/";
   /** PlayerData儲存目錄({@value #DIR_Player}) */
   static final public String DIR_Player  = "data/player/";
   /** ItemData儲存目錄({@value #DIR_Item}) */
   static final public String DIR_Item    = "data/item/";

   static final public String NOT_FOUND_IMG = "data/sysimg/404_not_found.jpg";

   static final public int RECORD_NUM = 10;

   static Random RandomGenerator = new Random();
   static private GameEnv gameEnv;
   static void setGameEnv(GameEnv gameEnv){
      GameTool.gameEnv = gameEnv;
   }
   /** 會呼叫 owner = null 的情形. */
   static public Building getNewBuildingByType(BuildingType bt, Land land){
      return getNewBuildingByType(bt, land, null);
   }
   /** 這個部分會處理讀取檔案的資訊，把它合進一個 building 的某種 subclass */
   static public Building getNewBuildingByType(BuildingType bt, Land land, Player owner){

      Building b = null;
      System.err.println(bt.className + "/" + bt.dataName);
      BuildingData b_data = getBuildingData(bt);
      if(b_data == null){
         System.err.println("[GameTool] !!ERROR!!: 讀取房子資料失敗!!!");
      }

      if("BuildingSmall".equals(bt.className)){
         //一般的房子
         b = new BuildingSmall(b_data, (LandEmpty) land);

      }else if("BuildingLarge".equals(bt.className)){
         //大型的房子 (這邊應該是各式各樣的 className,只不過還沒做)

      }else if("Hospital".equals(bt.className)){
         b = new Hospital( land, b_data);
      }else if("Park".equals(bt.className)){
         b = new Park( land, b_data);
      }else if("Prison".equals(bt.className)){
         b = new Prison( land, b_data);
      }else{
         //例外狀況
      }
      return b;
   }

   static private HashMap<BuildingType, BuildingData> mapBT2BD
      = new HashMap<BuildingType, BuildingData>();
   /** 取得 BuildingData, 以 HashMap 避免重複讀取 */
   static private BuildingData getBuildingData(BuildingType bt){
      BuildingData b_data = mapBT2BD.get(bt);
      if(b_data != null)
         return b_data;
      String fileName = "data/building/" + bt.dataName + ".dat";
      if(gameEnv != null){
         String mapName = gameEnv.getMap().getName();
         fileName = "data/map_" + mapName + "/building/" + bt.dataName + ".dat";
      }   
      b_data = (BuildingData)getObject( fileName );
      if(b_data == null){ //Try again, this should not happen (通常是因為找不到地圖檔)
         fileName = "data/building/" + bt.dataName + ".dat";
         b_data = (BuildingData)getObject(fileName);
      }

      if(b_data != null)
         mapBT2BD.put(bt, b_data);
      return b_data;
   }

   /** 提供介於start以及end之間的亂數(包含start但是不包含end) */
   static public int rnd(int start, int end){
      if(start >= end) return start;
      return RandomGenerator.nextInt(end-start) + start;
   }

   /** 提供購買房子所需要的花費 */
   static public int calcBuildingBuyingCost(BuildingType bt, Player owner){
      BuildingData b_data = getBuildingData(bt);
      //TODO 計算購買房子所需要的花費:P
      return b_data.upgradeCost[1];
   }

   /** 取得所有的Block. */
   static public Block[] getAllBlocks( GameEnv env ){
      Street[] streets = env.getMap().getStreets();
      ArrayList<Block> blks = new ArrayList<Block>(0);
      for( int i=0;i<streets.length;++i )
         for( int j=0 ; j<streets[i].getBlockSize() ; ++j )
            blks.add( streets[i].getBlockByIndex(j) );
      return blks.toArray( new Block[0] );
   }
   /** 取得所有 "是c的Instance的建築物" 所在的Block. */
   static public Block[] getAllBlockOfBuilding( GameEnv env , Class c ){
      Block[] all_b = getAllBlocks( env );
      ArrayList<Block> blks = new ArrayList<Block>(0);
      for( int i=0; i<all_b.length ; ++i)
         if( c.isInstance( all_b[i].getLand().getBuilding() ) ) //如果=null應該是false
            blks.add( all_b[i] ); 
      return blks.toArray( new Block[0] );
   }
   /** 取得所有ItemBuyable的ItemClassName. */
   static public String[] getAllItemBuyableName(){
      String dirName = DIR_Item;
      ArrayList<String> iName = new ArrayList<String>(0); 
      //取得檔案列表
      String[] files = ( new java.io.File( dirName ) ).list();
      //篩選檔案
      for(int i=0;i<files.length;++i)
         if( java.util.regex.Pattern.matches(".*Card[.]dat", files[i]) ){
            iName.add( files[i].replaceAll("[.]dat$","") );
         }
      return iName.toArray( new String[0] );
   }
   //==================== GameEnv/GameEnvInfo相關 ====================
   /** 讀入GameEnvInfo的list, 以便存檔讀檔的時候顯示簡單資訊.
    * 從目錄({@value #DIR_GameEnv})讀入所有.info的GameEnvInfo檔.
    * @see GameEnv
    * @see GameEnvInfo */
   static public GameEnvInfo[] getGameEnvInfoList( ){
      return getGameEnvInfoList( DIR_GameEnv );
   }
   /** @deprecated 理論上應該要用預設路徑,請使用{@link #getGameEnvInfoList()}. */
   @Deprecated
      static public GameEnvInfo[] getGameEnvInfoList( String dirName ){
         GameEnvInfo[] geis = new GameEnvInfo[ RECORD_NUM ];
         //取得檔案列表
         String[] files = (new java.io.File(dirName)).list();
         int idx = 0;
         //篩選檔案
         for(int i=0;i<files.length;++i)
            if( java.util.regex.Pattern.matches(".*[.]info", files[i]) ){
               try{
                  idx = Integer.parseInt( files[i].replaceAll("[.]info$","") );
               }catch(Exception e){ System.err.println(e); }
               geis[idx] = (GameEnvInfo)getObject( dirName + files[i]) ;
            }
         return geis;
      }
   /** 傳入GameEnvInfo傳回其GameEnv. */
   static public GameEnv getGameEnv( GameEnvInfo env ){
      return (GameEnv)getObject( DIR_GameEnv + env + ".sav" );
   }
   //==================== Map/MapInfo相關 ====================
   /** 讀入Map的list, 以便顯示簡單資訊.
    * 從目錄({@value #DIR_Map})下讀入所有.info的MapInfo檔.
    * @see Map
    * @see MapInfo */
   static public MapInfo[] getMapInfoList(){
      return getMapInfoList( DIR_Map );
   }
   /** @deprecated 理論上應該要用預設路徑,請使用{@link #getMapInfoList()}. */
   static public MapInfo[] getMapInfoList( String dirName ){
      ArrayList<MapInfo> mis = new ArrayList<MapInfo>(0);
      //取得檔案列表
      String[] files = ( new java.io.File( dirName ) ).list();
      //篩選檔案
      for(int i=0;i<files.length;++i)
         if( java.util.regex.Pattern.matches(".*[.]info", files[i]) )
            mis.add( (MapInfo)getObject( dirName + files[i]) );
      return mis.toArray( new MapInfo[0] );
   }
   /** 給定MapInfo, 傳回其完整Map.
    * 其實是從路徑{@value #DIR_Map}{@literal <MapInfo.name>(=<Map.name>)}.dat 讀資料 */
   static public proj.map.Map getMap( MapInfo mi ){
      return getMap( DIR_Map+mi+".dat" );
   }
   /** @deprecated 理論上應該要用預設路徑,請使用{@link #getMap(MapInfo)}. */
   @Deprecated
      static public proj.map.Map getMap( String fileName ){
         return (proj.map.Map)getObject( fileName );
      } 

   //==================== 動態暴力取得一些資訊 ======================
   static public GameTool defaultGameTool = new GameTool();
   public ArrayList<LandEmpty> getAllLandsOwnedByPlayer(Player p){
	   ArrayList<LandEmpty> allLands = new ArrayList<LandEmpty>();
	   Street[] st = gameEnv.getMap().getStreets();
	   for(int i = 0; i < st.length; i++){
		   for(int j = 0; j < st[i].getBlockSize(); j++){
			   Block b = st[i].getBlockByIndex(j);
			   if(b.getLand() instanceof LandEmpty && p.equals(((LandEmpty) b.getLand()).getOwner())){
				   allLands.add((LandEmpty) b.getLand());
			   }
		   }
	   }
	   return allLands;
   }
   //==================== Player/PlayerInfo相關 ====================
   /** 讀入PlayerData的list, 以便顯示簡單資訊.
    * 從目錄({@value #DIR_Player})下讀入所有.dat的PlayerData檔.
    * @see PlayerData */
   static public PlayerData[] getPlayerDataList(){
      return getPlayerDataList( DIR_Player );
   }
   /** @deprecated 理論上應該要用預設路徑,請使用{@link #getPlayerDataList()}. */ 
   @Deprecated
      static public PlayerData[] getPlayerDataList( String dirName ){
         ArrayList<PlayerData> pds = new ArrayList<PlayerData>(0);
         //取得檔案列表
         String[] files = ( new java.io.File( dirName ) ).list();
         //篩選檔案
         for(int i=0;i<files.length;++i)
            if( java.util.regex.Pattern.matches(".*[.]dat", files[i]) )
               pds.add( (PlayerData)getObject( dirName + files[i]) );
         return pds.toArray( new PlayerData[0] );
      }
   /** 載入玩家靜態資料.
    * 預設路徑是{@value #DIR_Player}{@literal <playerData.name>}.dat 
    static public PlayerData getPlayerData( PlayerData pd ){
    return getPlayerData( pd.name );
    }
   /** @deprecated 理論上應該要用預設路徑,請使用{@link #getPlayerData(PlayerData)}. 
   @Deprecated
   static public PlayerData getPlayerData( String fileName ){
   return getPlayerData( DIR_Player , fileName );
   }
   /** @deprecated 理論上應該要用預設路徑,請使用{@link #getPlayerData(PlayerData)}. 
   @Deprecated
   static public PlayerData getPlayerData( String dirName , String fileName){
   return (PlayerData)getObject( dirName + fileName + ".dat" );
   }*/
   //==================== 毫無相關XD ====================
   /** 輸入Item的ClassName 輸出其對應的Data. */
   static public ItemData getItemData( String className ){
      String fileName = DIR_Item + className + ".dat";
      ItemData data = (ItemData)getObject( fileName );
      return data;
   }

   //==================== 存檔相關 ====================
   /** 存檔.
    * 要存一個全部的GameEnv檔案(.sav) 還有 一個有部份資訊的檔案(.info).
    * 預設是把檔案存在data/record/目錄底下.
    * <pre>
    * GameEnv     :{@literal data/record/<gameEnv.name>.dat}
    * GameEnvInfo :{@literal data/record/<gameEnv.name>.info}
    * </pre>
    * TODO 看這個method要放在哪裡@@? */
   static public void saveGame( GameEnv env , int num ){
      saveGame( env , GameTool.DIR_GameEnv , num );
   }
   /** @deprecated 理論上要用預設路徑,請使用{@link #saveGame(GameEnv,int)}. */
   @Deprecated
      static public void saveGame( GameEnv env , String dirName , int num ){
         ObjectOutputStream OOS = null;
         try{
            OOS = new ObjectOutputStream( new FileOutputStream(
                     String.format( "%s%03d.sav" , dirName , num ) ) );
            OOS.writeObject( env );
            OOS.close();

            OOS = new ObjectOutputStream( new FileOutputStream(
                     String.format( "%s%03d.info" , dirName , num ) ) ); 
            OOS.writeObject( new GameEnvInfo(env , num) );
            OOS.close();
         }catch(Exception e){System.err.println(e);}
      }
   //==================== 毫無相關XD ====================
   /** 載入一個物件. 要轉型要在invoke完再轉~ */
   static public Object getObject( String fileName ){
      Object obj = null;
      try{
         ObjectInputStream OIS =new ObjectInputStream(new FileInputStream(fileName)); 
         obj = OIS.readObject();
         System.err.println("[GameTool]: 成功讀取檔案 " + fileName);
         OIS.close();
      }catch(Exception e){ System.err.println(e); }
      return obj;
   }

   //==================== 遊戲物件更新相關 ====================
   static private ArrayList<MapUpdateObserver> mapUpdateObservers = new ArrayList<MapUpdateObserver>();
   /** 當地圖上有東西被更動的時候, 就應該要呼叫這個通知所有地圖觀察者. */
   static public void notifyAllMapObservers(){
      for(int i = 0; i < mapUpdateObservers.size(); i++)
         mapUpdateObservers.get(i).updateViewMap();
   }
   /** 一開始這些觀察者要向 GameTool 註冊.
    * @see GUIMapPanel
    */
   static public void addMapUpdateObserver(MapUpdateObserver muo){
      if(!mapUpdateObservers.contains(muo))
         mapUpdateObservers.add(muo);
   }
   /** 也可以不再更新 */
   static public void removeMapUpdateObserver(MapUpdateObserver muo){
      mapUpdateObservers.remove(muo);
   }

   //=================== 時間控制/UI 相關 =========================
   /* 這邊的東西原則上是配合 UserInterfaceAnimator */
   /** 短暫的等待，例如建築物蓋起來之類 */
   static public int GAME_DELAY_SHORT = 400;
   /** 普通的等待，例如玩家回合結束 */
   static public int GAME_DELAY_NORMAL = 800;
   /** 較長的等待，例如...? */
   static public int GAME_DELAY_LONG = 1500;
}
