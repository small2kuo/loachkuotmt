package proj.event;

import proj.ui.*;
import proj.player.*;
import proj.item.*;
import proj.*;
import proj.map.*;

/** 增加某個Player的Item的Event. */
public class EventAddPlayerItem extends EventSingleTarget{
   private Item item;
   
   public EventAddPlayerItem(Player p,Item item){
     super(p);
     this.item=item;
   }
   
   public void act( UserInterface ui , GameEnv env){
     super.getUser().addItem(item);
   }
}


