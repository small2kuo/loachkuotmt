package proj.event;

import proj.*;
import proj.ui.*;
import proj.routine.*;

public class EventAddRoutine extends Event{
   Routine rt;
   public EventAddRoutine( Routine rt ){
      this.rt = rt;
   }
   public void act( UserInterface ui , GameEnv env ){
      env.addRoutine( rt );
   }
}
