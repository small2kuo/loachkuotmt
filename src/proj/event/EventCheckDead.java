package proj.event;

import proj.*;
import proj.ui.*;
import proj.player.*;
import proj.map.*;
import proj.routine.*;
import java.io.*;

public class EventCheckDead{
   public void act( UserInterface ui , GameEnv env ){
      Player[] p = env.getAllPlayers();
      for( int i=0 ; i<p.length ; ++i )
         //如果沒死才要檢查
         if( p[i].getStatus().getState() != Status.DEAD ){
            //如果沒錢才算死XD
            if( p[i].getCash() < 0 ){
               ui.showMessage( p[i]+"表示: ◢▆▅▄▃崩╰(〒皿〒)╯潰▃▄▅▇◣" );
               p[i].getStatus().setState( Status.DEAD );
               clear( ui , env , p[i] );
            }
         }
   }
   private void clear( UserInterface ui , GameEnv env , Player p ){
      Block[] blks = GameTool.getAllBlocks( env );
      LandEmpty le;
      for( int i=0; i<blks.length ; ++i )
         if( blks[i].getLand() instanceof LandEmpty){
            le = (LandEmpty)blks[i].getLand();
            if( le.getOwner() == p )
               le.setOwner( null );
         }
   }
}
