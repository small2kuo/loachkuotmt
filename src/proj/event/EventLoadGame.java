package proj.event;

import proj.*;
import proj.ui.*;
import proj.player.*;
import proj.map.*;
import proj.routine.*;
import java.io.*;

/** 在遊戲進行中決定要不要載入遊戲.
 * TODO [Q] 這件事情要怎麼辦到呢囧a.*/
public class EventLoadGame extends Event{
   private GameEnv nextEnv = null;
   public EventLoadGame(){}
   public void act( UserInterface ui , GameEnv env ){
      GameEnvInfo[] geis = GameTool.getGameEnvInfoList( );
      int cho = 0;
      if( (cho = ui.queryLoadGame( geis ))==-1 )//取消
         return;
      else{
         if( ui.queryYesNo("確定載入遊戲?") )
            this.nextEnv = GameTool.getGameEnv( geis[cho] );
      }
   }
   public GameEnv getNextEnv(){
      return this.nextEnv;
   }
}
