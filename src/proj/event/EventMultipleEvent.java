package proj.event;

import proj.ui.*;
import proj.player.*;
import proj.item.*;
import proj.*;

/** 把多個事件包在一起 */
public class EventMultipleEvent extends Event{
   Event[] eve;
   
   public EventMultipleEvent(Event... eve){
     this.eve=eve;
   }
   public  void act( UserInterface ui , GameEnv env){
     for(int i=0;i<eve.length;i++)
       eve[i].act(ui,env);
   }
}
