package proj.event;

import proj.map.*;
import proj.player.*;
import proj.*;
import proj.ui.*;

/** 錢不夠的事件.*/
public class EventNotEnoughMoney extends EventSingleTarget{
   private String msg;
   /** Constructor: 沒錢買東西. */
   public EventNotEnoughMoney(Player user,String msg){
      super(user);
      this.msg = msg;
   }
   /** 顯示沒錢的訊息. */
   public void act( UserInterface ui , GameEnv env ){
      ui.showMessage(this.msg); 
   }
}
