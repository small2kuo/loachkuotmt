package proj.event;

import proj.ui.*;
import proj.player.*;
import proj.item.*;
import proj.*;
import proj.map.*;

/** 在某塊地上放一個Item的Event. */
public class EventPutItem extends Event{
   private Block blk;
   private ItemStandable item;
   public EventPutItem( ItemStandable item , Block blk ){
     this.item=item;
     this.blk = blk;
   }
   public void act( UserInterface ui , GameEnv env){
     blk.addStandable( item );
   }
}


