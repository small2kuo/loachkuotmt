package proj.event;

import proj.*;
import proj.ui.*;
import proj.player.*;
import proj.map.*;
import proj.routine.*;
import java.io.*;

/** 在遊戲進行中決定要不要結束遊戲.
 * 結束遊戲. */
public class EventQuitGame extends Event{
   private boolean reply;
   public EventQuitGame(){}
   public void act( UserInterface ui , GameEnv env ){
      this.reply = ui.queryYesNo("確定結束遊戲?\n(小提醒:記得要存檔喔>.^)");
   }
   public boolean getReply(){
      return this.reply;
   }
}
