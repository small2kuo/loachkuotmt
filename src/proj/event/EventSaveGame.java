package proj.event;

import proj.*;
import proj.ui.*;
import proj.player.*;
import proj.map.*;
import proj.routine.*;
import java.io.*;

/** 在遊戲進行中決定要不要儲存遊戲.*/
public class EventSaveGame extends Event{
   public EventSaveGame(){}
   public void act( UserInterface ui , GameEnv env ){
      GameEnvInfo[] geis = GameTool.getGameEnvInfoList( );
      int cho = 0;
      if( (cho = ui.querySaveGame( geis ))==-1 )//取消
         return;
      else{
         if( ui.queryYesNo("確定儲存遊戲?") )
            GameTool.saveGame( env , cho );
      }
   }
}
