package proj.event;

import proj.player.*;
import proj.*;
import proj.ui.*;
import proj.item.*;

/** 這是一個Buyable的Item產生的Event原型
 * 一個Buyable的Item在使用者戳他的時候 照理來說會傳回其use()的Event
 * 然後GameController會去執行這個Event.
 * */
public class EventCantUse extends Event{
   private String msg;
   public EventCantUse( String msg ){
      this.msg = msg;
   }
   public EventCantUse(){
      this("這個道具無法使用喔.");
   }
   public void act(UserInterface ui, GameEnv env){
      ui.showMessage( msg );
   }
}
