package proj.item;

import proj.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.map.building.*;
import proj.ui.*;

/** 送醫院的Event. */
public class EventHospital extends EventSingleTarget{
   private int day;
   private String str;
   public EventHospital( Player p , int day , String str ){
      super( p );
      this.day = day;
      this.str = str;
   }
   public void act( UserInterface ui , GameEnv env ){
      Class cH = null;
      try{
         cH = Class.forName( "proj.map.building.Hospital" );
      }catch(ClassNotFoundException e){System.err.println(e);}
      
      Block[] blks = GameTool.getAllBlockOfBuilding( env , cH ); 
      Player p = getUser();
      Block hos = blks[GameTool.rnd(0,blks.length)];
      //設定使用者狀態
      p.getStatus().setState( Status.HOSPITAL , day );
      //拔到使用者 放下使用者
      p.getBlock().removeStandable( p );
      hos.addStandable( p );
      p.setBlock( hos );

      ui.showMessage( str );
   }
}
