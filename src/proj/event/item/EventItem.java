package proj.event;

import proj.player.*;
import proj.*;
import proj.ui.*;
import proj.item.*;

/** 這是一個Buyable的Item產生的Event原型
 * 一個Buyable的Item在使用者在遊戲進行中選道具使用時 傳回其use()的Event
 * 然後GameController會去執行這個Event.
 * 
 * 可以使用remove來把Item從Player的道具欄裡面刪掉
 * */
public class EventItem extends Event{
  protected Player user;
  protected Item toBeUsed;
  public EventItem(Player user,Item toBeUsed){
    this.user = user;
    this.toBeUsed = toBeUsed;
  }
  public void remove(){
    user.removeItem(toBeUsed);
  }
  public void act(UserInterface ui, GameEnv env){
    remove();
  }
}
