package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/** 放置物品的Event.
 * 包含詢問要放哪裡, 刪除使用者物品, 新增block上的物品. */
class EventPlaceItem extends EventItem{
   private ItemStandable item;
   public EventPlaceItem( Player p , ItemStandable item ,Item toBeUsed){
      super( p ,toBeUsed);
      this.item = item;
   }
   public void act( UserInterface ui , GameEnv env ){
      Block blk = ui.queryChooseBlock( item );
      if( blk == null )//選擇取消?
         return;
      super.remove();//移除道具從某人的道具列
      blk.addStandable( item );
      item.setBlock( blk );
   }
}
