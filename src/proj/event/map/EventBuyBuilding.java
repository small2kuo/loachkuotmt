package proj.event;

import proj.item.*;
import proj.map.*;
import proj.map.building.*;
import proj.player.*;
import proj.*;
import proj.ui.*;
import java.util.*;

/** 在地上建房子的事件.
 * call from: Block.passBy(p) -> Land.passBy(p){owner}. */
public class EventBuyBuilding extends EventSingleTarget{
   private LandEmpty land;
   public EventBuyBuilding(Player user,LandEmpty land){
      super(user);
      this.land = land;
   }
   public EventBuyBuilding(LandEmpty land){
      this( land.getOwner() , land );
   }
   /** 先選擇要建哪種房子,再去買房子. (在LandEmpty上設置Building)
    * @see LandEmpty#calcBuildingCost( BuildingType  )
    * @see LandEmpty#setBuilding( BuildingType ) */
   public void act( UserInterface ui , GameEnv env ){
      Player user = super.getUser();
      int buyingCost = 0;
      BuildingType bt = null;

      BuildingType[] aBTs = land.getAvailBuildingTypes();
      ArrayList<BuildingType> l_aBTs = new ArrayList<BuildingType>(0);
      ArrayList<String> options = new ArrayList<String>(0);
      for( int i=0 ; i<aBTs.length ; ++i )
         if(  land.calcBuildingCost(aBTs[i]) <= user.getCash() ){
            l_aBTs.add( aBTs[i] );
            options.add(aBTs[i].dataName+" ( $"+land.calcBuildingCost(aBTs[i])+" )");
         }

      if( options.size() > 0 ){
         int userOpt = 0;
         while( true ){
            userOpt = ui.queryMultiChoiceWithCancel( 
                  "請點選想要開發的軟體:" , options.toArray(new String[0]) );

            if( userOpt == -1 )return; //Cancel

            bt = l_aBTs.get( userOpt );
            buyingCost = land.calcBuildingCost( bt );
            System.err.println( userOpt+" "+bt+" "+buyingCost );
            if( buyingCost > user.getCash() )
               ui.showMessage("你沒有足夠的錢喔>.^!");         
            else
               break;
         }
      }else{
         ui.showMessage("你沒有足夠的錢喔>.^!");
         return;
      }
      //使用免費卡
      ItemBuyablePassive item = findItem( user , "proj.item.ItemFreeCard" );
      if( item!=null && ui.queryYesNo("是否使用"+item+"?") )
         item.trigger().act( ui , env );
      else
         user.payMoney( buyingCost );
      //設置building   
      land.setBuilding( bt );

      //我這邊有點亂加亂加的...看看要怎麼改吧?
      Upgradable building = (Upgradable) land.getBuilding(); //一定是Upgradable
      ui.showMessage( ((Building) building).getBuildingData().name + "升到了 " + building.getLevel() + "級!\n" + 
            ((Building) building).getBuildingData().msgLevel[ building.getLevel() ]);
   }
}
