package proj.event;

import proj.item.*;
import proj.map.*;
import proj.player.*;
import proj.*;
import proj.ui.*;

/** 買一塊地的事件.
 * call from: Block.passBy(p) -> Land.passBy(p){no owner}. */
public class EventBuyLand extends EventSingleTarget{
   private LandEmpty land;
   /** Constructor: 買地. */
   public EventBuyLand(Player user,LandEmpty land){
      super(user);
      this.land = land;
   }
   /** 購買這塊地.
    * @see LandEmpty#calcBuyingCost( Player )
    * @see LandEmpty#setOwner( Player ) */
   public void act( UserInterface ui , GameEnv env ){
      Player user = super.getUser();
      int buyingCost = land.calcBuyingCost( user );

      if( buyingCost > user.getCash() ){
         ui.showMessage("你沒有足夠的錢喔>.^!");         
         return;
      }

      if( ui.queryYesNo("開始著手開發軟體要花"+ buyingCost +"元.\n是否開始開發呢?",
               "Roooooock!","謝謝再聯絡~") ){
         //使用免費卡
         ItemBuyablePassive item = findItem( user , "proj.item.ItemFreeCard" );
         if( item!=null && ui.queryYesNo("是否使用"+item+"?") )
            item.trigger().act( ui , env );
         else
            user.payMoney( buyingCost );
         land.setOwner( user );
      }
   }
}
