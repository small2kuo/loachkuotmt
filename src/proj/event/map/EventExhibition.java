package proj.event;

import proj.ui.*;
import proj.player.*;
import proj.item.*;
import proj.*;
import proj.map.*;

/** 住在大型建築物的的event */
public class EventExhibition extends EventP2P{
   private int days;
   private LandEmpty land;

   public EventExhibition(Player p,int days,LandEmpty land){
      super( land.getOwner() , p );
      this.days=days;
      this.land=land;
   }

   public void act( UserInterface ui , GameEnv env){
      Player passiver = super.getPassiver();
      Player activer  = super.getActiver();
      int transaction = land.calcToll( passiver );
      
      ui.showMessage( "路過"+land+",強致參觀"+days+"天" );
      passiver.getStatus().setState(Status.HOTEL,days);
     
      //=====計算參觀費=====
      //Event e=new EventPayToll( p , land );
      //e.act(ui,env);
      ui.showMessage( activer + " 向 " + passiver 
            + " 收取參觀費 " + transaction + "元" );

      //使用免費卡
      ItemBuyablePassive item = findItem( passiver , "proj.item.ItemFreeCard" );
      if( item!=null && ui.queryYesNo("是否使用"+item+"?") )
         item.trigger().act( ui , env );
      else if( activer.getStatus().cantMove( )  ){
         ui.showMessage( activer+"表示： 哭哭拿不到錢TT^TT!" );
      }else{
         passiver.payMoney( transaction );
         activer.gainMoney( transaction );
      }
   }
}
