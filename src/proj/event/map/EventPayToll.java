package proj.event;

import proj.item.*;
import proj.player.*;
import proj.map.*;
import proj.*;
import proj.ui.*;
import java.util.*;

/** 收取過路費的事件.
 * call from: Block.passBy(p) -> Land.passBy(p){not owner} -> Building.passBy(p). */
public class EventPayToll extends EventP2P{
   private LandEmpty land;
   /** Constructor: gainer向payer收取對land的過路費. */ 
   public EventPayToll(Player payer,LandEmpty land){
      super( land.getOwner() , payer );
      this.land = land;
   }
   /** gainer向payer收取對land的過路費. 包含payer付錢 and gainer給錢.
    * @see LandEmpty#calcToll( Player )
    * @see Player#payMoney( int )
    * @see Player#gainMoney( int ) */
   public void act( UserInterface ui , GameEnv env ){
      Player passiver = super.getPassiver();
      Player activer  = super.getActiver();
      int transaction = 0;
      
      //同一條街上的所有land應該都被計算進去
      Street s = land.getStreet();
      ArrayList<LandEmpty> land_al  = new ArrayList<LandEmpty>(0);
      for(int i=0;i<s.getBlockSize();i++){
        Land l = s.getBlockByIndex(i).getLand();
        if(l instanceof LandEmpty){
          LandEmpty landEmpty =(LandEmpty)l;
          if(landEmpty.getOwner()==activer){
            transaction+= landEmpty.calcToll( passiver );
            land_al.add(landEmpty);
          }
        }
      }
      ui.getAnimator().flash(land_al.toArray(new LandEmpty[0]), 3, 150, 50);
      
      ui.showMessage( activer + " Demo給 " + passiver 
            + " 看, 收取Demo費 " + transaction + "元" );
      
      //使用免費卡
      ItemBuyablePassive item = findItem( passiver , "proj.item.ItemFreeCard" );
      if( item!=null && ui.queryYesNo("是否使用"+item+"?") )
         item.trigger().act( ui , env );
      else if( activer.getStatus().cantMove( )  )
         ui.showMessage( activer+"表示： 哭哭拿不到錢TT^TT!" );
      else{
         passiver.payMoney( transaction );
         activer.gainMoney( transaction );
      }
   }
}
