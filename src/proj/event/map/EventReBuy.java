package proj.event;

import proj.item.*;
import proj.map.*;
import proj.map.building.*;
import proj.player.*;
import proj.*;
import proj.ui.*;

/** 買一棟空屋的事件.
 * 只可能被Building.passBy()呼叫.
 * call from: Block.passBy(p) -> Land.passBy(p){no owner&&have building}->Building.passBy(p). */
public class EventReBuy extends EventSingleTarget{
   private LandEmpty land;
   public EventReBuy(Player user,LandEmpty land){
      super(user);
      this.land = land;
   }
   /** 購買這棟空屋. (把LandEmpty設置Owner.)
    * @see LandEmpty#calcBuyingCost( Player )
    * @see LandEmpty#setOwner( Player ) */
   public void act( UserInterface ui , GameEnv env ){
      Player user = super.getUser();
      int buyingCost = 0;
      int choice = 0;

      if( user.getCash() <  Math.min ( land.calcReBuyCost( user ) ,
            land.calcReBuyBuildingCost( user ) ) )
         return;

      while( true ){
         switch( choice = chooseBuy( ui , user ) ){
            case -1:
               return;
            case 0://買房子
               buyingCost = land.calcReBuyBuildingCost( user ); 
               break;
            case 1://買地
               buyingCost = land.calcReBuyCost( user );
               break;
         }
         if( buyingCost > user.getCash() ){
            ui.showMessage("你沒有足夠的錢喔>.^!");
         }else
            break;
      }
      //使用免費卡
      ItemBuyablePassive item = findItem( user , "proj.item.ItemFreeCard" );
      if( item!=null && ui.queryYesNo("是否使用"+item+"?") )
         item.trigger().act( ui , env );
      else
         user.payMoney( buyingCost );
      
      land.setOwner( user );
      if( choice==1 ){
         land.removeBuilding();
      }
   }
   private int chooseBuy( UserInterface ui , Player user ){
      String msg = String.format( "%s\n%s\n%s\n", "請問要接手軟體還是購買空間呢？"
            , "接手軟體將要花"+((Buyable)land.getBuilding()).calcBuyingCost( user )+"元"
            , "購買空間將要花"+land.calcReBuyCost( user )+"元(原本軟體要被丟掉喔)" );
      String[] options = new String[]{ "接手開發" , "購買空間" };
      return ui.queryMultiChoiceWithCancel( msg , options );
   }
}
