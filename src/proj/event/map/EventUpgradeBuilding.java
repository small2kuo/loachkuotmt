package proj.event;

import proj.item.*;
import proj.map.*;
import proj.map.building.*;
import proj.player.*;
import proj.*;
import proj.ui.*;

/** 把一棟建築物升級的事件.
 * call from: Block.passBy(p) -> Land.passBy(p){owner} ->  Building.passBy(p). */
public class EventUpgradeBuilding extends EventSingleTarget{
   private Upgradable building;
   public EventUpgradeBuilding(Player user,Upgradable building){
      super(user);
      this.building = building;
   }
   /** 把建築物升級.
    * @see Upgradable#calcUpgradeCost( Player )
    * @see Upgradable#upgrade( Player ) */
   public void act( UserInterface ui , GameEnv env ){
      Player user = super.getUser();
      int buyingCost = building.calcUpgradeCost( user );

      if( buyingCost > user.getCash() ){
         ui.showMessage("你沒有足夠的錢喔>.^!");         
         return;
      }

      if( ui.queryYesNo("要升級 " + ((Building) building).getBuildingData().name + " 要花"+ buyingCost +"元.\n是否要升級呢?",
               "那就升級吧@@!","不用了~\"~") ){
         //使用免費卡
         ItemBuyablePassive item = findItem( user , "proj.item.ItemFreeCard" );
         if( item!=null && ui.queryYesNo("是否使用"+item+"?") )
            item.trigger().act( ui , env );
         else
            user.payMoney( buyingCost );

         building.upgrade( 1 );
         //我這邊有點亂加亂加的...看看要怎麼改吧?
         ui.showMessage( ((Building) building).getBuildingData().name + "升到了 " + building.getLevel() + "級!\n" + 
               ((Building) building).getBuildingData().msgLevel[ building.getLevel() ]);
      }
   }
}
