package proj.event;

import proj.player.*;
import proj.map.*;
import proj.*;
import proj.ui.*;

import java.util.*;

/** 人物移動的事件. */
public class EventPlayerMove extends EventSingleTarget{
   /** Constructor: */
   public EventPlayerMove(Player user){
      super(user);
   }
   /** 把人從一格拔起來 丟到下一格去. */
   public void act( UserInterface ui , GameEnv env ){
      Player user = super.getUser();
      Status status = user.getStatus();
      Block block = user.getBlock() , preBlock = block;

      //走吧!
      Block[] goBlock = getNextBlocks( user.getBlock() , user.getNotGoBlock() );
      //選取下一個要走的Block 
      block = goBlock[ GameTool.rnd( 0 , goBlock.length ) ];
      //設定步數 位置
      status.setSteps( status.getSteps()-1 );   //停下來的狀況setSteps會處理

      if( preBlock != block ){
         preBlock.removeStandable( user );
         ui.showPlayerMove(user, preBlock , block );
         user.setBlock( block );
         block.addStandable( user );
      }
   }
   private Block[] getNextBlocks( Block blk , Block[] notGoBlk ){
      Block[] adjBlock = blk.getNeighbors(); 
      ArrayList<Block> blkNei = new ArrayList<Block>(0);
      for(int i=0;i<adjBlock.length;++i)
         blkNei.add( adjBlock[i] );
      
      for( int i=0;i<notGoBlk.length ; ++i )
         blkNei.remove( notGoBlk[i] );

      if(blkNei.size()==0)
         blkNei.add( notGoBlk[0] );
      
      return blkNei.toArray( new Block[0] );
   }
}

