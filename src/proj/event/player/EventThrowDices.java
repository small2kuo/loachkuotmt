package proj.event;

import proj.player.*;
import proj.ui.*;
import proj.*;

/** 丟骰子的事件. */
public class EventThrowDices extends EventSingleTarget{
   /** Constructor: 執骰子. */
   public EventThrowDices(Player user){
      super(user);
   }
   /** 設定使用者為走路狀態 and 拔掉地圖上的使用者. */
   public void act( UserInterface ui , GameEnv env ){
      Player user = super.getUser();

      //[Q]骰子是什麼樣子的東東呢??
      int dice_num = 0;

      if(user.last!=0){
        dice_num=user.next_dice_num;
        user.last--;
      }else{
        for(int i=0;i<user.getDiceNums();++i)
           dice_num += GameTool.rnd(1,6+1);
      }
      //顯示丟骰子動畫
      ui.showThrowDices( user , dice_num );
      //設定使用者為走路狀態
      user.getStatus().setState( Status.WALKING );
      user.getStatus().setSteps( dice_num );
      //拔掉使用者
      user.getBlock().removeStandable( user );
   }
}
