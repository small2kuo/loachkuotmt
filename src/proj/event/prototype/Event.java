package proj.event;

import proj.ui.*;
import proj.player.*;
import proj.item.*;
import proj.*;

/** 就是事件XD" */
public abstract class Event implements java.io.Serializable{
	//Serializable 好像是因為會記起來 我也不知道為啥
   public abstract void act( UserInterface ui , GameEnv env);
   
   /** 尋找某個BuyablePassive的Item. */
   protected ItemBuyablePassive findItem( Player user , String itemClassName ){
      return (ItemBuyablePassive)user.findItem(itemClassName);
   }
}
