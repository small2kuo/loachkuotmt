package proj.event;

import proj.player.*;
import proj.ui.*;
import proj.*;

/** 多人事件：無從屬關係的事件. */
public abstract class EventMultipleTarget extends Event{
   private final Player[] players;
   /** 建立與此事件相關的Player列表. */
   public EventMultipleTarget( Player[] players ){
      this.players = new Player[ players.length ];
      for(int i=0;i<players.length;++i)
         this.players[i] = players[i];
   }
   public abstract void act(UserInterface ui, GameEnv env);
}
