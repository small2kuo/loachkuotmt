package proj.event;

import proj.player.*;
import proj.*;
import proj.ui.*;

/** 雙人從屬事件：兩者且有從屬關係的事件 */
public abstract class EventP2P extends Event{
   /** 事件使用主體 */
   private final Player activer;
   /** 事件使用客體 */
   private final Player passiver;
   /** 建立一個雙人從屬事件,並以兩者為參數 */
   public EventP2P( Player activer , Player passiver ){
      this.activer   = activer;
      this.passiver  = passiver;
   }
   protected Player getActiver(){return activer;}
   protected Player getPassiver(){return passiver;}   
   public abstract void act(UserInterface ui, GameEnv env);
}
