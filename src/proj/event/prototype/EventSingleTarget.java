package proj.event;

import proj.player.*;
import proj.*;
import proj.ui.*;

/** 單人事件：單人使用的事件 */
public abstract class EventSingleTarget extends Event{
   /** 取得事件使用者 */
   private final Player user;
   /** 建立一個單人事件 */
   public EventSingleTarget( Player user ){
      this.user = user;
   }
   protected Player getUser(){return user;};
   public abstract void act(UserInterface ui, GameEnv env);
}

