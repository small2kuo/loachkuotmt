package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 *香蕉卡
 *  在視野內放置香蕉皮，每經過一個香蕉皮，將在原有步數基礎上多前進一步。
 */
public class ItemBananaCard extends ItemBuyable{
   public ItemBananaCard(){this(null);}
   public ItemBananaCard( Player p ){
      super( GameTool.getItemData( "ItemBananaCard" ) , p );
   }
   public Event use(){
     return new EventPlaceItem( this.getOwner() , new ItemBanana() ,this);
   }
}
