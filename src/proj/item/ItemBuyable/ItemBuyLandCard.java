package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.map.building.*;
/**
 *購地卡: 
 *  當處在不是自己土地上的時候,可以使用購地卡,連同建築強行收購獲得此地.
 */
public class ItemBuyLandCard extends ItemBuyable{
   public ItemBuyLandCard(){this(null);}
   public ItemBuyLandCard( Player p ){
      super( GameTool.getItemData( "ItemBuyLandCard" ) , p );
   }
   public Event use(){
      //一定要現在踩到是LandEmpty才行
      if( !( getOwner().getBlock().getLand() instanceof LandEmpty ) )
         return new EventCantUse("不能在這個空間上開發喔!\n請到可以進行開發的空間上用~");
      return new EventForceBuyLand(getOwner(),this);
   }
}
/** 強制設定Player現在踩到的land、building是自己的(不付錢)
 * @see ItemBuyLandCard */
class EventForceBuyLand extends EventItem{
   public EventForceBuyLand(Player p,Item toBeUsed){
      super( p ,toBeUsed );
   }
   public void act(UserInterface ui, GameEnv env){
      LandEmpty land = (LandEmpty)super.user.getBlock().getLand();
      land.setOwner(super.user);
      ui.showMessage(super.user+"表示：我是偽土地公~~");
      GameTool.notifyAllMapObservers();
      //用完後會從Player道具列中消失
      super.remove();
   }
}
