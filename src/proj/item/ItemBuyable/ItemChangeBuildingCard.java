package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.map.building.*;
/**
 *換屋卡
 *  停留在有房屋的土地上時，可以使用換屋卡，交換視野內房屋.
 *  實際上是交換房屋的等級而已.
 */
public class ItemChangeBuildingCard extends ItemBuyable{
   public ItemChangeBuildingCard(){this(null);}
   public ItemChangeBuildingCard( Player p ){
      super( GameTool.getItemData( "ItemChangeBuildingCard" ) , p);
   }
   public Event use(){
      //一定要現在踩到是LandEmpty而且要有房子才行
      if( !( this.getOwner().getBlock().getLand() instanceof LandEmpty ) )
         return new EventCantUse("不能在這個空間上開發喔!\n請到可以進行開發的空間上用~");
      if( this.getOwner().getBlock().getLand().getBuilding() == null )
         return new EventCantUse("這個空間裡面沒有軟體!!\n請到有軟體的空間上用~");
      return new EventChangeBuilding( this.getOwner() ,this);
   }
}
/** TODO 交換兩個房子的Level. 
 * @see ItemChangeBuildingCard */
class EventChangeBuilding extends EventItem{
   public EventChangeBuilding(Player p,Item toBeUsed){
     super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
      Upgradable this_b = (Upgradable)super.user.getBlock().getLand().getBuilding();
      Upgradable that_b = ui.queryChooseUpgradable();
      if( that_b == null )return;
      
      //交換building Level
      int this_lv  =  this_b.getLevel();
      int that_lv  =  that_b.getLevel();
      
      that_b.downgrade( this_lv - that_lv );
      this_b.downgrade( that_lv - this_lv );
      ui.showMessage(super.user+"說：換換換~全部都給他交換啦~");
      GameTool.notifyAllMapObservers();
      //用完後會從Player道具列中消失
      super.remove();
   }
}
