package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 * 換地卡
 *  停留在土地上時，可以使用換地卡，連同建築交換視野內土地。
 */
public class ItemChangeLandCard extends ItemBuyable{
   public ItemChangeLandCard(){this(null);}
   public ItemChangeLandCard( Player p ){
      super( GameTool.getItemData( "ItemChangeLandCard" ) , p );
   }
   public Event use(){
      //目前所在地必須要是LandEmpty
      if( !( this.getOwner().getBlock().getLand() instanceof LandEmpty ) )
         return new EventCantUse("不能在這個空間上開發喔!\n請到可以進行開發的空間上用~");
      return new EventChangeLand( this.getOwner() ,this);
   }
}
/** 更改目前Land跟指定Land的Owner(沒主人也可以換!)
 * @see ItemChangeLandCard */
class EventChangeLand extends EventItem{
   public EventChangeLand(Player p,Item toBeUsed){
     super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
      LandEmpty this_land = (LandEmpty)super.user.getBlock().getLand();
      LandEmpty that_land = ui.queryChooseLandEmpty();
      
      if( that_land == null )return;
      Player this_owner = this_land.getOwner();
      Player that_owner = that_land.getOwner();

      this_land.setOwner( that_owner );
      that_land.setOwner( this_owner );

      ui.showMessage( super.user+":交換空間囉~" );

      GameTool.notifyAllMapObservers();
      super.remove();
   }
}
