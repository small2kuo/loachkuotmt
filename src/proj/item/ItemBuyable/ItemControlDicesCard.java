package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 *遙控骰子
 *  使用遙控骰子，可以自由控制下一次骰子點數。 
 */
public class ItemControlDicesCard extends ItemBuyable{
   public ItemControlDicesCard(){this(null);}
   public ItemControlDicesCard( Player p ){
      super( GameTool.getItemData( "ItemControlDicesCard" ) , p );
   }
   public Event use(){
      return new EventControlDices( this.getOwner() ,this);
   }
}
/** @see ItemControlDicesCard */
class EventControlDices extends EventItem{
   public EventControlDices(Player p,Item toBeUsed){
     super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
     int dices_num=ui.queryInteger("遙控骰子：請輸入幾步(1~6)",1,6);
     if(dices_num==-1)return;
     super.user.next_dice_num = dices_num;
     super.user.last=1;
     super.remove();
   }
}
