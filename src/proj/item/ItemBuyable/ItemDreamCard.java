package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 *夢遊卡
 *  對對方使用夢遊卡，可以讓此人3天不能操作，不能收取租金，不能觸發事件。
 */
public class ItemDreamCard extends ItemBuyable{
   public ItemDreamCard(){this(null);}
   public ItemDreamCard( Player p ){
      super( GameTool.getItemData( "ItemDreamCard" ) , p );
   }
   public Event use(){
      return new EventDream( this.getOwner() ,this);
   }
}
/** 指定玩家去睡覺 .
 * @see ItemDreamCard */
class EventDream extends EventItem{
   public EventDream(Player p,Item toBeUsed){
      super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
      Player p = ui.queryChoosePlayer(super.user);
      if( p == null )return;
      //如果他現在不能動
      if( p.getStatus().cantMove() ){
         ui.showMessage( p + "現在處於" + p.getStatus() + "\n請換對象使用@@a..." );
         return;
      }
      p.getStatus().setState(Status.SLEEPING,3);
      ui.showMessage( p + ": zZzZzZzZ...!!" );
      super.remove();
   }
}
