package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 *陷害卡
 *  使用陷害卡後選定一名對手，可以立刻讓對方入獄2天。
 */
public class ItemEvilCard extends ItemBuyable{
   public ItemEvilCard(){this(null);}
   public ItemEvilCard( Player p ){
      super( GameTool.getItemData( "ItemEvilCard" ) , p );
   }
   public Event use(){
      return new EventEvil( this.getOwner() ,this);
   }
}
/** set 指定玩家.狀態 = 監獄.
 * @see ItemEvilCard */
class EventEvil extends EventItem{
   public EventEvil(Player p,Item toBeUsed){
      super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
      Player p = ui.queryChoosePlayer(super.user);
      if( p == null )return;
      //如果他現在不能動
      if( p.getStatus().cantMove() ){
         ui.showMessage( p + "現在處於" + p.getStatus() + "\n請換對象使用@@a..." );
         return;
      }
      EventPrison e = new EventPrison( p , 2 , p+"：我是被冤望的呀~~大人~~" );
      e.act( ui , env );
      super.remove();
   }
}
