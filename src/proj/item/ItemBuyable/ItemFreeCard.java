package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/** 免費卡. */
public class ItemFreeCard extends ItemBuyablePassive{
   public ItemFreeCard(){this(null);}
   public ItemFreeCard( Player p ){
      super( GameTool.getItemData( "ItemFreeCard" ) , p );
   }
   public Event use(){
      return new EventCantUse( "這個是被動卡片現在無法使用喔!" );
   }
   public Event trigger(){
      return new EventTakeFree( getOwner() , this );
   }
}
/** @see ItemFreeCard */
class EventTakeFree extends EventItem{
   public EventTakeFree(Player user,Item toBeUsed){
      super( user , toBeUsed );
   }
   public void act( UserInterface ui, GameEnv env){
      ui.showMessage( this.user+"表示: 輕鬆免費喔>.^!!" );
      super.remove();
   }
}
