package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.map.building.*;
/**
 * 拆屋卡
 *   使用拆屋卡後，可以任意選擇一處視野內房屋建築，將之拆低一級。
 */
public class ItemHouseBreakCard extends ItemBuyable{
   public ItemHouseBreakCard(){this(null);}
   public ItemHouseBreakCard( Player p ){
      super( GameTool.getItemData( "ItemHouseBreakCard" ) , p );
   }
   public Event use(){
      return new EventHouseBreak( super.getOwner() , this );
   }
}
/** 把指定建築物downgrade 1級
 * @see ItemHouseBreakCard */
class EventHouseBreak extends EventItem{
   public EventHouseBreak(Player p,Item toBeUsed){
      super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
      Upgradable building=ui.queryChooseUpgradable();
      if( building == null )return;//取消選擇
      
      building.downgrade(1);
      if( building.getLevel() == 0 )
         ((BuildingSmall)building).getLand().removeBuilding();
      ui.showMessage(user+"說: 中毒吧嘿嘿嘿!!");
      GameTool.notifyAllMapObservers();
      super.remove();
   }
}
