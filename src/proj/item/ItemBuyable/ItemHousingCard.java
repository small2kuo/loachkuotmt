package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.map.building.*;
/**
 * 建屋卡 
 *  將視野內任意一處建築加蓋一層。
 */
public class ItemHousingCard extends ItemBuyable{
   public ItemHousingCard(){this(null);}
   public ItemHousingCard( Player p ){
      super( GameTool.getItemData( "ItemHousingCard" ) , p );
   }
   public Event use(){
      return new EventHousing( super.getOwner() ,this);
   }
}
/** 選擇一棟建築物升級. 
 * @see ItemHousingCard */
class EventHousing extends EventItem{
   public EventHousing(Player p,Item toBeUsed){
      super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
      Upgradable building=ui.queryChooseUpgradable();
      if( building == null )return;//取消選擇
      
      building.upgrade( 1 );
      
      ui.showMessage(user+"說：衝呀衝呀~熱血開發@@!");
      GameTool.notifyAllMapObservers();
      super.remove();
   }
}
