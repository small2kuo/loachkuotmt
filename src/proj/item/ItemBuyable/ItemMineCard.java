package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 * 超級地雷卡
 *   地雷的進化版。普通模式下角色受傷後住院6天。收集8張地雷卡後自動獲得。
 */
public class ItemMineCard extends ItemBuyable{
   public ItemMineCard(){this(null);}
   public ItemMineCard( Player p ){
      super( GameTool.getItemData( "ItemMineCard" ) , p );
   }
   public Event use(){
      ItemStandable item = new ItemMine();
      return new EventPlaceItem( this.getOwner() , item ,this);
   }
}
