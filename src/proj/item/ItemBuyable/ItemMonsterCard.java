package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.map.building.*;
/**
 *怪獸卡 
 *  停留在土地上時，可以使用怪獸卡將目前所處土地上的房屋夷為平地。
 */
public class ItemMonsterCard extends ItemBuyable{
   public ItemMonsterCard(){this(null);}
   public ItemMonsterCard( Player p ){
      super( GameTool.getItemData( "ItemMonsterCard" ) , p );
   }
   public Event use(){
      Player p = super.getOwner();
      //一定要現在踩到是LandEmpty而且要有房子才行
      if( !( p.getBlock().getLand() instanceof LandEmpty ) )
         return new EventCantUse("不能在這個空間上開發喔!\n請到可以進行開發的空間上用~");
      if( p.getBlock().getLand().getBuilding() == null )
         return new EventCantUse("這個空間裡面沒有軟體!!\n請到有軟體的空間上用~");
      return new EventMonster(p,this);
   }
}
/** 
 * @see ItemMonsterCard */
class EventMonster extends EventItem{
   public EventMonster(Player p,Item toBeUsed){
      super( p , toBeUsed );
   }
   public void act(UserInterface ui, GameEnv env){
      Upgradable building = (Upgradable)super.user.getBlock().getLand().getBuilding();
      
      building.downgrade(100);
      if( building.getLevel() == 0 )
         ((BuildingSmall)building).getLand().removeBuilding();
      
      ui.showMessage( super.user + "表示: 看我找出你的軟體的bug!!" );
      GameTool.notifyAllMapObservers();
      super.remove();
   }
}
