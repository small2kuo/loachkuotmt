package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 *均貧卡
 *  對一個對手使用均貧卡，可以與此對手平分現金。 
 */
public class ItemPoorCard extends ItemBuyable{
   public ItemPoorCard(){this(null);}
   public ItemPoorCard( Player p ){
      super( GameTool.getItemData( "ItemPoorCard" ) , p );
   }
   public Event use(){
      return new EventPoor( this.getOwner() ,this);
   }
}
/** 把兩個cash抓出來/2.
 * @see ItemPoorCard */
class EventPoor extends EventItem{
   public EventPoor(Player p,Item toBeUsed){
      super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
      Player p=ui.queryChoosePlayer(super.user);
      if(p==null)return;

      int money1 = p.getCash();
      int money2 = super.user.getCash();

      if( money1 > money2 ){
         p.payMoney( (money1-money2)/2 );
         super.user.gainMoney( (money1-money2)/2 );
         ui.showMessage(super.user+"從"+p+"獲得$"+(money1-money2)/2);
      }else{
         p.gainMoney((money2-money1)/2);
         super.user.payMoney((money2-money1)/2);
         ui.showMessage(p+"從"+super.user+"獲得$"+(money2-money1)/2);
      }
      super.remove();
   }
}
