package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 *均富卡
 *  對所有對手使用均富卡，可以與所有對手平分現金。 
 */
public class ItemRichCard extends ItemBuyable{
   public ItemRichCard(){this(null);}
   public ItemRichCard( Player p ){
      super( GameTool.getItemData( "ItemRichCard" ) , p );
   }
   public Event use(){
      return new EventRich( this.getOwner() ,this);
   }
}
/** @see ItemRichCard */
class EventRich extends EventItem{
   public EventRich(Player p,Item toBeUsed){
      super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
      Player pa[] = env.getAllPlayers();
      int total = 0;
      int pn = 0;
      for(int i=0;i<pa.length;i++)
         if(pa[i].getStatus().getState() != Status.DEAD ){ //沒死才能均富呀呀
            total+=pa[i].getCash();
            pn++;
         }

      total /= pn;
      for(int i=0;i<pa.length;i++)
         if(pa[i].getStatus().getState() != Status.DEAD ){ //沒死才能均富呀呀
            if( pa[i].getCash() > total )
               pa[i].payMoney(pa[i].getCash()-total);
            else
               pa[i].gainMoney(total-pa[i].getCash());
         }

      ui.showMessage(super.user+"：大家是共患難的好兄弟!!~");
      super.remove();
   }
}
