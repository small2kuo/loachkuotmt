package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 *冬眠卡
 *  使用後除自己以外的所有玩家冰凍3回合。??會不會有點少呢?
 */
public class ItemSleepCard extends ItemBuyable{
   public ItemSleepCard(){this(null);}
   public ItemSleepCard( Player p ){
      super( GameTool.getItemData( "ItemSleepCard" ) , p );
   }
   public Event use(){
      return new EventSleep( this.getOwner() ,this);
   }
}
/** 把所有使用者變成睡覺狀態. 
 * @see ItemSleepCard */
class EventSleep extends EventItem{
   public EventSleep(Player p,Item toBeUsed){
      super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
      Player pa[]=env.getAllPlayers();
      for(int i=0;i<pa.length;i++)
         if( pa[i]!=super.user 
               && !pa[i].getStatus().cantMove() ){ //沒死才能冰凍呀
            pa[i].getStatus().setState(Status.SLEEPING,3);
               }
      ui.showMessage( super.user+": 睡吧睡吧哇哈哈!!" );
      super.remove();
   }
}
