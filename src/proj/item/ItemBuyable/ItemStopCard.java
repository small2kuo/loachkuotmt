package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 *路障卡
 *  使用路障卡後，選擇視野內任一處放下路障，任何角色遇到路障都會停下。
 */
public class ItemStopCard extends ItemBuyable{
   public ItemStopCard(){this(null);}
   public ItemStopCard( Player p ){
      super( GameTool.getItemData( "ItemStopCard" ) , p );
   }
   public Event use(){
      ItemStandable item = new ItemStop();
      return new EventPlaceItem( this.getOwner() , item ,this);
   }
}
