package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 * 查稅卡
 *   直接對視野內對手使用而從其手中收取現金+存款的10%稅款。
 */
public class ItemTaxCard extends ItemBuyable{
   public ItemTaxCard(){this(null);}
   public ItemTaxCard( Player p ){
      super( GameTool.getItemData( "ItemTaxCard" ) , p );
   }
   public Event use(){
      return new EventTax( this.getOwner() ,this);
   }
}
/** get某個人10%的錢.
 * @see ItemTaxCard */
class EventTax extends EventItem{
   public EventTax(Player p,Item toBeUsed){
      super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
      Player p=ui.queryChoosePlayer(super.user);
      if( p == null )return;
      int tax = p.getCash()/10;
      p.payMoney(tax);
      super.user.gainMoney(tax);
      ui.showMessage(super.user+"查到盜用別人軟體"+p+"罰錢一共$"+tax);
      super.remove();
   }
}
