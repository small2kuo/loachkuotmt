package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 * 轉向卡
 *   使用轉向卡，可以讓對手或自己立刻掉頭向反方向前進。 
 */
public class ItemTurnCard extends ItemBuyable{
   public ItemTurnCard(){this(null);}
   public ItemTurnCard( Player p ){
      super( GameTool.getItemData( "ItemTurnCard" ) , p );
   }
   public Event use(){
      return new EventTurn( this.getOwner() ,this);
   }
}
/** 選取某個人把他轉方向.
 * @see ItemTurnCard */
class EventTurn extends EventItem{
   public EventTurn(Player p,Item toBeUsed){
      super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
      Player p=ui.queryChoosePlayer(null);
      if(p==null)return;
      
      ui.showMessage( super.user + ":轉吧轉吧~七彩"+p+"!" );
      p.changeDirection();
      super.remove();
   }
}
