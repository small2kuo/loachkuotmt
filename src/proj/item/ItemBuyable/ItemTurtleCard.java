package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 * 烏龜卡
 *   對對方或自己使用停留卡，可以讓目標連續三回合都走一步。
 */
public class ItemTurtleCard extends ItemBuyable{
   public ItemTurtleCard(){this(null);}
   public ItemTurtleCard( Player p ){
      super( GameTool.getItemData( "ItemTurtleCard" ) , p );
   }
   public Event use(){
      return new EventTurtle( this.getOwner() ,this);
   }
}
/** 把某個人下次丟的點數設成1
 * @see ItemTurtleCard */
class EventTurtle extends EventItem{
   public EventTurtle(Player p,Item toBeUsed){
     super(p,toBeUsed);
   }
   public void act(UserInterface ui, GameEnv env){
     Player p=ui.queryChoosePlayer(null);
     if(p == null)return;
     p.next_dice_num=1;
     p.last=3;
     super.remove();
   }
}
