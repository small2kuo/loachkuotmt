package proj.item;

import proj.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

import java.io.*;

/** ItemData. */
public class ItemData implements Serializable{
   public String name;
   public String handImgPath;
   public String groundImgPath;
   public ItemData( String name , String handImgPath , String groundImgPath ){
      this.name = name;
      this.handImgPath = handImgPath;
      this.groundImgPath = groundImgPath;
   }
}
