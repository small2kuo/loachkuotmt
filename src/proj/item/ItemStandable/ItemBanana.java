package proj.item;

import proj.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.ui.*;

/**
 *香蕉卡(放在block上的)
 *  在視野內放置香蕉皮，每經過一個香蕉皮，將在原有步數基礎上多前進一步。
 */
public class ItemBanana extends ItemStandable{
   //還沒放好所以不知道自己的位置(block)
   public ItemBanana(){
      super( GameTool.getItemData( "ItemBanana" ) , null,null );
   }
   public Event touch(Player p){
      this.remove();
      return new EventBanana(p);
   }
}
/** @see ItemBanana */
class EventBanana extends EventSingleTarget{
   public EventBanana(Player p){
      super(p);
   }
   public void act(UserInterface ui, GameEnv env){
      super.getUser().getStatus().setSteps(super.getUser().getStatus().getSteps()+1);
      ui.showMessage(super.getUser()+"不小心踩到香蕉!!滑滑滑~滑倒了");
   }
}
