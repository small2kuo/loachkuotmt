package proj.item;

import proj.*;
import proj.ui.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.map.building.*;

/** 寶箱. */
public class ItemChestBox extends ItemStandable{
   private ItemBuyable[] ilist;
   public ItemChestBox( Block b , ItemBuyable ... items ){
      super( GameTool.getItemData( "ItemChestBox" ) , b , null );
      this.ilist = items;
   }
   public Event touch( Player p ){
      if( p.getStatus().getState() != Status.STOP || ilist.length == 0 )
         return new EventDoNothing();

      super.remove(); 
      return new EventGetItem( p , ilist[ GameTool.rnd(0,ilist.length) ] );
   }
}
/** 在使用者物品欄裡面加一個item.
 * @see ItemChestBox */
class EventGetItem extends EventSingleTarget{
   private ItemBuyable item;
   public EventGetItem( Player p , ItemBuyable i ){
      super( p );
      this.item = i;
   }
   public void act( UserInterface ui, GameEnv env ){
      Player p = super.getUser();
      this.item.setOwner( p );
      p.addItem( this.item );
      ui.showMessage( p+"撿到"+item+",超爽ㄉ~" );
   }
}
