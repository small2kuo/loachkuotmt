package proj.item;

import proj.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;
/**
 * 地雷(放在Block的)
 *   超級地雷的山寨版。普通模式下角色受傷後住院3天。
 */
public class ItemMine extends ItemStandable{
   //還沒放好所以不知道自己的位置(block)
   public ItemMine(){
      super( GameTool.getItemData( "ItemMine" ) , null,null );
   }
   public Event touch(Player p){
      if( p.getStatus().getState() == Status.STOP ){
         this.remove();
         return new EventHospital(p,3,p+"踩到地雷~~送醫急救三天");
      }else
         return new EventDoNothing();
   }
}
