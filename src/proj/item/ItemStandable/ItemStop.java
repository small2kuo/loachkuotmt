package proj.item;

import proj.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.ui.*;

/**
 * 路障卡(放在Block的)
 *   使用路障卡後，選擇視野內任一處放下路障，任何角色遇到路障都會停下。
 */
public class ItemStop extends ItemStandable{
   //還沒放好所以不知道自己的位置(block)
   public ItemStop(){
      super( GameTool.getItemData( "ItemStop" ) , null,null );
   }
   public Event touch(Player p){
      this.remove(); 
      return new EventStop(p);
   }
}
/** 把人的步數設成0. 
 * @see ItemStop */
class EventStop extends EventSingleTarget{
   public EventStop(Player p){
      super(p);
   }
   public void act(UserInterface ui, GameEnv env){
      getUser().getStatus().setSteps(0);
      ui.showMessage( getUser()+": Don't Block me!!!" );
   }
}
