package proj.item;

import proj.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/**
 *超級地雷(放在Block的)
 *  地雷的進化版。普通模式下角色受傷後住院6天。
 */
public class ItemSuperMine extends ItemStandable{
   //還沒放好所以不知道自己的位置(block)
   public ItemSuperMine(){
      super( GameTool.getItemData( "ItemSuperMine" ) , null,null );
   }
   public Event touch(Player p){
      if(p.getStatus().getState()==Status.STOP){
         this.remove();
         return new EventHospital(p,6,p+"踩到超級大大大地雷~~送醫急救六天");
      }
      else return new EventDoNothing();
   }
}
