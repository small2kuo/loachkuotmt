package proj.item;

import proj.event.*;
import proj.player.*;
import java.io.Serializable;

/** 道具.
 * 每種道具一定有一個data. */
public abstract class Item implements Serializable{
   private ItemData data;
   public Item( ItemData data ){
      this.data = data;
   }
   public ItemData getData(){return this.data;}
   public String toString(){return data.name;}
}
