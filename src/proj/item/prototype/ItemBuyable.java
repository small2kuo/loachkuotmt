package proj.item;

import proj.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/** 可以持有的道具. */
public abstract class ItemBuyable extends Item implements Buyable{
   public ItemBuyable( ItemData data , Player owner ){
      super( data );
      setOwner( owner );
   }
   /** 當擁有者要使用這個東西.
    * 刪除item"不應該"要在這裡做?
    * 因為有可能使用了會取消. 所以移除動作應該要放在Event裡.
    * 同樣新增物品也不應該在這裡做!!*/
   public abstract Event use();
   /*----------買賣相關(Buyable)----------*/
   public int calcBuyingCost(Player p){return 0;}
   public int calcSellingProfit(Player p){return 0;}
   public int getValue(){return 0;}
   /*----------擁有者相關(Buyable)----------*/
   private Player owner;
   public void setOwner( Player owner ){
      this.owner = owner;
   }
   public Player getOwner(){
      return this.owner;
   }
}
