package proj.item;

import proj.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/** 可以持有的被動型道具. */
public abstract class ItemBuyablePassive extends ItemBuyable{
   public ItemBuyablePassive( ItemData data , Player owner ){
      super( data , owner );
   }
   /** 當擁有者要使用這個東西會無法使用.
    * 因為他是被動使用的@@!. */
   public Event use(){
      return new EventCantUse( "這個是被動卡片現在無法使用喔!" );
   }
   /** 當擁有者發生某事件的時候,Event會去呼叫這個method來傳回會使用這個道具的事件. */
   public abstract Event trigger();
}
