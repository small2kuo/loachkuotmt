package proj.item;

import proj.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/** 可以放在路上的道具. */
public abstract class ItemStandable extends Item implements Standable {
   //需不需要owner???
   public ItemStandable( ItemData data ,  Block block , Player owner ){
      super( data );
      setBlock( block );
      setOwner( owner );
   }
   /** 在路上碰到這個東西. */ 
   public abstract Event touch(Player p);
   /*----------擁有者相關(Owner)----------*/
   private Player owner;
   public void setOwner( Player owner ){
      this.owner = owner;
   }
   public Player getOwner( ){
      return this.owner;
   }
   /*----------地點相關(Standable)----------*/
   private Block block;
   public void setBlock( Block block ){
      this.block = block;
   }
   public Block getBlock(){
      return block;
   }
   public void remove(){
      if( this.block!=null ){
         this.block.removeStandable( this );
         System.err.println( this+"從地圖上拔掉囉!" );
      }
   }
}
