package proj.map;

import proj.map.building.*;
import proj.event.*;
import proj.player.*;
import proj.item.*;
import java.util.*;
import java.io.Serializable;

/**
 * 這個的定義是一格：這個與一個人前進的"一格"是相同的，
 * 這個 block 所連結的 Land，有可能會指向同一塊 Land。
 * 人與道具可以採在這個空格上，玩家經過或停下來的時候
 * 會觸動 {@link #passBy} 而回傳事件。
 */
public class Block implements Serializable, MapComponentVisitable{
   protected ArrayList<Standable> itemList = new ArrayList<Standable>(); //上面出現的人物或道具
   protected Land land; //這個 block 裡面的 land
   protected Street street;

   //記錄上次Event做到哪裡
   private int lastRounds, eventRunIndex;
   private Player lastPlayer;
   private ArrayList<Event> elist;

   /** 理論上這個 method 會按照順序回傳所有應該發生的事件 */
   public Event passBy(int rounds, Player p){
	   System.err.print("Block: passBy(" + rounds + ", " + p + ")");
	   if(lastRounds != rounds || lastPlayer != p){
		   lastRounds = rounds;
		   lastPlayer = p;
		   eventRunIndex = 0;
		   elist = new ArrayList<Event>();
		   for(int i = 0; i < itemList.size(); i++){
			   Event e = itemList.get(i).touch(p);
			   if(e != null)
				   elist.add(e);
		   }
		   if(land != null){
				Event e = land.passBy(p);
			if(e != null)
			   elist.add(e);
		   }
	   }
	   System.err.println(" eID: " + eventRunIndex);
	   if(eventRunIndex < elist.size())
		   return elist.get(eventRunIndex++);
	   else{
		   lastPlayer = null; //要結束了, 把player設成null, 怕同一回合之內有可能再走回來
		   return null;
	   }
   }

   /** 這個 function 會決定 something 能不能被放在這個 block 上面 */
   public boolean canPutStandable( Standable something ){
      /*
      //是Item的話就要確認這個地方沒放其他物品.
      if( something instanceof ItemStandable ){
         for( int i=0 ; i<itemList.size() ; ++i )
            if( itemList.get(i) instanceof Item )
               return false;
         return true;
      }
      return true;
      */
      return itemList.size()==0;
   }

   /** 加上一個 Standable */
   public void addStandable(Standable something){
      itemList.add(something);
   }
   /** 移除一個 Standable */
   public void removeStandable(Standable something){
      itemList.remove(something);
   }
   /** 取得 Street */
   public Street getStreet(){
      return this.street;
   }

   /** 取得 land */
   public Land getLand(){
      return this.land;
   }

   /** 取得 Standable */
   public Standable[] getStandable(){
      return itemList.toArray(new Standable[0]);
   } 

   /** 傳回前一個Block, 如果沒路就傳回null. */
   public Block preBlock(){
      int idx = this.street.indexOf( this );
      if( idx-1 < 0 )
         return null;
      else
         return this.street.getBlockByIndex( idx-1 );
   }
   /** 傳回後一個Block, 如果沒路就傳回null. */
   public Block nextBlock(){
      int idx = this.street.indexOf( this );
      if( this.street.getBlockSize() <= idx+1 )
         return null;
      else
         return this.street.getBlockByIndex( idx+1 );
   }
   public Block[] getNeighbors( ){
      ArrayList<Block> blkNei = new ArrayList<Block>(0);
      //先把同條街上的block加進來
      Block tmp = null;
      if( (tmp = this.nextBlock())!=null ) blkNei.add(tmp);
      if( (tmp = this.preBlock())!=null ) blkNei.add(tmp);
      //再把接出去的加進來
      Street[] branch = null;
      if( this == street.getBlockByIndex(0) ){
         branch = street.getStreetFrom();

         for(int i=0;i<branch.length;++i)
            blkNei.add( branch[i].getBlockByIndex( branch[i].getBlockSize()-1 ) );

      }else if( this == street.getBlockByIndex( street.getBlockSize()-1 ) ){
         branch = street.getStreetTo();

         for(int i=0;i<branch.length;++i)
            blkNei.add( branch[i].getBlockByIndex( 0 ) );
      }
      return blkNei.toArray( new Block[0] );
   }
   //========== Constructor ===========
   /** 這個 Block 上面的 Land 是個新的 LandEmpty */
   public Block(Street s){
      this(s, new LandEmpty(s));
   }
   /** 這個 Block 上面的 Land 指定了可以蓋哪些房子 */
   public Block(Street s, BuildingType[] bt_arr){
      this(s, new LandEmpty(s, bt_arr));
   }
   /** 這個 Block 上面的 Land 是個已知的 Land */
   public Block(Street s, Land land){
      this.street = s;
      this.land = land;
   }
   /** 這個 Block 上面的 Land 是個已知 Block 上面的 Land */
   public Block(Street s, Block block){
      this.street = s;
      this.land = block.land;
   }
   //============ MapComponentVisitable =============
   public void accept(MapComponentVisitor visitor){
      visitor.visit(this);
   }
   public void accept(MapComponentVisitor visitor, java.awt.Shape shape, java.awt.Graphics g){
      visitor.visit(this, shape, g);
   }
}
