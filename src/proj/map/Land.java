package proj.map;

import proj.*;
import proj.event.*;
import proj.player.*;
import proj.map.building.*;
import java.io.Serializable;

public class Land implements Serializable, MapComponentVisitable{
   protected Street street;
   protected Building building;

   /** 這個方法回傳當玩家經過這塊地時會發生什麼事情.
    * 預設的話會看看這塊土地上有沒有 {@link Building},
    * 有的話就取得 Event, 否則回傳 EventDoNothing();
    * @see Building#passBy(Player)
    */
   public Event passBy(Player p){
      if( building == null || p.getStatus().isWalking() )return new EventDoNothing();
      return building.passBy(p);
   }

   /** 至少要給 Street */
   public Land(Street street){
      this.street = street;
      this.building = null;
   }
   /** 給定了 building */
   public Land(Street street, Building building){
      this.street = street;
      this.building = building;
      if(building != null)
         building.setLand(this);
   }
   /** 給定了 building type */
   public Land(Street street, BuildingType bt){
      this.street = street;
      //這邊需要 GameTool 這個 class 來產生一個新的 Building
      this.building = GameTool.getNewBuildingByType(bt, this);
   }
   /** 取得建築物 */
   public Building getBuilding(){
      return building;
   }
   /** 取得街道 */
   public Street getStreet(){
	   return street;
   }
   //============ MapComponentVisitable =============
   public void accept(MapComponentVisitor visitor){
      visitor.visit(this);
   }
   public void accept(MapComponentVisitor visitor, java.awt.Shape shape, java.awt.Graphics g){
	   visitor.visit(this, shape, g);
   }
}
