package proj.map;

import proj.*;
import proj.player.*;
import proj.map.building.*;
import proj.event.*;

public class LandEmpty extends Land implements Buyable{
   private Player owner;
   /** 指定可以建造的建築物種類.
    * 這個如果是 null 代表不指定建築種類 */
   private final BuildingType[] availBuildingTypes;
   
   //========== Constructor ==========
   /** 空的 Land */
   public LandEmpty(Street s){
      this(s, null, 100);
   }

   /** 指定建築種類的 Land, 如果傳入 null 代表不指定建築種類 */
   public LandEmpty(Street s, BuildingType[] bt_arr){
      this(s, bt_arr, 100);
   }
   /** 指定了土地價值，這個值乘以地段加成才是實際價值 */
   public LandEmpty(Street s, BuildingType[] bt_arr, int landValue){
      super(s);
      this.owner = null;
      this.availBuildingTypes = bt_arr;
      this.landValue = landValue;
   }

   //========== Method ==========
   /** 這個方法回傳當玩家經過這塊地時會發生什麼事情.
    * 當building存在時會回傳building的passBy事件,
    * 否則收取買土地/買房子/收過路費.
    * @see Building#passBy
    * @see EventBuyLand
    * @see EventBuyBuilding
    * @see EventPayToll
    */
   public Event passBy(Player p){
      if( p == null ) return null;
      if( p.getStatus().isWalking() )return new EventDoNothing();
      if( building == null ){
         //沒有房子OAQ
         if(owner == null){
            //土地沒主人    -> 買土地
            return new EventBuyLand(p, this);
         }else if(p == owner){
            //地主是你      -> 蓋房子
			System.err.println("LandEmpty: 蓋房子");
            return new EventBuyBuilding(p, this);
         }else
            //地主不是你    -> 收過路費>.^
            return new EventPayToll(p, this);
      }else
         return building.passBy(p);
   }
   
   //========== Calculate ===========
   /** 計算蓋房子的價格，基礎價格乘以地價加成。
    * 如果目前有房子，不計算賣(毀掉?)房子的價格。
    * @see EventBuyBuilding */
   public int calcBuildingCost(BuildingType bt){
      if( this.owner == null )
         return 0; //這個部份應該是要回傳不能買？
      //return (int) (((Buyable) b).calcBuyingCost(owner) * street.getPriceFactor());
      return (int) (GameTool.calcBuildingBuyingCost(bt, owner) * street.getPriceFactor());
   }
   /** 計算賣房子的獲利，理論上不能夠隨意地賣掉房子？ */
   public int calcSellBuildingProfit(Player p){
      if( this.building == null )
         return 0;
      return ((Buyable) this.building).calcSellingProfit(p);
   }
   /** 計算經過時所需要付的費用，預設為土地價值的十分之一.
    * 被{@link EventPayToll}呼叫 */
   public int calcToll(Player p){
      if( super.getBuilding() != null)
         return super.getBuilding().calcToll(p);
      return this.getValue()/10;
   }
   //===== 重買相關 =====
   /** 重購這塊土地上的建築物的價格.
    * 被{@link EventReBuy}呼叫 */
   public int calcReBuyBuildingCost( Player p ){
      if( this.building instanceof BuildingSmall )
         return (int)(((BuildingSmall)this.building).calcReBuyCost(p) * street.getPriceFactor());
      return 0;
   }
   /** 重購這塊土地的價格.
    * 被{@link EventReBuy}呼叫 */
   public int calcReBuyCost( Player p ){
      return calcBuyingCost( p );
   }
   //========== 建房子相關 ==========
   /** 取得可以建造的房屋列表 */
   public BuildingType[] getAvailBuildingTypes(){
      return availBuildingTypes; //TODO: 如果房子重複蓋要小心? 需不需要移除
   }
   /** 直接蓋房子, 這邊要用 GameTool 產生 */
   public void setBuilding(BuildingType bt){
      this.building = GameTool.getNewBuildingByType(bt, this, this.owner);
	  GameTool.notifyAllMapObservers();	//地圖訊息有變更.
   }
   /** 這個與 setBuilding(null) 效果相同 */
   public void removeBuilding(){
      this.building = null;
   }
   
   private int landValue;
   //=========== interface Buyable ============
   /** 買入價格為土地價格 * 地段加成.
    * 被{@link EventBuyLand}呼叫 */
   public int calcBuyingCost(Player p){
      return this.getValue();
   }
   /** 預設售出獲利為買入價格之 1/2 (銀行抵押?)*/
   public int calcSellingProfit(Player p){
      return calcBuyingCost(p)/2;
   }
   public void setOwner(Player p){
      this.owner = p;
	  GameTool.notifyAllMapObservers();	//地圖訊息有變更.
   }
   public Player getOwner(){
      return this.owner;
   }
   /** 取得此塊土地的價格. */
   public int getValue(){
      return (int)(this.landValue * this.street.getPriceFactor());
   }
}
