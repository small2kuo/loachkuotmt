package proj.map;

import proj.map.building.*;

/** 一個公有地不應該是空的，理論上應該要有一棟房子在上面. */
public class LandPublic extends Land{
   public LandPublic(Street s, Building b){
      super(s, b);
   }
}
