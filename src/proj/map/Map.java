package proj.map;

import proj.*;
import java.util.*;
import java.io.*;

/**
 * 每一個 Map 製作好以後應該要與圖片配合(需要畫一張大圖)
 * 裡面需要設法標明所有物件的位置。
 *
 * 一個 Map 裡面會有 Streets, 在這個 class 裡面只表示了 Streets 之間的關係。
 * 尚不知道這些 Street 會怎麼被顯示在螢幕上。
 */
public class Map implements Serializable{
	protected ArrayList<Street> streetSet;
	protected String name; //只能在Constructor階段指定name, 因為之後用不到
	protected MapData data; //決定每一個 Block/Land/Street 在真正地圖上的座標位置.
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	public MapData getMapData(){
		return data;
	}
        public Street[] getStreets(){return streetSet.toArray( new Street[0] );}
	public void setMapData(MapData data){
		this.data = data;
	}
	//TODO: loadMapData()
	public Map(String name, MapData data){
		this.name = name;
		this.data = data;
		streetSet = new ArrayList<Street>();
	}
        /**
         * @see MapData#MapData(String bgImageSource , String miniBgImageSource)
         * */
	public Map(String name){
		this(name, new MapData(null , null));
	}
	/** 增加一個 Street */
	public void addStreet(Street s){
		streetSet.add(s);
	}
}
