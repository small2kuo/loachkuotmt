package proj.map;

import java.awt.Shape;
import java.awt.Graphics;
/**
 * 參考<a href="http://caterpillar.onlyfun.net/Gossip/DesignPattern/VisitorPattern.htm">這裡</a>
 */
public interface MapComponentVisitable{
	void accept(MapComponentVisitor visitor); //Visitor Pattern
	void accept(MapComponentVisitor visitor, Shape shape, Graphics g); //Associated With a shape
}
