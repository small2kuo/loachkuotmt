package proj.map;

import java.awt.Shape;
import java.awt.Graphics;
/** 拜訪、取得資料、顯示 */
public interface MapComponentVisitor{
	void visit(Block block);
	void visit(Land land);
	void visit(Street street);
	void visit(Block block, Shape shape, Graphics g);
	void visit(Land land, Shape shape, Graphics g);
	void visit(Street street, Shape shape, Graphics g);
}
