package proj.map;

import proj.*;
import java.util.*;
import java.awt.*;
import java.io.*;

/** 裡面儲存了關於地圖的所有靜態資料，例如每一個 Street, Block, Land 物件出現在地圖上的位置 */
public class MapData implements Serializable{
   public String bgImgSrc;
   public String miniBgImgSrc;
   public ArrayList<Shape> allShapes;
   public HashMap<Shape, MapComponentVisitable> hashmap;
   
   public MapData(String bgImageSource , String miniBgImageSource){
      this.bgImgSrc = bgImageSource;
      this.miniBgImgSrc = miniBgImageSource;
      allShapes = new ArrayList<Shape>();
      hashmap = new HashMap<Shape, MapComponentVisitable>();
   }
   public Shape getShape(MapComponentVisitable c){
	   for(int i = 0; i < allShapes.size(); i++)
		   if(hashmap.get(allShapes.get(i)) == c)
			   return allShapes.get(i);
	   return null;
   }
}
