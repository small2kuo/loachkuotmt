package proj.map;

import java.io.Serializable;

/** 地圖的簡單資訊.
 * 存於{@literal data/record/<map.name>}.info.
 * 其對應的map為{@literal data/map<map.name>}.dat. */
public class MapInfo implements Serializable{
   private String name;
   private String miniBgImgSrc;
   public MapInfo( Map map ){
      this.name = map.name;
      this.miniBgImgSrc = map.data.miniBgImgSrc;
   }
   public String getName(){return this.name;}
   public String toString(){return getName();}
}
