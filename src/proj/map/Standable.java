package proj.map;

import proj.event.Event;
import proj.player.*;

public interface Standable{
   public Event touch(Player p);
   
   public void setBlock( Block block );
   public Block getBlock( );

   //Standable 應該要知道自己在哪裡
/*   public int getBlockIndex();
   public Street getStreet();

   public void setStreet(Street s);
   public void setBlockIndex(int i);*/
}
