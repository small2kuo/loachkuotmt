package proj.map;

import java.util.*;
import java.io.Serializable;

/**
 * 街道，上面會有一排提供行走的格子，有方向性，上面的房子屬於同一個地段。
 */
public class Street implements Serializable, MapComponentVisitable{
   private String name;
   private ArrayList<Street> streetFrom; //前一條街道(反向行走時使用)
   private ArrayList<Street> streetTo; //下一條街道
   public final int length; //街道長度
   protected Block[] block; //街道上的每一個block
   private double priceFactor; //地段的價格加成

   /** 新的街道上定義了街道長度, 預設地價加成為 1 */
   public Street(int length){
      this(length, 1.0);
   }
   /** 新的街道上定義了街道長度, 也指定了地價加成 */
   public Street(int length, double priceFactor){
      this.length = length;
      this.priceFactor = priceFactor;
	  streetFrom = new ArrayList<Street>();
	  streetTo = new ArrayList<Street>();
      block = new Block[length];
      initBlocks();
   }
   /** 設定名字 */
   public void setName(String name){
      this.name = name;
   }
   /** 取得名字 */
   public String getName(){
      return name;
   }
   //=========== 價格相關 ===========
   /** 取得地段的價格加成 */
   public double getPriceFactor(){
      return priceFactor;
   }
   /** 設定地段的價格加成 */
   public void setPriceFactor(double p){
      priceFactor = p;
   }
   /** 設定地段價格加成的相對加成 */
   public void setPriceFactorByFactor(double factor){
      priceFactor *= factor;
   }
   //=========== Block相關 ===========
   /** 取得 Block */
   public Block getBlockByIndex(int index){
      return block[index];
   }
   /** 取得某個 block 所在的索引編號 */
   public int indexOf(Block b){
      for(int i = 0; i < block.length; i++){
         if(b == block[i])
            return i;
      }
      return -1; //Not Found
   }
   /** 取得街道上Block的數量. */
   public int getBlockSize(){
      return block.length; 
   }
   /** 設定 Block, 初始化用的. */
   @Deprecated
   public void setBlock(int index, Block b){
	   block[index] = b;
   }
   /** 初始化道路上的所有格子.
    * TODO 要怎麼實踐呢?? 這個交給 EditUseStreet 來做吧? */
   public void initBlocks(){}
   //=========== Street相關 ===========
   /** 增加一條來源的道路 */
   public void addFromStreet(Street s){
      streetFrom.add(s);
   }
   /** 增加一條出口的道路 */
   public void addToStreet(Street s){
      streetTo.add(s);
   }
   /** 刪除一條來源道路 */
   public boolean removeFromStreet(Street s){
	   return streetFrom.remove(s);
   }
   /** 刪除一條出口道路 */
   public boolean removeToStreet(Street s){
	   return streetTo.remove(s);
   }
   /** 取得來源街道 */
   public Street[] getStreetFrom(){
      return streetFrom.toArray( new Street[0] );
   }
   /** 取得出口街道 */
   public Street[] getStreetTo(){
      return streetTo.toArray( new Street[0] );
   }
   /** 取得來源街道數量 */
   public int getStreetFromSize(){
      return streetFrom.size();
   }
   /** 取得出口街道數量 */
   public int getStreetToSize(){
      return streetTo.size();
   }
   //============ MapComponentVisitable =============
   public void accept(MapComponentVisitor visitor){
	   visitor.visit(this);
   }
   public void accept(MapComponentVisitor visitor, java.awt.Shape shape, java.awt.Graphics g){
	   visitor.visit(this, shape, g);
   }
}
