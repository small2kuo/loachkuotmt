package proj;

import proj.player.*;

/**
 * 只要是有等級、可以升級的東西都可以實作 Upgradable
 */
public interface Upgradable{

   /** 取得等級 */
   public int getLevel();

   /** 取得升級最大上限 */
   public int getMaxLevel();

   /** 一個玩家對該物件升級 */
   public void upgrade( int lv_up );

   /** 計算升級所需花費 */
   public int calcUpgradeCost(Player p);

   /** 降級，如果是事件觸動(機會、命運等)，Player 可能是 null */
   public void downgrade( int lv_down );

   //==========以下是配合UI的部分============

}
