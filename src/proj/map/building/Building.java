package proj.map.building;

import proj.event.*;
import proj.player.*;
import proj.map.*;
import java.io.*;

public abstract class Building implements Serializable{
   private Land land;
   private BuildingData data; //should be final?

   public Building(){
      this(null, null);
   }
   public Building(Land land){
      this(land, null);
   }
   public Building(Land land, BuildingData data){
      this.land = land;
      this.data = data;
   }
   public Land getLand(){
      return land;
   }
   public void setLand(Land land){
      this.land = land;
   }
   public BuildingData getBuildingData(){
      return data;
   }

   /**
    * 這邊要實作當一個玩家經過/進入這棟建築的時候，
    * 要回傳什麼樣的事件。
    */
   public abstract Event passBy(Player p);
   /**
    * 這邊要實作不管有沒有要付錢都要問看看需要付多少錢
    */
   public abstract int calcToll(Player p);
    
   /** 剛建造時可能會發生的事件. */
    public Event constructAct(){return new EventDoNothing();};
}
