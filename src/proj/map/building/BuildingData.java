package proj.map.building;

import java.io.*;
import java.awt.*;
/** 一個一般房子的 <b>靜態</b> Data */
public class BuildingData implements Serializable{
	public String name;
	public int maxLv;
        /** 每個階段所要顯示標註的字串 */
	public String[] msgLevel;
        /** 升到該等級所需要的花費，如果是 1 的話就是購買的費用.
         * 理論上不應該有index = 0的這個值被用到@@!*/
	public int[] upgradeCost;
        /** 計算經過該房子所需要繳交的過路費用 */
	public int[] passFee;
        /** 每一個等級所關聯的圖片檔名 */
	public String[] imgPath;
}
