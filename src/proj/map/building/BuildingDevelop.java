package proj.map.building;

import proj.*;
import proj.map.*;
import proj.event.*;
import proj.player.*;
import proj.item.*;
import proj.routine.*;
/**
 * 軟體研發中心: 不能收錢 + 定期獲得道具
 */
public class BuildingDevelop extends BuildingLarge{
   /**
    * 這邊要實作當一個玩家經過/進入這棟建築的時候，
    * 要回傳什麼樣的事件。
    */
   public BuildingDevelop(BuildingData data, LandEmpty land){
      super( data , land);
   }
   
   /** 每幾回合就會產生新道具給的routine. */ 
   public Event constructAct(){
      return new EventAddRoutine( 
            new RoutineAddPlayerItem( super.getOwner() , new Item[]{null} , 5)  );
   }
   public Event passBy(Player p){
      if(getOwner() == null){
         //可能是一棟沒有主人的空房子，可能是因為屋主倒閉了
         return new EventReBuy( p , (LandEmpty)super.getLand() );
      }else if(p == getOwner()){
         //問要不要升級
         return new EventUpgradeBuilding( p , this );
      }else{
         //不用付過路費!!
         return new EventDoNothing();
      }
   }
   /** 直接回傳0(不收錢). */
   public int calcToll(Player p){
      return 0;
   }
}
