package proj.map.building;

import proj.*;
import proj.map.*;
import proj.event.*;
import proj.player.Player;
import proj.player.Status;

/**
 * 軟體展示中心: 要付錢唷^.< + 會在裡面逛三天(限制行動=>人不能收錢喔)
 */
public class BuildingExhibition extends BuildingLarge{
   public BuildingExhibition(BuildingData data, LandEmpty land){
      super(data,land);
   }     

   public Event passBy(Player p){
      if(getOwner() == null){
         //可能是一棟沒有主人的空房子，可能是因為屋主倒閉了
         return new EventReBuy( p , (LandEmpty)super.getLand() );
      }else if(p == getOwner()){
         //問要不要升級
         return new EventUpgradeBuilding( p , this );
      }else{
         //逛三天+付過路費
         return new EventExhibition(p, 3 , (LandEmpty)super.getLand());
      }
   }
   /**
    * 這邊要實作不管有沒有要付錢都要問看看需要付多少錢
    */
   public int calcToll(Player p){
      return getBuildingData().passFee[super.getLevel()];
   }
}
