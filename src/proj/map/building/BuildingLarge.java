package proj.map.building;

import proj.*;
import proj.map.*;
import proj.event.*;
import proj.player.Player;

/**
 * 一些大型房子的基礎模型，其差異在於不同的 passBy 函式
 * TODO 其他延伸的 building
 */
public abstract class BuildingLarge extends BuildingSmall{
    public BuildingLarge( BuildingData data, LandEmpty land ){
        super(data, land);
    }
} 
