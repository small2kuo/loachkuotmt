package proj.map.building;

import proj.*;
import proj.map.*;
import proj.event.*;
import proj.player.Player;
import proj.player.Status;

/**
 * 軟體展售中心: 花錢(輪盤式決定花的錢) //call小小遊戲
 */
public class BuildingSales extends BuildingLarge{
   /**
    * 這邊要實作當一個玩家經過/進入這棟建築的時候，
    * 要回傳什麼樣的事件。
    */

   public BuildingSales(BuildingData data, LandEmpty land){
      super(data,land );
   }

   public Event passBy(Player p){
      if(getOwner() == null){
         //可能是一棟沒有主人的空房子，可能是因為屋主倒閉了
         return new EventReBuy( p , (LandEmpty)super.getLand() );
      }else if(p == getOwner()){
         //問要不要升級
         return new EventUpgradeBuilding( p , this );
      }else{
         //付過路費
         return new EventPayToll(p, (LandEmpty)super.getLand());
      }
   }
   /**
    * 直接回傳某個rnd的數值
    */
   public int calcToll(Player p){
      return getBuildingData().passFee[super.getLevel()]* GameTool.rnd(1,6);
   }
}
