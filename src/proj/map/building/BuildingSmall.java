package proj.map.building;

import proj.*;
import proj.map.*;
import proj.event.*;
import proj.player.Player;

/**
 * 一種很普通的房子，可以設定等級上限以及每一個等級需要升級付的費用。
 * (或許付費或升級費用可以使用公式計算?)
 * 目前先以陣列儲存這些資料，會有點浪費空間不過似乎還好?
 */
public class BuildingSmall extends Building implements Buyable, Upgradable{
   /** 普通房子的Level, 剛蓋好是1. */	
   private int level;

   public BuildingSmall( BuildingData data, LandEmpty land ){
      super(land, data);
      this.level = 1;
   }

   /** 經過普通房子的時候所發生的事件.
    * 普通的房子經過的時候有三種狀況:<br>
    * <ol><li>沒主人的時候: 詢問要不要買房子的Event
    *     <li>主人是你的時候: 詢問是否要升級的Event
    *     <li>主人不是你的時候: 要付過路費的Event</ol>
    * @see EventReBuy
    * @see EventUpgradeBuilding
    * @see EventPayToll */
   public Event passBy(Player p){
      if(getOwner() == null){
         //可能是一棟沒有主人的空房子，可能是因為屋主倒閉了
         return new EventReBuy( p , this.getLand() );
      }else if(p == getOwner()){
         //問要不要升級
         if(getLevel() < getMaxLevel())
            return new EventUpgradeBuilding( p , this );
         else
            return new EventDoNothing();
      }else{
         //付過路費
         return new EventPayToll(p, this.getLand() );
      }
   }

   /** 計算過路費.
    * 被{@link LandEmpty#calcToll}呼叫. */
   public int calcToll(Player p){
      return super.getBuildingData().passFee[ this.level ];
   }
   /** 購買這棟空房子的價格.
    * 被{@link #calcReBuyCost}呼叫. */
   public int calcReBuyCost( Player p ){
      return this.getValue();
   }
   //========== Buyable ===========
   /** 購買價格，可能與地段加成、物價有關，或者與購買者有關.
    * 被{@link EventBuyBuilding}或{@link EventReBuy}呼叫. */
   public int calcBuyingCost(Player p){
      if( getOwner() == null )
         return calcReBuyCost( p );
      return super.getBuildingData().upgradeCost[1];
   }

   /** 賣出獲利，可能與地段加成、物價有關，或者與賣出者有關 */
   public int calcSellingProfit(Player p){
      return this.getValue() / 2; //應該是除以2
   }

   /** 取得目前的物件價值 */
   public int getValue(){
      int val = 0;
      for(int i = 1; i <= level; i++){
         val += super.getBuildingData().upgradeCost[i];
      }
      return val;
   }

   /** 只要是可以買賣的，都應該可以設定擁有者 */
   public void setOwner(Player p){
      this.getLand().setOwner(p);
   }
   /** 取得目前該物件的擁有者。 */
   public Player getOwner(){
      return this.getLand().getOwner();
   }
   //========== Upgradable ===========
   /** 取得等級 */
   public int getLevel(){
      return level;
   }
   /** 取得升級最大上限 */
   public int getMaxLevel(){
      return super.getBuildingData().maxLv;
   }
   /** 一個玩家對該物件升級 */
   public void upgrade( int lv_up ){
      level = Math.min( level+lv_up , getMaxLevel() );
   }
   /** 計算升級所需花費.
    * 被{@link EventUpgradeBuilding}呼叫. */
   public int calcUpgradeCost(Player p){
      if(level < super.getBuildingData().maxLv)
         return super.getBuildingData().upgradeCost[level+1];
      return 0;
   }
   /** 降級，如果是事件觸動(機會、命運等)，Player 可能是 null */
   public void downgrade(int downlevels){
      if(level <= downlevels){
         level = 0; //乾脆移除房子算了?? 應該要丟給Event做!?
      }else
         level -= downlevels;
   }
   //========== Override ===========
   @Override
   public LandEmpty getLand(){
      return (LandEmpty)super.getLand();
   }
}
