package proj.map.building;

import java.io.*;
/**
 * BuildingType 裡面包含了某種房子的檔案資訊，
 * 包括須動態載入的資料檔，以及互相配合的房子模型檔
 */
public class BuildingType implements Serializable{
	public final String dataName;
	public final String className;
	public BuildingType(String d, String c){
		dataName = d;
		className = c;
	}
	public String toString(){
		return "data: " + dataName + "/ class: " + className;
	}
}
