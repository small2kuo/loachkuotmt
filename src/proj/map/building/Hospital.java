package proj.map.building;

import proj.*;
import proj.map.*;
import proj.player.*;
import proj.event.*;

/** 醫院: 沒有效果 (一塊純粹價值的地) 
 * BuildingTye必須是BuildingType( "Hospital" , "Hospital" ) */
public class Hospital extends Building{
   /*
   public Hospital( Land land ){
      super( land , GameTool.getBuildingType( 
               new BuildingType( "Hospital" , "Hospital" ) )  );
   }*/
   public Hospital( Land land , BuildingData data ){
      super( land , data );
   }
   public Event passBy(Player p){
      return new EventDoNothing();
   }
   public int calcToll(Player p){return 0;}
}
