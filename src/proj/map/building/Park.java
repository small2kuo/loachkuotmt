package proj.map.building;

import proj.*;
import proj.map.*;
import proj.player.*;
import proj.event.*;

/** 私人公園: 沒有效果 (一塊純粹價值的地) 
 * BuildingTye必須是BuildingType( "Park" , "Park" ) */
public class Park extends Building{
   /*public Park( Land land ){
      super( land , GameTool.getBuildingType( 
               new BuildingType( "Park" , "Park" ) )  );
   }*/
   public Park( Land land , BuildingData data ){
      super( land , data );
   }
   public Event passBy(Player p){
      return new EventDoNothing();
   }
   public int calcToll(Player p){return 0;}
}
