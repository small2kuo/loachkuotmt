package proj.map.building;

import proj.*;
import proj.map.*;
import proj.player.*;
import proj.event.*;

/** 監獄: 沒有效果 (一塊純粹價值的地) 
 * BuildingTye必須是BuildingType( "Prison" , "Prison" ) */
public class Prison extends Building{
   /*
   public Prison( Land land ){
      super( land , GameTool.getBuildingType( 
               new BuildingType( "Prison" , "Prison" ) )  );
   }*/
   public Prison( Land land , BuildingData data ){
      super( land , data );
   }
   public Event passBy(Player p){
      return new EventDoNothing();
   }
   public int calcToll(Player p){return 0;}
}
