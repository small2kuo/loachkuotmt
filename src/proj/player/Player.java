package proj.player;

import proj.*;
import proj.map.*;
import proj.event.*;
import proj.item.*;
import java.util.ArrayList;
import java.io.Serializable;
/** 使用者. */
public class Player implements Standable, Serializable{
   private int cash;
   private int dices;//how many dices do the player has
   
   //next_dice_num 下次擲骰子的點數  (給遙控骰子用)
   public int next_dice_num=0;// 1-6表示下次值的點數
   public int last=0;//持續幾回合
   //last=0 代表沒有(正常擲骰子)

   private ArrayList<Item> itemList;
   private Status status;
   private String name;
   private Block block = null;
   private Block[] notGoBlock = new Block[0];

   private PlayerData data; //靜態資料，包含玩家代表色、玩家頭像、行走圖等

   public Player(String name,int cash,PlayerData data){
      this.name=name;
      this.cash=cash;
      itemList=new ArrayList<Item>(0);
      status=new Status();
      dices=1;
      this.data = data;
   }
   
   /** 當新的回合開始要呼叫這個method告訴player呦~. */
   public void newRound(){
      this.status.deEffectDays();
   }
   /** 當A遇上B...(上B?) */
   public Event touch(Player p){
      Event event=new EventPlayerMeet(p,this);
      return event;
   }
   //========== Get ==========
   /** 取得玩家靜態資料, 預設是給 UI介面取得使用 */
   public PlayerData getData(){
      return data;
   }
   public String getName(){
      return name;
   }
   public String toString(){
      return this.getName();
   }
   /** 取得當前狀態(Status). */
   public Status getStatus(){
      return status;
   }
   //========== 金錢相關 ==========
   public void payMoney(int cost){ 
      cash-=cost;
   }
   public void gainMoney(int money){ 
      cash+=money;
   }
   /** 取得目前現金 */
   public int getCash(){
      return cash;
   }
   //========== Item相關 ==========
   /** 給定物品的ClassName 尋找相同ClassName的物品. */
   public Item findItem( String itemClassName ){
      Item items[]=getAllItems();
      for(int i=0;i<items.length;i++){
         if( itemClassName.equals( items[i].getClass().getName() ) )
            return items[i];
      }
      return null;
   }
   public boolean addItem(Item item){
      if(item==null)return false;
      return itemList.add(item);
   }
   public boolean removeItem(Item item){ 
      return itemList.remove( item );
   }
   public Item[] getAllItems(){
      return itemList.toArray(new Item[0]);
   }

   public int getDiceNums(){
      return dices;
   }
   public boolean setDiceNums(int d){
      if(d<=0)return false;
      dices=d;
      return true;
   }
   //=========== Standable ==========
   /** 取得當前所在的Block. */
   public Block getBlock(){
      return this.block;
   }
   public Block[] getNotGoBlock(){
      return this.notGoBlock;
   }
   /** 設置Block.
    * 只把人的block改掉.
    * "在地圖上拔掉自己&amp;加上自己"交給{@link EventPlayerMove}. */
   public void setBlock( Block b ){
      if( this.notGoBlock.length!=1 )
         this.notGoBlock = new Block[1];
      this.notGoBlock[0] = this.block;
      this.block = b;
   }
   /** 改變行走方向. */
   public void changeDirection(){
      Block[] adjBlocks = this.block.getNeighbors();
      ArrayList<Block> chos = new ArrayList<Block>(0);
      for( int i=0 ; i<adjBlocks.length ; ++i )
         chos.add( adjBlocks[i] );
      for( int i=0; i<notGoBlock.length ; ++i )
         chos.remove( notGoBlock[i] );
      notGoBlock = chos.toArray( new Block[0] );
   }
}
