package proj.player;

import proj.*;

import java.awt.*;
import java.io.*;

/**
 * 玩家的所有<b>靜態</b>資料!.
 * 目前都是GUI =_=
 *
 * 預設檔案路徑是{@literal data/player/<PlayerData.name>}.dat
 */
public class PlayerData implements Serializable{
   public String name;           //角色的型態名稱
   public Color pColor;          //角色的顏色
   public String avatarPath;     //頭像
   public String charSetPath;    //行走圖像

   /** 方便測試用, 應該要從檔案讀取才是? */
   public PlayerData(String name,Color c, String a, String b){
      this.name = name;
      pColor = c;
      avatarPath = a;
      charSetPath = b;
   }
}
