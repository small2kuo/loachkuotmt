package proj.player;

import proj.*;
import proj.map.*;
import proj.event.*;
import proj.item.*;
import java.util.ArrayList;
import java.io.Serializable;
/** 要顯示在存讀檔列表中的使用者資訊.
 * <b>只會</b>被{@link GameEnvInfo}存取.
 * @see Player */
public class PlayerInfo implements Serializable{
   private String name;
   private int cash;
   private PlayerData pdata;
   public PlayerInfo( Player p ){
      this.name = p.getName();
      this.cash = p.getCash();
      this.pdata = p.getData();
   }
}
