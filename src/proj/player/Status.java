package proj.player;
import java.io.Serializable;

/** 使用者狀態.
 * 紀錄天數要在這裡而非routine: 因為時間是看使用者不是看回合 */
public class Status implements Serializable{
   /** 人物狀態. */
   private int state;
   /** 人物維持這個狀態還剩幾天解除*/
   private int effectDays;
   
   private int steps;//how many steps left to go.
   
   /** Enumeration all condition. */
   public static final int STOP      =   0;
   public static final int WALKING   =   1;
   public static final int SLEEPING  =   2;//冬眠卡的效果
   public static final int HOSPITAL  =   3;
   public static final int JAIL      =   4;
   public static final int HOTEL     =   5;//住在大型building中的狀態
   public static final int DEAD      =   6;//死掉的狀態X_X
   
   public String toString(){
      String[] stat = new String[]{"停止中",
         "行走中",
         "睡覺中",
         "治療中",
         "被關中",
         "住宿中",
         "死掉了"};
      return stat[this.state];
   }
   public Status(){
      steps=0;
      state=STOP;
   }
   /** 是否在行走. */
   public boolean isWalking(){
      return state == WALKING;
   }
   /** 是否不能動. */
   public boolean cantMove(){
      return state!=STOP && state!=WALKING;
   }
   /** 剩下的步數. */
   public int getSteps(){
      return steps;
   }
   /** 設定走路步數.
    * 要先設定步數,再設定狀態!! */
   public void setSteps(int s){
      if( (this.steps=s)==0 )
        this.state = this.STOP;
      else this.state = this.WALKING;  //這行要嗎??
   }
   
   /** 同時設定狀態以及天數 */
   public void setState(int s,int d){
      state = s;
      this.effectDays = d;
   }
   /** 設定狀態.
    * [Q]如果任意修改了狀態, 需要查看當前狀態嗎? 
    * 理論上不行, Event會檢查! */
   public void setState(int s){
      state = s;
   }
   /** 取得當前狀態. */
   public int getState(){
      return state;
   }
   /** 取得當前狀態.
    * 還要維持幾天. */
   public int getEffectDays(){
     return this.effectDays;
   }
   public void deEffectDays(){
     if( this.effectDays>0 )
        this.effectDays--;
     else
        this.state = STOP;
   }
}
