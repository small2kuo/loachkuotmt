package proj.routine;

import proj.*;
import proj.event.Event;
import proj.ui.UserInterface;
import java.io.Serializable;

/** 給每回合要做的事件註冊在gameController內 */
public abstract class Routine implements Serializable{
   public abstract Event execute(UserInterface ui , GameEnv env);
}
