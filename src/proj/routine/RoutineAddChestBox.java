package proj.routine;

import proj.*;
import proj.ui.*;
import proj.map.*;
import proj.item.*;
import proj.event.*;
import proj.player.*;

/**
 * 每n回合會隨機增加一些寶箱在地上.
 * */
public class RoutineAddChestBox extends Routine{
   /** 可以獲得的item list. */
   private int idx = -1;
   private String[] itemList;
   
   private int cycle,count;

   public RoutineAddChestBox( int cycle ){
      this.cycle=cycle;
      count=0;
      itemList = GameTool.getAllItemBuyableName(); 
   }
   public Event execute(UserInterface ui , GameEnv env){
      count++;
      if(count>=cycle){
         count = 0;

         ItemChestBox box = makeChestBox( env );
         Block blk = getBlockToPut( env , box );
         if( blk == null )return null;
         
         box.setBlock( blk );
         return new EventPutItem( box , blk );
      }else{
         return null;
      }
   }
   private ItemChestBox makeChestBox( GameEnv env ){
      return new ItemChestBox( null , nextItem() );
   }
   private ItemBuyable nextItem(){
      if( ++idx >= itemList.length ){
         listShuffle();
         idx = 0;
      }
      ItemBuyable item = null;
      try{
         item = (ItemBuyable)(Class.forName("proj.item." + itemList[idx])).newInstance();
      }catch( Exception e ){System.err.println(e);}
      return item;
   }
   /** 弄亂itemList表 */
   private void listShuffle(){
      String tmp;
      int a,b,len = itemList.length;
      for( int i = 0  ; 2*i < len ; ++i  ){
         a = GameTool.rnd( 0 , len );
         b = GameTool.rnd( 0 , len );
         //swap
         tmp = itemList[ a ];
         itemList[a] = itemList[b];
         itemList[b] = tmp;
      }
   }
   /** 選擇一個地方去放... */
   private Block getBlockToPut( GameEnv env , ItemStandable item ){
      Block[] blks = GameTool.getAllBlocks( env );
      Block put_b = null;
      //嘗試三次隨便選個block看看可不可以放XD (偷懶:P)
      for( int i=0;i<3;++i )
         if( ( put_b = blks[ GameTool.rnd( 0 , blks.length ) ] ).canPutStandable( item ) )
            return put_b;
      //不行就不要放囉>.^
      return null;
   }
}
