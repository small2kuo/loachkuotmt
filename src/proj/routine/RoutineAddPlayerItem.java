package proj.routine;

import proj.event.Event;
import proj.GameEnv;
import proj.GameTool;
import proj.ui.UserInterface;
import proj.player.Player;
import proj.item.Item;
import proj.event.*;

/**
 * 每n回合會隨機增加某個指定的Player的道具
 * 由BuildingDevelop來註冊 給他的owner
 */
public class RoutineAddPlayerItem extends Routine{
   /* 固定不變，如果BuildingDevelop 的owner改了就要把這個Routine刪除
      ,重新再new,items也一樣
      TODO */
   private Player p;
   /** 可以獲得的item list. */
   private Item[] items;
   private int cycle,count;

   public RoutineAddPlayerItem(Player p,Item items[],int cycle){
      this.p=p;
      this.items=items;    
      this.cycle=cycle;
      count=0;
   }

   public Event execute(UserInterface ui , GameEnv env){
      count++;
      if(count>=cycle){
         count = 0;
         return new EventAddPlayerItem(p,items[GameTool.rnd(0,items.length-1)]);
      }else{
         return null;
      }
   }
}
