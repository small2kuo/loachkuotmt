package proj.ui;

import proj.*;
import proj.map.*;
import proj.map.building.*;
import proj.item.*;
import proj.event.*;
import proj.player.*;

import java.awt.GraphicsDevice;
import java.awt.Window;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.LayoutManager;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/** 負責實行所有<b>遊戲進行中</b> UI 介面的動畫的GUI物件. */
public class GUIAnimator implements UserInterfaceAnimator{
   private GameEnv env;
   private GUIGameFrame gameFrame;
   private GUIMapPanel mapPanel;
   private GUIMenuBarPanel menuBarPanel;
   private GUIOptionPanel optionPanel;
   private JPanel middlePanel;
   private JWindow loadingWindow;

   private UI_EventHandler ehandle;
   private JLayeredPane popupPane;
   public GUIRoot.PlayerMovingLabel u; //playerMovingLabel
   
   private GUIRoot ui;
   /** 傳入 GUIRoot的所有field 進行設定XD". */
   public GUIAnimator(  GUIRoot ui
         ,GameEnv env
         ,GUIGameFrame gameFrame
         ,GUIMapPanel mapPanel
         ,GUIMenuBarPanel menuBarPanel 
         ,GUIOptionPanel optionPanel 
         ,JPanel middlePanel
         ,JWindow loadingWindow
         ,UI_EventHandler ehandle
         ,JLayeredPane popupPane
		 ,GUIRoot.PlayerMovingLabel u){
      this.ui = ui;
      this.env = env;
      this.gameFrame = gameFrame;
      this.mapPanel = mapPanel;
      this.menuBarPanel = menuBarPanel;
      this.optionPanel = optionPanel;
      this.middlePanel = middlePanel;
      this.loadingWindow = loadingWindow;
      this.ehandle = ehandle;
      this.popupPane = popupPane;
	  this.u = u;
	  popupPane.add(diceL, new Integer(-100));
	  popupPane.add(afterDiceL, new Integer(-100));
	  popupPane.add(coveringPanel, new Integer(800));
	  coveringPanel.setBounds(0, 0, 800, 600);
   }

   //TODO===========UserInterface==============
   public class ActionListenerJumpingPlayer implements ActionListener{
      boolean t = false;
      public void actionPerformed(ActionEvent e){
         if(t) u.setLocation(u.getX(), u.getY()+7);
         else u.setLocation(u.getX(), u.getY()-7);
         t=!t;
      }
   }
   //========================= 擲骰子相關 ==============================
   public DiceLabel diceL = new DiceLabel();
   public DiceLabel afterDiceL = new DiceLabel("1");
   public class DiceLabel extends JLabel implements GUIRelocatable{
	   boolean pflag = false;
      public DiceLabel(){
         super();
      }
	  public DiceLabel(String str){
		  super(str);
		  this.setFont(new Font("LiHei pro", 0, 96));
		  pflag = true;
	  }
      int realX=-1, realY=-1;
      public void setXY(int realX,int realY){
         this.realX = realX;
         this.realY = realY;
      }
	  public void relocate(){
		  relocate(mapPanel.getTopLeftX(), mapPanel.getTopLeftY() );
	  }
      public void relocate(int topLeftCornerX, int topLeftCornerY){
         this.setLocation(realX - topLeftCornerX, realY - topLeftCornerY);
      }
	  @Override
		  public void paint(Graphics g){
			  if(pflag){
				  g.setColor( Color.WHITE );
				  g.drawString( getText(), 5, 88);
			  }
			  super.paint(g);
		  }
      @Override
         public void setIcon(Icon icon){
            if(icon != null)
               this.setSize(icon.getIconWidth(), icon.getIconHeight());
            super.setIcon(icon);
         }
	  @Override
		  public void setText(String str){
			  this.setSize(100, 100);
			  super.setText(str);
		  }
   }
   /** call by {@link EventThrowDices}. */
   public void showThrowDices( Player user , int dice_num ){
      System.err.println("玩家: " + user.getName() + " 骰到了 " + dice_num + " 步 XD");

      //TODO 不會寫.. ◢▆▅▄▃崩╰(〒皿〒)╯潰▃▄▅▇◣
	  ui.gameScreenFreeze();
	  menuBarPanel.freezeUI();

	 Rectangle fromRect = env.getMap().getMapData().getShape( user.getBlock() ).getBounds();
      mapPanel.moveCameraTo(fromRect.getX()+fromRect.getWidth()/2, fromRect.getY()+fromRect.getHeight()/2);

	  //移除POPUP_LAYER的東西.
	  Component[] c = popupPane.getComponentsInLayer(JLayeredPane.POPUP_LAYER);
      for(int i = 0; i < c.length; i++)
		  if(c[i]!=u)
	         popupPane.remove(c[i]);
	  popupPane.repaint();

      ImageIcon[] diceImg = new ImageIcon[6];
      for( int i=0 ; i<6 ; ++i )	
         diceImg[i] = new ImageIcon(
	 			 GUIGameTool.getDefaultGameTool().getImage( "data/dice/dice"+(i+1)+".png")
				 );
		
	  Rectangle rs = env.getMap().getMapData().getShape( user.getBlock() ).getBounds();
	  diceL.setXY( (int)rs.getX() + 50, (int)rs.getY() - 50);
	  diceL.relocate();
      popupPane.setLayer(diceL, JLayeredPane.DRAG_LAYER.intValue());

      for(int i=0;i<40;++i){
         diceL.setIcon( diceImg[i%diceImg.length] );
		 try{Thread.sleep(20);}catch(InterruptedException ignore){System.err.println(ignore.toString());}
	  }
	  diceL.setIcon( diceImg[dice_num-1] );
	  try{Thread.sleep(300);}catch(InterruptedException ignore){}
	  afterDiceL.setXY((int)rs.getX()+ 50 + 30, (int)rs.getY() - 50 + 15);
	  afterDiceL.relocate();
	  afterDiceL.setText( Integer.toString(dice_num) );
	  popupPane.setLayer( afterDiceL, JLayeredPane.DRAG_LAYER.intValue());
	  this.flashComponent( afterDiceL, 2, new int[]{150, 150}, 700 );
	  popupPane.setLayer( afterDiceL, new Integer(-100) );
      popupPane.setLayer( diceL , new Integer(-100));
	  ui.gameScreenUnfreeze();
	  menuBarPanel.unfreezeUI();
   }
   //================== 使用者移動相關 =======================

   public Timer playerMovingTimer = new Timer(200, new ActionListenerJumpingPlayer());
   /** call by {@link EventPlayerMove}. */
   public void showPlayerMove( Player user , Block fromBlock , Block toBlock){
      menuBarPanel.freezeUI();
      ui.gameScreenFreeze();

      //如果user不是目前的玩家的話, 可能會出現重複圖片 & 缺少目前玩家的圖片 待修正!
      Image walkingImg = GUIGameTool.getDefaultGameTool().getImage(user.getData().charSetPath);


      u.setIcon(new ImageIcon( walkingImg ));

      Rectangle fromRect = env.getMap().getMapData().getShape(fromBlock).getBounds();
      Rectangle toRect = env.getMap().getMapData().getShape(toBlock).getBounds();
      mapPanel.moveCameraTo(fromRect.getX()+fromRect.getWidth()/2, fromRect.getY()+fromRect.getHeight()/2);
      popupPane.setLayer(u, JLayeredPane.DRAG_LAYER.intValue());
      u.setBounds((int)fromRect.getX()-mapPanel.getTopLeftX(),
            (int)fromRect.getY()-mapPanel.getTopLeftY()+GUIGameTool.SHOW_PLAYER_OFFSET_Y,
            walkingImg.getWidth(popupPane), walkingImg.getHeight(popupPane));
      Component[] c = popupPane.getComponentsInLayer(JLayeredPane.POPUP_LAYER);
      u.setMoving(true);
      for(int i = 0; i < c.length; i++)
         popupPane.remove(c[i]);

      double dx = toRect.getX() - fromRect.getX();
      double dy = toRect.getY() - fromRect.getY();
      int steps = (int) (Math.sqrt(dx*dx+dy*dy)/ GUIGameTool.PLAYER_MOVING_SPEED);
      double tx = dx / steps;
      double ty = dy / steps;
      playerMovingTimer.start();
      for(int i = 0; i <= steps; i++){
         mapPanel.moveCameraTo(fromRect.getX() + fromRect.getWidth()/2 + i * tx, fromRect.getY() + fromRect.getHeight()/2 + i * ty);
         try{Thread.sleep(20);}catch(Exception e){}
      }
      playerMovingTimer.stop();
      if(!user.getStatus().isWalking()) //偷偷看一下player的狀況
         u.setMoving(false);
      ui.gameScreenUnfreeze();
      popupPane.setLayer(u, JLayeredPane.POPUP_LAYER.intValue());
      menuBarPanel.unfreezeUI();
   }
   /** 遊戲節奏暫停. 這部分原本應該屬於 GameTool 的, 不過為了畫面更新效果上先寫在這裡 */
   public void gamePause( int delay_msec ){
	   ui.gameScreenFreeze();
	   menuBarPanel.freezeUI();
	   try{Thread.sleep(delay_msec);}catch(InterruptedException e){System.err.println(e.toString());}
	   menuBarPanel.unfreezeUI();
	   ui.gameScreenUnfreeze();
   }

   private void flashComponent(JComponent c, int times, int[] delay, int finaldelay){
	   for(int i = 0; i < times; i++){
		   c.setVisible(true);
		   try{Thread.sleep(delay[0]);}catch(Exception ignore){}
		   c.setVisible(false);
		   try{Thread.sleep(delay[0]);}catch(Exception ignore){}
	   }
	   c.setVisible(true);
	   try{Thread.sleep(finaldelay);}catch(Exception ignore){}
   }
   
   public void flash(MapComponentVisitable mcv, int times, int delay, int finaldelay){
	   ui.gameScreenFreeze();
	   menuBarPanel.freezeUI();
	   Rectangle rect = env.getMap().getMapData().getShape(mcv).getBounds();
	   JPanel p = new JPanel();
	   p.setOpaque(true);
	   p.setBackground(new Color(255, 255, 50, 80));
	   p.setBounds( (int)rect.getX()-mapPanel.getTopLeftX(), (int)rect.getY()-mapPanel.getTopLeftY(), (int)rect.getWidth(), (int)rect.getHeight() );
	   popupPane.add(p, JLayeredPane.POPUP_LAYER);
	   flashComponent( p, times, new int[]{delay, delay}, finaldelay);
	   popupPane.remove(p);
	   menuBarPanel.unfreezeUI();
	   ui.gameScreenUnfreeze();
   }
   
   private void flashComponent(JComponent []c_arr, int times, int[] delay, int finaldelay){
	   for(int i = 0; i < times; i++){
       for(int j = 0; j < c_arr.length; j++)
		     c_arr[j].setVisible(true);
       try{Thread.sleep(delay[0]);}catch(Exception ignore){}
       for(int j = 0; j < c_arr.length; j++)
		     c_arr[j].setVisible(false);
       try{Thread.sleep(delay[0]);}catch(Exception ignore){}
     }
     for(int j = 0; j < c_arr.length; j++)
		   c_arr[j].setVisible(true);
	   try{Thread.sleep(finaldelay);}catch(Exception ignore){}
   }
   
   public void flash(MapComponentVisitable[] mcv_arr, int times, int delay, int finaldelay){
	   ui.gameScreenFreeze();
	   menuBarPanel.freezeUI();
	   
     Rectangle []rect_arr = new Rectangle[mcv_arr.length];
     JPanel []p_arr = new JPanel[mcv_arr.length];
     for(int i=0;i<mcv_arr.length;i++){
       rect_arr[i] = env.getMap().getMapData().getShape(mcv_arr[i]).getBounds();
       p_arr[i] = new JPanel();
	     p_arr[i].setOpaque(true);
	     p_arr[i].setBackground(new Color(255, 255, 50, 80));
	     p_arr[i].setBounds( (int)rect_arr[i].getX()-mapPanel.getTopLeftX(), (int)rect_arr[i].getY()-mapPanel.getTopLeftY(), (int)rect_arr[i].getWidth(), (int)rect_arr[i].getHeight() );
	     popupPane.add(p_arr[i], JLayeredPane.POPUP_LAYER);

     }
     flashComponent( p_arr, times, new int[]{delay, delay}, finaldelay);
     for(int i=0;i<mcv_arr.length;i++){
	     popupPane.remove(p_arr[i]);
     }
	   menuBarPanel.unfreezeUI();
	   ui.gameScreenUnfreeze();
   }
   
   CoverPanel coveringPanel = new CoverPanel();
   /** 淡出, 出現一個黑色遮罩 */
   public void fadeOut(int delay_msec){
	   coveringPanel.setBounds(0, 0, popupPane.getWidth(), popupPane.getHeight());
	   if(popupPane.getLayer(coveringPanel) > 0)
		   return;
	   ui.gameScreenFreeze();
	   menuBarPanel.freezeUI();
	   popupPane.setLayer(coveringPanel, 800);
	   coveringPanel.setVisible(true);
	   for(int i = 0; i <=10; i++){
		   try{Thread.sleep(delay_msec/20);}catch(Exception e){System.err.println(e);}
		   coveringPanel.alpha = 0.1f*i;
		   coveringPanel.repaint();
	   }
	   try{Thread.sleep(delay_msec/2);}catch(Exception e){System.err.println(e);}
	   menuBarPanel.unfreezeUI();
	   ui.gameScreenUnfreeze();
   }
   /** 淡入, 移除黑色遮罩 */
   public void fadeIn(int delay_msec){
	   if(popupPane.getLayer(coveringPanel) < 0)
		   return;
	   ui.gameScreenFreeze();
	   menuBarPanel.freezeUI();
	   for(int i = 10; i >=0; i--){
		   try{Thread.sleep(delay_msec/10);}catch(Exception e){System.err.println(e);}
		   coveringPanel.alpha = 0.1f*i;
		   popupPane.repaint();
	   }
	   popupPane.setLayer(coveringPanel, -100);
	   coveringPanel.setVisible(false);
	   menuBarPanel.unfreezeUI();
	   ui.gameScreenUnfreeze();
   }
   public class CoverPanel extends JPanel{
	   public float alpha = 1.0f;
	   public CoverPanel(){
		   super();
		   setOpaque(false);
	   }
	   public void paint(Graphics g){
			super.paint(g);
		   Graphics2D g2 = (Graphics2D) g.create();
		   g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
		   g2.setColor(Color.BLACK);
			g2.fillRect(0, 0, getWidth(), getHeight());
		   g2.dispose();
	   }
   }
}

