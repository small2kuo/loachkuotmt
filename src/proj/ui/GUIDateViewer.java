package proj.ui;

import proj.*;
import java.awt.*;
import javax.swing.*;

public class GUIDateViewer extends JPanel{
	GameEnv env;
	Font font = new Font("LiHei pro", 0, 28);
	public GUIDateViewer(GameEnv env){
		this.env = env;
		this.setBounds(640, 0, 200, 200);
		this.setPreferredSize(new Dimension(160, 200));
		this.setOpaque(false);
		this.setFont(font);
		this.setLayout(null);
		this.add(lbl);
		lbl.setBounds(0, 130, 160, 70);
	}
	JLabel lbl = new JLabel(""){
		Font font = new Font("LiHei pro", 0, 28);
		boolean first = true;
		public void paint(Graphics g){
			super.paint(g);
			if(first){first = false; setFont(font);}
			setText(env.getDate().getMonth() + "月" + env.getDate().getDay() + "日");
		}
	};
	public void paint(Graphics g){
		super.paint(g);
		//g.setColor(new Color(155, 255, 255, 155) );
		//g.fillRect(0, 0, getWidth(), getHeight());
		//g.setColor( Color.BLACK );
		
		//g.drawString(env.getDate().getMonth() + "月" + env.getDate().getDay() + "日", 25, 175);
	}
}
