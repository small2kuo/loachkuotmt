package proj.ui;

import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import java.util.ArrayList;

public class GUIGameRoot extends JFrame implements UserInterfaceRoot{
   /** 最一開始的遊戲選單. */
   public int startMenu( String[] opts, String[] imgsources ){
	   Icon[] icons = new Icon[ imgsources.length ];
	   for(int i = 0; i < imgsources.length; i++)
		   icons[i] = new ImageIcon( GUIGameTool.getDefaultGameTool().getImage(imgsources[i]) );
      int action = optionPanel.queryMultiChoiceWithCancel( "這是開始選單" , icons);

      return action;
   } 
   /** 讀入遊戲的選單.
    * TODO 要加上回前頁的選項(return -1) */
   public int showLoadGameList( GameEnvInfo[] eis ){
      return 0;
   }

   /** 在開心遊戲的選單讀入使用者所做的操作.
    * Object[0] = 操作{ Integer(0): 開始遊戲;  Integer(1): 回主選單 }<br>
    * if Object[0] == 0 then<br>
    *    Object[1] = 選擇的MapInfo<br>
    *    Object[2] = 選擇的PlayerData[]<br>
    *    Object[3] = 選擇的使用者的姓名String[]<br>
    * endif
    * */
   public Object[] getNewGameListOption( MapInfo[] maps , PlayerData[] ps ){
      Object[] ret = optionPanel.getNewGameListOption(maps, ps);
      return ret;
   }

   private GameRootOptionPanel optionPanel;
   private JPanel middlePanel;
   private JWindow loadingWindow;

   private UI_EventHandler ehandle;
   private JLayeredPane popupPane;

   /** 傳入 GameEnvironment 進行設定. */
   public GUIGameRoot(){
      super();
      this.setTitle("OOP.S. 之 大富翁沒有假期 之 軟體大亨哼哼哼哼");
      this.setPreferredSize(new Dimension(800, 600));
      this.setBackground(new Color(255,255,255));
      //this.setSize(800, 600);
      this.setLocation(100, 100);
      this.pack();
      this.setLayout(new BorderLayout());
      this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      optionPanel = new GameRootOptionPanel(this);
      this.add(optionPanel, BorderLayout.CENTER );
      new Thread(){public void run(){while(true){
         try{Thread.sleep(10000);}catch(Exception e){}}};}.start();
   }

   public class GameRootOptionPanel extends JPanel implements ActionListener{
      public Color bgc = new Color(200,200,200,200);
      JFrame frame;
      public GameRootOptionPanel(JFrame frame){
         this.frame = frame;
         setLayout(null);
         setOpaque(false);
         setVisible(false);
      }
      public void paint(Graphics g){
         g.setColor(bgc);
         g.fillRect(0, 0, getWidth(), getHeight());
         super.paint(g);
      }
      int chooseNum = -1;
      Object flag = new Object();
      public void actionPerformed(ActionEvent e){
         synchronized(flag){
            chooseNum = Integer.parseInt(e.getActionCommand());
            flag.notifyAll();
         }
      }
      public int queryMultiChoiceWithCancel(String msg, Icon[] icons){
         this.removeAll();
         this.setLayout(new GridLayout(icons.length + 1, 1));
         this.add(new JLabel(msg, JLabel.CENTER));
         for(int i = 0; i < icons.length; i++){
            JButton b = new JButton(icons[i]);
            b.setOpaque(false);
			b.setBackground(new Color(0, 0, 0, 0));
            b.setActionCommand( Integer.toString(i) );
            b.addActionListener(this);
            this.add(b);
         }
         this.setVisible(true);
         validate();
         chooseNum = -1;
         synchronized(flag){
            try{flag.wait();}catch(Exception e){System.err.println(e.toString());}
         }
         System.err.println("HERE end choice!");
         this.setVisible(false);
         this.removeAll();
         setLayout(null);
         return chooseNum;
      }

      int action = -1;
      NewGameOption ngo = null;
      JButton[] bs = null;
      MapInfo[] maps = null;
      PlayerData[] ps = null;
      JPanel chosenPlayerListPanel = null;
      public Object[] getNewGameListOption( MapInfo[] maps , PlayerData[] ps ){
         this.removeAll();
         this.setLayout(new BorderLayout());
         this.maps = maps;
         this.ps = ps;
         ngo = new NewGameOption();
         action = -1;
         
         //最下面的開始遊戲/回到選單
         JPanel buttomPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
         buttomPanel.setOpaque(false);
         JButton gameStartButton = new JButton("開始遊戲");
         NGOActionListener ngoListener = new NGOActionListener();
         gameStartButton.setActionCommand("開始遊戲");
         gameStartButton.addActionListener(ngoListener);
         JButton retMenuButton = new JButton("回到選單");
         retMenuButton.setActionCommand("回到選單");
         retMenuButton.addActionListener(ngoListener);
         buttomPanel.add(gameStartButton);
         buttomPanel.add(retMenuButton);
         this.add(buttomPanel, BorderLayout.SOUTH);

         //左邊的 cMapPanel
         JPanel cMapPanel = new JPanel(new BorderLayout());
         JList mList = new JList(maps);
         mList.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
         mList.addListSelectionListener( ngoListener );
         cMapPanel.add(mList, BorderLayout.CENTER);
         cMapPanel.add(new JLabel("請選擇遊戲地圖:"), BorderLayout.NORTH);

         //右邊的 cPlayerPanel
         JPanel cPlayerPanel = new JPanel(new BorderLayout());
         JPanel pPanelListPanel = new JPanel(new FlowLayout());
         chosenPlayerListPanel = new JPanel(new GridLayout(0, 1));
         cPlayerPanel.add(pPanelListPanel, BorderLayout.CENTER);
         cPlayerPanel.add(chosenPlayerListPanel, BorderLayout.SOUTH);
         bs = new JButton[ps.length];
         for(int i = 0; i < ps.length; i++){
            Image img = GUIGameTool.getDefaultGameTool().getImage( ps[i].avatarPath );
            bs[i] = new JButton(ps[i].name, new ImageIcon(img));
            bs[i].setActionCommand( Integer.toString(i) );
            bs[i].addActionListener( ngoListener );
            pPanelListPanel.add(bs[i]);
         }

         //中間的 JSplitPane
         JSplitPane middlePane = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT, cMapPanel, cPlayerPanel);
         middlePane.setDividerLocation(300);
         this.add(middlePane);


         this.validate();
         this.setVisible(true);
         synchronized(flag){
            while(action == -1)
               try{flag.wait();}catch(Exception e){}
         }
         this.setVisible(false);
         Object[] retobj = new Object[4];
         retobj[0] = new Integer(action);
         retobj[1] = ngo.chosenMapInfo;
         retobj[2] = ngo.chosenPlayerData.toArray( new PlayerData[0] );
         retobj[3] = ngo.playerNames.toArray( new String[0] );
         this.removeAll();
         this.setLayout(null);
         return retobj;
      }
      public class NGOActionListener implements ActionListener, ListSelectionListener{
         int val = 0;
         public void actionPerformed(ActionEvent e){
            String cmd = e.getActionCommand();
            synchronized(flag){
               if("開始遊戲".equals(cmd)){
                  if(ngo.isReadyToPlay()){
                     action = 0;
                     flag.notifyAll();
                  }else{
                     JOptionPane.showMessageDialog(optionPanel, "還沒有設定好喔!", "遊戲訊息", JOptionPane.INFORMATION_MESSAGE);
                  }
               }else if("回到選單".equals(cmd)){
                  action = 1;
                  flag.notifyAll();
               }else{ //選擇玩家
                  val = Integer.parseInt(cmd);
                  String name = (String)JOptionPane.showInputDialog(optionPanel, "請輸入玩家姓名: ", "新增玩家", JOptionPane.QUESTION_MESSAGE, bs[val].getIcon(), null, ps[val].name);
				  if(name == null)
					  return;
                  //TODO: name is null
                  int rtv = JOptionPane.showConfirmDialog(optionPanel, "是否新增玩家?", "新增玩家", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                  if(rtv == JOptionPane.YES_OPTION){
                     ngo.chosenPlayerData.add( ps[val] );
                     ngo.playerNames.add( name );
                     bs[val].setVisible(false); //選擇後要消失
                     GridLayout grid = ((GridLayout) chosenPlayerListPanel.getLayout());
                     grid.setRows( grid.getRows() + 1 );
                     chosenPlayerListPanel.add(new JLabel(name, bs[val].getIcon(), JLabel.CENTER){
                        Color bgc = ps[val].pColor;
						Font myFont = new Font("LiHei pro", 0, 28);	
						boolean first = true;
                        public void paint(Graphics g){
                           Color bg = bgc;
						   if(first){ first = false; setFont(myFont);}
                           int cr=bg.getRed(), cg=bg.getGreen(), cb=bg.getBlue();
                           for(int i = 19; i >= 0; i--){
                              g.setColor(bg);
                              g.fillRect(0, getHeight()*i/20, getWidth(), getHeight()/20+1);
                              //bg = bg.brighter();
                              cr = cr*16/15; cg = cg*16/15; cb = cb*16/15;
                              if(cr>255) cr=255; if(cg>255) cg=255; if(cb>255) cb=255;
                              bg = new Color(cr, cg, cb);
                           }
                           //g.fillRect(0, getHeight()/2, getWidth(), getHeight()/2);
                           super.paint(g);
                        }
                     });
                  }
               }
            }
         }
         public void valueChanged(ListSelectionEvent e){
            System.err.println("你選的是: " + maps[ ((JList) e.getSource()).getSelectedIndex() ]);
            ngo.chosenMapInfo = maps[ ((JList) e.getSource()).getSelectedIndex() ];
         }
      }
   }
   public class NewGameOption{
      public MapInfo chosenMapInfo = null;
      public ArrayList<PlayerData> chosenPlayerData = new ArrayList<PlayerData>();
      public ArrayList<String> playerNames = new ArrayList<String>();
      
      public boolean isReadyToPlay(){
         if(chosenMapInfo ==  null || chosenPlayerData.size() < 2)
            return false;
         return true;
      }
   }
}
