package proj.ui;

import java.awt.*;
import java.io.*;
import javax.swing.*;
import java.util.HashMap;

/** 遊戲進行時關於GUI的工具 */
public class GUIGameTool{
	static GUIGameTool defaultGameTool = new GUIGameTool();
	private HashMap<String, Image> allImages = new HashMap<String, Image>();
	private JPanel p = new JPanel();
	static public GUIGameTool getDefaultGameTool(){
		return defaultGameTool;
	}
	/** 載入圖片，在確認完全載入之前不會回傳 */
	public Image getImage(String filename){
		if(filename == null) return null;
		if(allImages.get(filename) != null)
			return allImages.get(filename);
		System.err.print("[GUIGameTool] Loading Image: " + filename + "...");
		Image img = Toolkit.getDefaultToolkit().createImage(filename); //FILE Exists?
		if(img == null)
			return null;
		while(!Toolkit.getDefaultToolkit().prepareImage(img, -1, -1, p)){
			try{Thread.sleep(50);}catch(Exception e){}
			System.err.print(".");
		}
		allImages.put(filename, img);
                System.err.println("Loading Successful!");
		return img;
	}
	/** 載入圖片並且指定大小 */
	public Image getImage(String filename, int w, int h){
		if(allImages.get(filename) != null)
			return allImages.get(filename);
		Image img = getImage(filename);
		double scaleX = (double)w/(double)img.getWidth(p);
		double scaleY = (double)h/(double)img.getHeight(p);
		double ratio = scaleX<scaleY? scaleX:scaleY;
		System.err.println("[GUIGameTool]: Scaling Ratio = " + ratio);
		img = img.getScaledInstance((int)(ratio*img.getWidth(p)), (int)(ratio*img.getHeight(p)), Image.SCALE_REPLICATE);
		allImages.put(filename, img);
		return img;
	}
	//============== 公共的 optionPanel =============
	private GUIOptionPanel optionPanel = null;
	/** Called by GUIRoot at the very beginning. */
	public void setDefaultOptionPanel(GUIOptionPanel o){
		optionPanel = o;
	}
	public GUIOptionPanel getDefaultOptionPanel(){
		return optionPanel;
	}
	//============== 公共的 ehandle =================
	private UI_EventHandler ehandle = null;
	public void setDefaultEventHandler(UI_EventHandler e){
		ehandle = e;
	}
	public UI_EventHandler getDefaultEventHandler(){
		return ehandle;
	}

	// static variables
	public static final int SHOW_PLAYER_OFFSET_Y = -30;
	public static final double GAMESPEED_NORMAL = 3.0;
	public static final double GAMESPEED_FAST = 8.0;
	public static final double GAMESPEED_SLOW = 2.0;
	public static double PLAYER_MOVING_SPEED = GAMESPEED_FAST;
	public static int SCREEN_DRAG_SPEED = 1;

	public static Color SYSTEM_MSG_BGCOLOR = new Color(244, 233, 127, 200);
}
