package proj.ui;

import proj.*;
import proj.map.*;
import proj.map.building.*;

import java.awt.*;
import javax.swing.*;

/**
 * 負責顯示物件資訊的 Panel, 處理了 Street, Block, Land 等等.
 * 希望還可以處理 Item 資訊, Player 資訊等
 */
public class GUIInfoPanel extends JPanel implements GUIRelocatable{
	JPanel innerPanel;
	Shape s;
	Color panelColor = new Color(244, 233, 127, 200);
	public GUIInfoPanel(Land land, Shape shape, int topLeftCornerX, int topLeftCornerY){
		super(new BorderLayout());
		this.s = shape;
		this.setOpaque(false);
		innerPanel = new GUILandInfo(land);
		this.setSize(innerPanel.getWidth(), innerPanel.getHeight());
		this.add(innerPanel, BorderLayout.CENTER);
		relocate(topLeftCornerX, topLeftCornerY);
	}
	public void relocate(int topLeftCornerX, int topLeftCornerY){
		this.setBounds((int)(s.getBounds().getX()+s.getBounds().getWidth()/2)-topLeftCornerX,
				(int)(s.getBounds().getY()+s.getBounds().getHeight()/4-this.getHeight())-topLeftCornerY,
				this.getWidth(),
				this.getHeight());
	}
	public void paint(Graphics g){
		g.setColor(panelColor);
		g.fill3DRect(0, 0, getWidth(), getHeight(), true);
		super.paint(g);
	}
	/** 顯示關於 Land 的資訊 */
	public class GUILandInfo extends JPanel{
		Land land;
		public GUILandInfo(Land land){
			super(new BorderLayout());
			this.land = land;
			this.setOpaque(false);
			this.setSize(100, 75);
			this.add(new JLabel(land.getStreet().getName(), JLabel.CENTER){
				public void paint(Graphics g){
					g.setColor(new Color(100, 100, 100, 200));
					g.drawLine(0, getHeight()-1, getWidth(), getHeight()-1);
					super.paint(g);
				}
				}, BorderLayout.NORTH);
			Building b = land.getBuilding();
			if(land instanceof Buyable && ((Buyable) land).getOwner() != null){
				panelColor = new Color(((Buyable) land).getOwner().getData().pColor.brighter().brighter().getRGB() + (200 * (1<<24)), true);
			}
			String cstr = "<html>";
			if(b == null){
				cstr += "&nbsp;空地";
			}else{
				if(b instanceof Upgradable)
					cstr += "&nbsp;" + b.getBuildingData().name + " (等級: " + ((Upgradable) b).getLevel() + ")";
				else
					cstr += "&nbsp;" + b.getBuildingData().name;
			}
			if(land instanceof LandEmpty && ((Buyable) land).getOwner() != null){
				cstr += "<br>&nbsp;過路費: $" + ((LandEmpty) land).calcToll(null);
			}
			cstr += "</html>";
			this.add(new JLabel(cstr), BorderLayout.CENTER);
			if(land instanceof Buyable && ((Buyable) land).getOwner() != null){
				this.add(new JLabel(" 擁有者: " + ((Buyable) land).getOwner().getName()), BorderLayout.SOUTH);
			}
		}
	}
}
