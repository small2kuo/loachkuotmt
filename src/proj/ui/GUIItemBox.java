package proj.ui;

import proj.item.*;
import proj.player.*;
import proj.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/** 顯示 Player 的 Item */
public class GUIItemBox extends JScrollPane implements ActionListener{
	Item choosedItem = null;
	Player p;
	JPanel panel;
	GUIOptionPanel o;
	public GUIItemBox(Player p, JPanel panel, GUIOptionPanel o){
		super(panel);
		this.setOpaque(false);
		this.panel = panel;
		this.o = o;
		//panel.setBgcolor
		panel.setLayout(new FlowLayout( FlowLayout.LEFT ));
		//panel.setPreferredSize(new Dimension(480, 280));
		panel.setOpaque(false);
		this.setPreferredSize(new Dimension(600, 350));
		this.addMouseListener(o);
		//iList = p.getAllItems();
		this.p = p;
		Item[] iList = p.getAllItems();
		for(int i = 0; i < iList.length; i++){
			Image img = GUIGameTool.getDefaultGameTool().getImage(iList[i].getData().handImgPath);
			JButton b = new JButton(new ImageIcon(img));
			b.setOpaque(false);
			b.setBackground( Color.BLUE );
			b.setActionCommand( Integer.toString(i) );
			b.addActionListener(this);
			b.setToolTipText(iList[i].getData().name);
			panel.add(b);
		}
	}
	public void actionPerformed(ActionEvent e){
		synchronized(o){
			try{
				choosedItem = p.getAllItems()[ Integer.parseInt(e.getActionCommand()) ];
				//這邊再看看要怎麼改，先取消了
				/*int r = JOptionPane.showConfirmDialog(this, "是否使用 " + choosedItem + "?", "使用道具/卡片", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if(r != JOptionPane.YES_OPTION)
					choosedItem = null;*/
			}catch(Exception ignore){
			} //陣列超出邊界? 理論上不應該發生.
			System.err.println("choosedItem: " + choosedItem);
			o.notifyAll();
		}
	}
	
	public Item getChoosedItem(){
		return choosedItem;
	}
}
