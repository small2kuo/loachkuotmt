package proj.ui;

import proj.*;
import proj.item.*;
import proj.map.*;
import proj.map.building.*;
import proj.player.*;

import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.HashMap;

/** 這個是顯示地圖用的 Panel */
public class GUIMapPanel extends JPanel implements MouseListener, MouseMotionListener, MapComponentVisitor, MapUpdateObserver{
	private GUIMapPanel thisPanel;
	private GameEnv env;
	private UI_EventHandler ehandle;
	private Image bg_img;
	private Map map;
	private int cntX, cntY;
	private Timer timer;
	private JLayeredPane popupPane;
	private Image bgImageNew;
	private boolean first = true;
	public GUIMapPanel(GameEnv env, UI_EventHandler ehandle, JLayeredPane popupPane){
		super(new BorderLayout());
		this.popupPane = popupPane;
		this.thisPanel = this;
		this.env = env;
		this.ehandle = ehandle;
		this.setOpaque(false);

		map = this.env.getMap();
		bg_img = GUIGameTool.getDefaultGameTool().getImage( map.getMapData().bgImgSrc );
		cntX = 1500;
		cntY = 1200;
		mouseX = mouseY = 0;
		this.addMouseListener(this);
		this.addMouseMotionListener(this);

		//向 GameTool 註冊畫面的更新
		GameTool.addMapUpdateObserver(this);

		//this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		timer = new Timer(30, new ActionListenerRollScreen());
		timer.start();
	}
	public Timer getTimer(){
		return timer;
	}
	public int getTopLeftX(){
		return cntX-getWidth()/2;
	}
	public int getTopLeftY(){
		return cntY-getHeight()/2;
	}
	public void paint(Graphics g){

		//System.err.println(bg_img + " " + cntX + " " + cntY + " " + getWidth() + " " + getHeight());
		//第一次畫圖會把街道等背景畫上去
		if(first){
			if(bgImageNew == null){
				if(bg_img != null)
					bgImageNew = new BufferedImage(bg_img.getWidth(this), bg_img.getHeight(this), BufferedImage.TYPE_INT_ARGB );
				//createImage( bg_img.getWidth(this), bg_img.getHeight(this) );
				else
					bgImageNew = new BufferedImage(800, 600, BufferedImage.TYPE_INT_ARGB );
				//createImage( 800, 600 );
			}
			Graphics2D g2 = (Graphics2D) bgImageNew.getGraphics();
			//g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.9f));
			g2.drawImage(bg_img, 0, 0, this);
			for(int i = map.getMapData().allShapes.size()-1; i>=0; i--){
				Shape shape = map.getMapData().allShapes.get(i);
				MapComponentVisitable mcv = map.getMapData().hashmap.get( shape );
				mcv.accept(this, shape, g2);
			}
			first = false;
		}
		checkCameraRange();
		int LX = cntX - getWidth()/2;
		int LY = cntY - getHeight()/2;
		g.drawImage(bgImageNew, 
				0, 0, getWidth(), getHeight(),
				LX, LY, LX +getWidth(), LY + getHeight(),
				this);
		super.paint(g);
		//TODO: Sort the shapes by their coordinates.(這個部分在MapEditor裡面處理吧!)
		//畫出一些活動物件
		for(int i = map.getMapData().allShapes.size()-1; i>=0; i--){
			Shape shape = map.getMapData().allShapes.get(i);
			MapComponentVisitable mcv = map.getMapData().hashmap.get( shape );
			mcv.accept(this, shape, g);
		}

	}

	//========= MapComponentVisitor =========
	private HashMap<Shape, GUIInfoPanel> mouseMoveGenPanels = new HashMap<Shape, GUIInfoPanel>();
	private Shape mouseVisitShape;
	private MouseEvent mouseEvent;
	/** 當滑鼠移到某個 Street 上面的時候，會顯示的東西。 */
	public void visit(Street street){
		//理論上會是什麼事都沒發生吧XD"
	}
	/** 當滑鼠移到某個 Block 上面的時候，會顯示的東西。(顯示包括上面的人事物) */
	public void visit(Block block){
		if( querying == STATUS_QUERYING_BLOCK && block.canPutStandable( (Standable) iLabel.getSource() ) == true){
			if(iLabel != null){
				iLabel.setXY( (int)map.getMapData().getShape(block).getBounds().getX(),
						(int)map.getMapData().getShape(block).getBounds().getY() + GUIGameTool.SHOW_PLAYER_OFFSET_Y );
				iLabel.relocate(getTopLeftX(), getTopLeftY());
				iLabel.setCovering(block, map.getMapData().getShape(block));
			}
		}else if(querying == STATUS_QUERYING_PLAYER){
			Standable[] sarr = block.getStandable();
			for(int i = sarr.length-1; i>=0 ; i--) //只抓到最上層且不等於iLabel.sourceObj的人
				if(sarr[i] instanceof Player){
					if(iLabel != null && iLabel.getSource() != sarr[i]){
						iLabel.setXY( (int)map.getMapData().getShape(block).getBounds().getX(),
							(int)map.getMapData().getShape(block).getBounds().getY() + GUIGameTool.SHOW_PLAYER_OFFSET_Y );
						iLabel.relocate(getTopLeftX(), getTopLeftY());
						iLabel.setCovering(sarr[i], map.getMapData().getShape(block));
						break;
					}
				}
		}
	}
	/** 當滑鼠移到某個 Land 上面的時候，會顯示的東西。(顯示包括土地上的房子狀況等) */
	public void visit(Land land){
		if(querying == STATUS_QUERYING_UPGRADABLE){
			if(land.getBuilding() instanceof Upgradable){
				if(iLabel != null){
					iLabel.setXY( (int)map.getMapData().getShape(land).getBounds().getX()
							+(int)map.getMapData().getShape(land).getBounds().getWidth()/2-iLabel.getWidth()/2,
							(int)map.getMapData().getShape(land).getBounds().getY()
						   +(int)map.getMapData().getShape(land).getBounds().getHeight()/2-iLabel.getHeight()/2);
					iLabel.relocate(getTopLeftX(), getTopLeftY());
					iLabel.setCovering(land.getBuilding(), map.getMapData().getShape(land));
				}
			}
		}else if(querying == STATUS_QUERYING_LANDEMPTY){
			if(land instanceof LandEmpty){
				if(iLabel != null){
					iLabel.setXY( (int)map.getMapData().getShape(land).getBounds().getX()
							+(int)map.getMapData().getShape(land).getBounds().getWidth()/2-iLabel.getWidth()/2,
							(int)map.getMapData().getShape(land).getBounds().getY()
						   +(int)map.getMapData().getShape(land).getBounds().getHeight()/2-iLabel.getHeight()/2);
					iLabel.relocate(getTopLeftX(), getTopLeftY());
					iLabel.setCovering(land, map.getMapData().getShape(land));
				}
			}
		}else{
			//顯示土地或土地上的房子狀況，但是在選擇房屋的時候不會顯示。
			GUIInfoPanel p = new GUIInfoPanel(land, mouseVisitShape, (cntX-getWidth()/2), (cntY-getHeight()/2));
			popupPane.add(p, JLayeredPane.POPUP_LAYER, 0); //加入至最上層.
			mouseMoveGenPanels.put(mouseVisitShape, p);
		}
	}
	static Stroke STROKE_MAP_STREET =
		new BasicStroke(10.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 10.0f, new float[]{15.0f, 25.0f}, 0.0f);
	static Color COLOR_MAP_STREET = new Color(55,55,255);
	/** 把 Street 畫在 MapPanel 上面 */
	public void visit(Street street, Shape shape, Graphics g){
		if(first){
			//只有第一次才需要畫
			Graphics2D g2 = (Graphics2D) g;
			g2.setStroke( STROKE_MAP_STREET );
			g2.setColor( COLOR_MAP_STREET );
			g2.draw( shape );
		}
	}
	static Stroke STROKE_MAP_BLOCK = new BasicStroke(2.0f);
	static Color COLOR_MAP_BLOCK = new Color(55, 55, 255);
	/** 把 Block 以及 Block 上面的 Standable 畫在 MapPanel 上面 */
	public void visit(Block block, Shape shape, Graphics g){
		Graphics2D g2 = (Graphics2D) g;
		if(first){
			//只有第一次才需要畫
			g2.setStroke( STROKE_MAP_BLOCK );
			g2.setColor( COLOR_MAP_BLOCK );
			g2.fill( shape );
		}else{
			Standable[] iList = block.getStandable();
			for(int i = 0; i < iList.length; i++){
				if(iList[i] instanceof Player){
					Player p = (Player) iList[i];
					//如果是正在進行該回合的玩家，就不要畫上去，交給 u (playerMovingPanel) 處理
					if(p != env.getNowPlayer() && p.getStatus().getState() != Status.DEAD){
						Image img = GUIGameTool.getDefaultGameTool().getImage(p.getData().charSetPath);
						g2.drawImage(img, (int)shape.getBounds().getX() - (cntX-getWidth()/2), (int)shape.getBounds().getY() -(cntY-getHeight()/2) + GUIGameTool.SHOW_PLAYER_OFFSET_Y, this);
					}
				}else if(iList[i] instanceof Item){
               Item item = (Item) iList[i];
               Image img = GUIGameTool.getDefaultGameTool().getImage(item.getData().groundImgPath);
               g2.drawImage(img,(int)shape.getBounds().getX() - (cntX-getWidth()/2), (int)shape.getBounds().getY() -(cntY-getHeight()/2) + GUIGameTool.SHOW_PLAYER_OFFSET_Y, this);
            }
			}
		}
	}
	static Stroke STROKE_MAP_LAND =
		new BasicStroke(2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	//  	new BasicStroke(2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10.0f, new float[]{7.0f, 7.0f}, 0.0f);
	static Color COLOR_MAP_LAND_DARK = new Color(55, 55, 55, 155);
	static Color COLOR_MAP_LAND_LIGHT = new Color(185, 185, 185, 155);
	static AlphaComposite COMPOSITE_TRANSP50 = AlphaComposite.getInstance( AlphaComposite.SRC_OVER, 0.5f );

	/** 把 Land 以及 Land 上面的 Building 畫在 MapPanel 上面 */
	public void visit(Land land, Shape shape, Graphics g){
		Graphics2D g2 = (Graphics2D) g;
		if(first){
			//只有第一次才需要畫
         if(!(land instanceof LandPublic)){
	   		g2.setStroke( STROKE_MAP_LAND );
	   		g2.setColor( COLOR_MAP_LAND_DARK );
	   		g2.draw( AffineTransform.getTranslateInstance(+2,+2).createTransformedShape(shape) );
	   		g2.setColor( COLOR_MAP_LAND_LIGHT );
   			g2.draw( shape );
         }
			if(land instanceof Buyable){
				Buyable b = (Buyable) land;
				if(b.getOwner() != null){
					g2.setStroke( STROKE_MAP_LAND );
					g2.setColor( b.getOwner().getData().pColor );
					Composite ac = g2.getComposite();
					g2.setComposite( COMPOSITE_TRANSP50 );
					g2.fill( shape );
					g2.setComposite( ac );
					g2.draw( shape );
				}else{
					g2.setColor( COLOR_MAP_LAND_LIGHT );
					Composite ac = g2.getComposite();
					g2.setComposite( COMPOSITE_TRANSP50 );
					g2.fill( shape );
					g2.setComposite( ac );
				}
			}
			if(land.getBuilding() != null){
				Building b = land.getBuilding();
				Image buildImg;
				if(b instanceof Upgradable)
					buildImg = GUIGameTool.getDefaultGameTool().getImage(
							b.getBuildingData().imgPath[ ((Upgradable) b).getLevel() ]
							);
				else
					buildImg = GUIGameTool.getDefaultGameTool().getImage(
							b.getBuildingData().imgPath[1]						//Default只有一張圖
							);
				g2.drawImage(buildImg, (int)shape.getBounds().getX(),
						(int)shape.getBounds().getY(), this);
			}
		}
		
	}

	//========= MouseListener ===========
	private int mouseX, mouseY;
	public void mouseClicked(MouseEvent e){
		if(querying != STATUS_QUERYING_NONE){
			if(e.getButton() == MouseEvent.BUTTON3) //按下右鍵代表取消.
				ohandle.setObj( UI_ObjHandler.MSG_CANCEL );
			else if(iLabel != null && iLabel.getCoveringObj() != null)
				ohandle.setObj(iLabel.getCoveringObj());
		}
	}
	public void mousePressed(MouseEvent e){
		mouseX = e.getX();
		mouseY = e.getY();
	}
	public void mouseReleased(MouseEvent e){
		if(cntX < getWidth()/2) cntX = getWidth()/2;
		if(cntY < getHeight()/2) cntY = getHeight()/2;
	}
	public void mouseEntered(MouseEvent e){}
	public void mouseExited(MouseEvent e){moving=false;}

	//========= MouseMotionListener ==========
	synchronized public void mouseDragged(MouseEvent e){
    if(querying == STATUS_QUERYING_NONE){
   		cntX -= (e.getX()-mouseX) * GUIGameTool.SCREEN_DRAG_SPEED;
   		cntY -= (e.getY()-mouseY) * GUIGameTool.SCREEN_DRAG_SPEED;
   		checkCameraRange();
   		mouseX = e.getX();
   		mouseY = e.getY();
   		movePopupPanel(cntX-getWidth()/2, cntY-getHeight()/2); //移動彈跳層
   		this.repaint();
      }else{
         mouseMoved(e); //Query東西的時候視同 mouseMoved????
      }
	}
	static int MOUSEMARGIN = 30;
	static int CAMERAMOVE = 8;
	boolean moving = false;
	int screenMoveX = 0, screenMoveY = 0;
	int mouseRealX = 0, mouseRealY = 0;
	public void mouseMoved(MouseEvent e){
		mouseX = e.getX();
		mouseY = e.getY();
		mouseRealX = cntX - getWidth()/2 + e.getX();
		mouseRealY = cntY - getHeight()/2 + e.getY();

      if(querying == STATUS_QUERYING_NONE){ //Query的時候全部都不能動, 要動就把這行註解掉
   		if(e.getX() > getWidth() - MOUSEMARGIN){
   			screenMoveX = CAMERAMOVE;
   		}else if(e.getX() < MOUSEMARGIN){
   			screenMoveX = -CAMERAMOVE;
   		}else
   			screenMoveX = 0;
   		if(e.getY() > getHeight() - MOUSEMARGIN){
   			screenMoveY = CAMERAMOVE;
   		}else if(e.getY() < MOUSEMARGIN){
   			screenMoveY = -CAMERAMOVE;
   		}else
   			screenMoveY = 0;
   		moving = (screenMoveX != 0) || (screenMoveY != 0);
      }
		boolean flagScreenChanged = false;
		for(int i = 0; i < map.getMapData().allShapes.size(); i++){
			JPanel p = mouseMoveGenPanels.get( map.getMapData().allShapes.get(i) );
			if(p != null){
				if(!map.getMapData().allShapes.get(i).contains(mouseRealX, mouseRealY)){
					if(p.getParent()!=null)
						p.getParent().remove(p);
					mouseMoveGenPanels.remove( map.getMapData().allShapes.get(i) );
					flagScreenChanged = true;
				}
			}else if(map.getMapData().allShapes.get(i).contains(mouseRealX, mouseRealY)){
				MapComponentVisitable c = map.getMapData().hashmap.get( map.getMapData().allShapes.get(i) );
				mouseEvent = e;
				mouseVisitShape = map.getMapData().allShapes.get(i);
				c.accept(this);
				flagScreenChanged = true;
			}
		}
		mouseMotionCheckQuerying();
		if(flagScreenChanged)
			this.repaint();
	}
   private void mouseMotionCheckQuerying(){
      //如果是正在詢問的狀態, 那麼圖片要跟隨著滑鼠移動位置
		if(querying != STATUS_QUERYING_NONE && iLabel != null){
			if(iLabel.getCoveringShape() != null){
				if(iLabel.getCoveringShape().contains(mouseRealX, mouseRealY) == false){
					iLabel.resetCovering();
					iLabel.setXY(mouseRealX-iLabel.getWidth()/2, mouseRealY-iLabel.getHeight()/2);
					iLabel.relocate(getTopLeftX(), getTopLeftY());
				}
			}else{
				iLabel.setXY(mouseRealX-iLabel.getWidth()/2, mouseRealY-iLabel.getHeight()/2);
				iLabel.relocate(getTopLeftX(), getTopLeftY());
			}
		}
   }
	public class ActionListenerRollScreen implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(moving && querying == STATUS_QUERYING_NONE){ //Query的時候不能動.
				cntX += screenMoveX;
				cntY += screenMoveY;
				checkCameraRange();
				movePopupPanel(cntX-getWidth()/2, cntY-getHeight()/2); //移動彈跳層
				repaint();
			}
		}
	}
	//之後要改掉, 因為例如轉移視角要call別的function
	public void updateNewRound(){
		Player p = env.getNowPlayer();
		if(p != null){
			//取得玩家位置, 把畫面轉移到中心
			Block b = p.getBlock();
			Shape s = map.getMapData().getShape(b);
			if(s != null)
				moveCameraTo((s.getBounds().getX()+s.getBounds().getWidth()/2), (s.getBounds().getY()+s.getBounds().getHeight()/2));
		}
		updateUI();
	}
	public void movePopupPanel(int x,int y){
		//Component[] c = popupPane.getComponentsInLayer( JLayeredPane.POPUP_LAYER );
		Component[] c = popupPane.getComponents();
		for(int i = 0; i < c.length; i++){
			if(c[i] instanceof GUIRelocatable)
				((GUIRelocatable) c[i]).relocate(cntX-getWidth()/2, cntY-getHeight()/2);
		}
	}
	public void moveCameraTo(double x,double y){
		cntX = (int)x;
		cntY = (int)y;
		//checkCameraRange(); This should not happen because the map should be large enough!
		movePopupPanel(cntX-getWidth()/2, cntY-getHeight()/2); //移動彈跳層
		repaint();
	}
	public void moveCameraTo(Rectangle r){
		moveCameraTo(r.getX() + r.getWidth()/2.0, r.getY() + r.getHeight()/2.0);
	}
	public void checkCameraRange(){
		if(bg_img != null){
			if(cntX > bg_img.getWidth(thisPanel) - getWidth()/2) cntX = bg_img.getWidth(thisPanel) - getWidth()/2;
			if(cntY > bg_img.getHeight(thisPanel) - getHeight()/2) cntY = bg_img.getHeight(thisPanel) - getHeight()/2;
			if(cntX < getWidth()/2) cntX = getWidth()/2;
			if(cntY < getHeight()/2) cntY = getHeight()/2;
		}
	}
	public void updateUI(){
		if(env != null && env.getNowPlayer() != null)
			moveCameraTo( env.getMap().getMapData().getShape( env.getNowPlayer().getBlock() ).getBounds() );
		super.updateUI();
	}
	public void updateViewMap(){
		first = true;
		this.repaint();
	}

	public class QueryUsedListener implements MouseListener, MouseMotionListener{
		public void mouseClicked(MouseEvent e){}
		public void mouseEntered(MouseEvent e){}
		public void mouseExited(MouseEvent e){}
		public void mousePressed(MouseEvent e){}
		public void mouseReleased(MouseEvent e){}
		public void mouseDragged(MouseEvent e){}
		public void mouseMoved(MouseEvent e){}
	}
	public static int STATUS_QUERYING_NONE = 0;
	public static int STATUS_QUERYING_BLOCK = 1;
	public static int STATUS_QUERYING_PLAYER = 2;
	public static int STATUS_QUERYING_UPGRADABLE = 3;
	public static int STATUS_QUERYING_LANDEMPTY = 4;
	private int querying = STATUS_QUERYING_NONE;
	private UI_ObjHandler ohandle = new UI_ObjHandler();
	GUIQueryingLabel iLabel = null;
	public Block queryChooseBlock( GUIQueryingLabel itemLabel ){
		return (Block) queryChooseSomething(itemLabel, STATUS_QUERYING_BLOCK);
	}
	public Upgradable queryChooseUpgradable( GUIQueryingLabel itemLabel ){
		return (Upgradable) queryChooseSomething(itemLabel, STATUS_QUERYING_UPGRADABLE);
	}
	public LandEmpty queryChooseLandEmpty( GUIQueryingLabel itemLabel ){
		return (LandEmpty) queryChooseSomething(itemLabel, STATUS_QUERYING_LANDEMPTY);
	}
	/** TODO: [BUG] 只能夠選擇同一格中比較上層(Standable index比較後面的, 後發先至的) 的Player */
	public Player queryChoosePlayer( GUIQueryingLabel itemLabel){
		return (Player) queryChooseSomething(itemLabel, STATUS_QUERYING_PLAYER);
	}
	
	private Object queryChooseSomething( GUIQueryingLabel itemLabel, int querytype ){
		iLabel = itemLabel;
		ohandle.resetObj();
		querying = querytype;
		synchronized(ohandle){
			while(ohandle.getObj() == null)
				try{ohandle.wait();}catch(Exception e){ System.err.println(e.toString());}
		}
		querying = STATUS_QUERYING_NONE;
		iLabel = null;
		//如果取消:
		if(UI_ObjHandler.MSG_CANCEL.equals( ohandle.getObj() ))
			ohandle.resetObj();
		return ohandle.getObj();
	}
	
}
