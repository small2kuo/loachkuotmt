package proj.ui;

import proj.*;
import proj.map.*;

import java.awt.*;
import javax.swing.*;

/** 這個是顯示地圖下方選單列用的 Panel */
public class GUIMenuBarPanel extends JPanel{
	private GameEnv env;
	Image bg_img;
	
	private GUIPlayerStatusViewerSimple pViewer;
	private GUIMenuLister menuLister;
	private GUIDateViewer dateViewer;
	private UI_EventHandler ehandle;

	public GUIMenuBarPanel(GameEnv env, UI_EventHandler ehandle){
		this.env = env;
		this.ehandle = ehandle;
		this.setOpaque(false);
		this.setLayout(new BorderLayout());
		bg_img = GUIGameTool.getDefaultGameTool().getImage("data/sysimg/bg_MenuBarPanel.png");
		this.pViewer = new GUIPlayerStatusViewerSimple(env);
		this.add(pViewer, BorderLayout.WEST);
		this.menuLister = new GUIMenuLister(env);
		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 130));
		p.setOpaque(false);
		p.add(menuLister);
		this.add(p, BorderLayout.CENTER);
		this.dateViewer = new GUIDateViewer(env);
		this.add(dateViewer, BorderLayout.EAST);
	}
	/**
	 * 預設把背景畫上去
	 */
	@Override
	public void paint(Graphics g){
		g.drawImage(bg_img, 0, 0, getWidth(), getHeight(), 0, 0, bg_img.getWidth(this), bg_img.getHeight(this), this);
		super.paint(g);
	}
	public void updateNewRound(){
		updateUI();
	}
	public void updateViewPlayer(){
		pViewer.repaint();
	}
   /** 理論上讓滑鼠點下去不會有反應 */
   public void freezeUI(){
      menuLister.removeMouseListener(menuLister);
      menuLister.removeMouseMotionListener(menuLister);
   }
   /** 恢復原本的效果 */
   public void unfreezeUI(){
      menuLister.addMouseListener(menuLister);
      menuLister.addMouseMotionListener(menuLister);
   }
}

