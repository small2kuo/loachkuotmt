package proj.ui;

import proj.*;
import proj.item.*;
import proj.event.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class GUIMenuLister extends JPanel implements MouseListener, MouseMotionListener{
	GameEnv env;
	ArrayList<MenuIconLabel> iLabel;
	String[] STR = {"查看道具", "玩家資料", "存檔", "讀檔", "遊戲設定", "離開遊戲"};
	String[] imgStr = {"data/sysimg/sys_useitem.png", "data/sysimg/sys_status.png", "data/sysimg/sys_savefile.png", "data/sysimg/sys_loadfile.png", "data/sysimg/sys_config.png", "data/sysimg/sys_exit.png"};
	public GUIMenuLister(GameEnv env){
		this.env = env;
		this.setBounds(200, 100, 300, 100);
		
		this.setPreferredSize(new Dimension(300, 60));
		this.setMinimumSize(new Dimension(300, 60));
		this.setOpaque(false);
		this.setLayout(null);
		iLabel = new ArrayList<MenuIconLabel>();
		for(int i = 0; i < 6; i++){
			iLabel.add( new MenuIconLabel(20+47*i+20, 10+20, 40, 40, STR[i], imgStr[i]) );
			//iLabel.get(i).setToolTipText(STR[i]);
			this.add(iLabel.get(i));
		}
		this.revalidate();
		this.repaint();
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
	}

	/* 這邊會有 Icon List, 然後要做一些事情OQ(模擬Mac動畫效果) */

	public void paint(Graphics g){
		//g.setColor(new Color(255, 255, 155, 155) );
		//g.fillRect(0, 0, getWidth(), getHeight());
		super.paint(g);
	}

	class MenuIconLabel extends JPanel{
		Color bgc;
		Image img;
		String str;
		int absX, absY, defW, defH; //這裡的座標是正中心!
		MenuIconLabel(int absX, int absY, int defW, int defH, String str, String imgstr){
			super();
			this.setOpaque(false);
			this.absX = absX;
			this.absY = absY;
			this.defW = defW;
			this.defH = defH;
			this.setBounds(absX-defW/2, absY-defH/2, defW, defH);
			this.str = str;
			//this.bgc = new Color(GameTool.rnd(0, 256), GameTool.rnd(0, 256), GameTool.rnd(0, 256));
			if(imgstr!= null)
				img = GUIGameTool.getDefaultGameTool().getImage(imgstr);
			else
				img = null;
			this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		}
		public void setDefaultBounds(){
			this.setBounds(absX-defW/2, absY-defH/2, defW, defH);
		}
		public void paint(Graphics g){
			//g.setColor( bgc );
			//g.fill3DRect(0, 0, getWidth(), getHeight(), true);
			if(img != null)
				g.drawImage(img, 0, 0, getWidth(), getHeight(), this);
			super.paint(g);
		}
		GUIItemBox gb = null;
		public void click(){
			System.err.println("你按到的是 " + str );
			if(STR[0].equals(str)){ //查看道具
				new Thread(){
					synchronized public void run(){
						Item item = GUIGameTool.getDefaultGameTool().getDefaultOptionPanel().showPlayerItemBox( env.getNowPlayer(), true );
						System.err.println("GUIMenuLister: HERE item=" + item);
						if(item != null){
							if(item instanceof ItemBuyable){
								UI_EventHandler ehandle = GUIGameTool.getDefaultGameTool().getDefaultEventHandler();
								if(ehandle.isListening())
								ehandle.setEvent(((ItemBuyable) item).use());
							}						
						}
					}
				}.start();
			}else if(STR[1].equals(str)){ //查看玩家資料
				new Thread(){
					synchronized public void run(){
						GUIGameTool.getDefaultGameTool().getDefaultOptionPanel().showPlayerStatusBox( env.getNowPlayer() );
					}
				}.start();
			}else if(STR[2].equals(str)){ //存檔
				new Thread(){
					synchronized public void run(){
						UI_EventHandler ehandle = GUIGameTool.getDefaultGameTool().getDefaultEventHandler();
						if(ehandle.isListening())
							ehandle.setEvent( new EventSaveGame() );
					}
				}.start();
			}else if(STR[3].equals(str)){ //讀檔
				new Thread(){
					synchronized public void run(){
						UI_EventHandler ehandle = GUIGameTool.getDefaultGameTool().getDefaultEventHandler();
						if(ehandle.isListening())
							ehandle.setEvent( new EventLoadGame() );
					}
				}.start();
			}else if(STR[4].equals(str)){ //遊戲設定
			}else if(STR[5].equals(str)){ //離開遊戲
				new Thread(){
					synchronized public void run(){
						UI_EventHandler ehandle = GUIGameTool.getDefaultGameTool().getDefaultEventHandler();
						if(ehandle.isListening())
							ehandle.setEvent( new EventQuitGame() );
					}
				}.start();
			}
		}
	}

	//========= MouseListener ===========
	public void mouseClicked(MouseEvent e){}
	public void mousePressed(MouseEvent e){
		int x = e.getX();
		int y = e.getY();

		for(int i = 0; i < iLabel.size(); i++){
			int xx = iLabel.get(i).absX;
			int yy = iLabel.get(i).absY;
			int defW = iLabel.get(i).defW;
			int defH = iLabel.get(i).defH;
			int dist = Math.max( Math.abs(xx-x), Math.abs(yy-y) );
			if(dist <= 20){
				iLabel.get(i).click();
				return;
			}
		}
	}
	public void mouseReleased(MouseEvent e){}
	public void mouseEntered(MouseEvent e){}
	public void mouseExited(MouseEvent e){
		for(int i = 0; i < iLabel.size(); i++)
			iLabel.get(i).setDefaultBounds();
		/** 狀態會傳到上一層 */
		Container c = this.getParent();
		e.translatePoint(getLocation().x, getLocation().y);
		while(c != null && !(c instanceof MouseListener)){
			e.translatePoint(c.getLocation().x, c.getLocation().y);
			c = c.getParent();
		}
		if(c!=null && (c instanceof MouseListener))
			((MouseListener) c).mouseExited(e);
	}

	//========= MouseMotionListener ==========

	public void mouseDragged(MouseEvent e){
	}
	public void mouseMoved(MouseEvent e){
		int x = e.getX();
		int y = e.getY();
		for(int i = 0; i < iLabel.size(); i++){
			int xx = iLabel.get(i).absX;
			int yy = iLabel.get(i).absY;
			int defW = iLabel.get(i).defW;
			int defH = iLabel.get(i).defH;
			int dist = Math.max( Math.abs(xx-x), Math.abs(yy-y) );
			if(dist > 14 + 14){
				iLabel.get(i).setBounds(xx - defH/2, yy - defW/2, defW, defH);
			}else if(dist > 14){
				int sizW = defW + (28 - dist);
				int sizH = defH + (28 - dist);
				iLabel.get(i).setBounds(xx - sizW/2, yy - sizH/2 - (28-dist)/3, sizW, sizH);
			}else{
				iLabel.get(i).setBounds(xx - (defW+14)/2, yy - (defH+14)/2 - 4, defW+14, defH+14);
			}
		}
		
		/** 狀態會傳到上一層 */
		Container c = this.getParent();
		e.translatePoint(getLocation().x, getLocation().y);
		while(c != null && !(c instanceof MouseMotionListener)){
			e.translatePoint(c.getLocation().x, c.getLocation().y);
			c = c.getParent();
		}
		if(c!=null && (c instanceof MouseMotionListener))
			((MouseMotionListener) c).mouseMoved(e);
	}
}
