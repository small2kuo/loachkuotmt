package proj.ui;

import proj.*;
import proj.map.building.*;
import proj.map.*;
import proj.player.*;
import proj.item.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.ArrayList;

/** 處理 UserInterface 當中的一些 Query 問題 */
public class GUIOptionPanel extends JPanel implements MouseListener, MouseMotionListener, ActionListener, ListSelectionListener{
	private GUIMapPanel mapPanel;
	private JLabel message;
	private JList qlist = null;
	private JScrollPane qpane = null;
	private Color sysMsgBgColor = new Color(GUIGameTool.SYSTEM_MSG_BGCOLOR.getRGB());
	private Color sysMsgBgShaderColor = new Color(80, 80, 80, 155);
	private Color sysBgColor = new Color(200,200,200,110);
	private Object result = null;
	private int selIndex = -1;
	private JButton yesButton, noButton, cancelButton;
	private JButton mouseOnButton = null;
	private JScrollPane iBox = null;
	private boolean cancelWithRightClick = false;

	public static String MSG_YES = "是";
	public static String MSG_NO = "否";
	public static String MSG_CANCEL = "取消";
	public static int FIXED_QLIST_HEIGHT = 120;
	public GUIOptionPanel(GUIMapPanel mapPanel){
		super();
		this.setLayout(null);
		this.mapPanel = mapPanel;
		this.setOpaque(false);
		this.setVisible(false);
		message = new JLabel("", JLabel.CENTER);
		message.setVisible(false);
		this.add(message);
		this.addMouseMotionListener(this);
		this.addMouseListener(this);

		yesButton = new JButton(MSG_YES);
		noButton = new JButton(MSG_NO);
		cancelButton = new JButton(MSG_CANCEL);
		yesButton.setVisible(false);
		noButton.setVisible(false);
		cancelButton.setVisible(false);
		yesButton.setActionCommand( MSG_YES );
		noButton.setActionCommand( MSG_NO );
		cancelButton.setActionCommand( MSG_CANCEL );
		yesButton.addActionListener(this); yesButton.addMouseListener(this);
		noButton.addActionListener(this); noButton.addMouseListener(this);
		cancelButton.addActionListener(this); cancelButton.addMouseListener(this);
		this.add(yesButton);
		this.add(noButton);
		this.add(cancelButton);

		qlist = null;
	}
	public void valueChanged(ListSelectionEvent e){
		selIndex = qlist.getSelectedIndex();
	}
	public void actionPerformed(ActionEvent e){
		synchronized(this){
			result = e.getActionCommand();
			notifyAll();
		}
	}
	public void mouseClicked(MouseEvent e){
		synchronized(this){
			if(e.getButton() == MouseEvent.BUTTON3 && cancelWithRightClick == true) //rightClicked
				result = MSG_CANCEL;
			notifyAll();
		}
	}
	public void mousePressed(MouseEvent e){}
	public void mouseReleased(MouseEvent e){}
	public void mouseEntered(MouseEvent e){
		if(e.getComponent() instanceof JButton){
			mouseOnButton = (JButton) e.getComponent();
			mouseOnButton.setLocation(mouseOnButton.getX()-1, mouseOnButton.getY()-5);
		}
	}
	public void mouseExited(MouseEvent e){
		if(e.getComponent() instanceof JButton){
			mouseOnButton = (JButton) e.getComponent();
			mouseOnButton.setLocation(mouseOnButton.getX()+1, mouseOnButton.getY()+5);
			mouseOnButton = null;
		}
	}
	public void mouseDragged(MouseEvent e){}
	public void mouseMoved(MouseEvent e){}

	static Stroke STROKE_OPTION_MSG =
		new BasicStroke(5.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	public void paint(Graphics g){
		g.setColor(sysBgColor);
		g.fillRect(0, 0, getWidth(), getHeight());
		if(message.isVisible())
			paintMessage(g);
		if(qpane != null)
			paintQpane(g);
		super.paint(g);
	}
	private void paintQpane(Graphics g){
		g.setColor(sysMsgBgShaderColor.brighter().brighter());
		g.fillRect(qpane.getX()-2, qpane.getY()-2, qpane.getWidth(), qpane.getHeight());
		g.setColor(sysMsgBgShaderColor);
		g.fillRect(qpane.getX()+2, qpane.getY()+2, qpane.getWidth(), qpane.getHeight());
	}
	private void paintMessage(Graphics g){
		g.setColor(sysMsgBgShaderColor.brighter().brighter());
		g.fillRect(message.getX()-6, message.getY()-6, message.getWidth()+20, message.getHeight()+20);
		g.setColor(sysMsgBgShaderColor);
		g.fillRect(message.getX()-8, message.getY()-8, message.getWidth()+20, message.getHeight()+20);
		g.setColor(sysMsgBgColor);
		//((Graphics2D) g).setStroke( STROKE_OPTION_MSG );
		g.fill3DRect(message.getX()-10, message.getY()-10, message.getWidth()+20, message.getHeight()+20, true);
	}
	private static String toHTML(String str){
		return "<html>" + str.replaceAll("\n", "<br />") + "</html>";
	}
	private int calcHeights(){
		int ret = 20;
		if(message.isVisible()) ret += (int)message.getPreferredSize().getHeight() + 20;
		if(qpane != null){
			//ret += qlist.getPreferredSize().getHeight() + 20;
			ret += FIXED_QLIST_HEIGHT + 20;
			//System.err.println( "jscrollpane: " + qpane );
		}
		if(iBox != null){
			ret += (int) iBox.getPreferredSize().getHeight() + 20;
		}
		if(yesButton.isVisible() || noButton.isVisible() || cancelButton.isVisible())
		   ret += (int)yesButton.getPreferredSize().getHeight() + 20;
		return ret;
	}
	private int calcButtonsWidth(){
		int ret = 10;
		if(yesButton.isVisible())
			ret += (int)yesButton.getPreferredSize().getWidth() + 10;
		if(noButton.isVisible())
			ret += (int)noButton.getPreferredSize().getWidth() + 10;
		if(cancelButton.isVisible())
			ret += (int)cancelButton.getPreferredSize().getWidth() + 10;
		return ret;
	}
	/** 調整所有物件在螢幕中的相對位置 */
	private void adjustComponents(){
		int h = getHeight()/2 - calcHeights()/2 + 20, pw = calcButtonsWidth() - 20, w = getWidth()/2-pw/2;
		if(message.isVisible()){
			pw = (int)message.getPreferredSize().getWidth();
		   	w = getWidth()/2 - (int)message.getPreferredSize().getWidth()/2;
			if( calcButtonsWidth() - 20 > pw){
				pw = calcButtonsWidth() - 20;
				w = getWidth()/2 - pw/2;
			}
			message.setBounds( w, h, pw, (int)message.getPreferredSize().getHeight() );	
			h+=(int)message.getPreferredSize().getHeight() + 20;
		}
		if(qpane != null){
			if(pw < (int)qpane.getPreferredSize().getWidth() + 40)
			{
				pw = (int)qpane.getPreferredSize().getWidth() + 40;
				w = getWidth()/2 - pw/2;
			}
			qpane.setBounds( w, h, pw, FIXED_QLIST_HEIGHT);
			h+=FIXED_QLIST_HEIGHT + 20;
		}
		if(iBox != null){
			pw = (int)iBox.getPreferredSize().getWidth();
			w = getWidth()/2-pw/2;
			System.err.println("here, iBox: " + w + ", " + h + ", " + pw + ", " + (int)iBox.getPreferredSize().getHeight());
			iBox.setBounds(w, h, pw, (int)iBox.getPreferredSize().getHeight());
			h+=(int)iBox.getPreferredSize().getHeight() + 20;
		}
		if(yesButton.isVisible() || noButton.isVisible() || cancelButton.isVisible()){
			int fw = getWidth()/2 - calcButtonsWidth()/2 + 10;
			if(yesButton.isVisible()){
				yesButton.setBounds( fw, h, (int)yesButton.getPreferredSize().getWidth(), (int)yesButton.getPreferredSize().getHeight());
				fw += (int)yesButton.getPreferredSize().getWidth() + 10;
			}
			if(noButton.isVisible()){
				noButton.setBounds( fw, h, (int)noButton.getPreferredSize().getWidth(), (int)noButton.getPreferredSize().getHeight());
				fw += (int)noButton.getPreferredSize().getWidth() + 10;
			}
			if(cancelButton.isVisible()){
				cancelButton.setBounds( fw, h, (int)cancelButton.getPreferredSize().getWidth(), (int)cancelButton.getPreferredSize().getHeight());
				fw += (int)cancelButton.getPreferredSize().getWidth() + 10;
			}
			h += 20 + (int)yesButton.getPreferredSize().getHeight();
		}
	}

	//==================== User Interface called from GUIRoot =====================
	/** 顯示訊息 並點任意地方繼續. */
	public void showMessage( String msg ){
		// 這些到時候要改成正常的...
		System.err.println("Message:[" + msg + "]" );
		//JOptionPane.showMessageDialog(mapPanel, msg, "確認", JOptionPane.INFORMATION_MESSAGE);
		message.setText( toHTML(msg) );
		message.setVisible(true);
		adjustComponents();
		this.setVisible(true);
		synchronized(this){
			try{this.wait();}catch(InterruptedException e){ System.err.println(e.toString()); }
		}
		this.setVisible(false);
		message.setVisible(false);
	}
	/** 詢問Yes/No with specified Yes/No的字串. */
	public boolean queryYesNo(String msg, String yesMsg, String noMsg ){
		System.err.println("Query:[" + msg + "]");
		// 這些到時候要改成正常的...
		//int retv = JOptionPane.showOptionDialog(mapPanel, msg, "請回答", JOptionPane.YES_NO_OPTION,
		//		JOptionPane.QUESTION_MESSAGE, null, new Object[]{yesMsg, noMsg}, null);
		message.setText( toHTML(msg) );
		message.setVisible(true);
		yesButton.setText( toHTML(yesMsg) );
		noButton.setText( toHTML(noMsg) );
		yesButton.setVisible(true);
		noButton.setVisible(true);
		adjustComponents();
		result = null;
		cancelWithRightClick = true;
		this.setVisible(true);
		synchronized(this){
			while(result == null)
				try{this.wait();}catch(InterruptedException e){ System.err.println(e.toString()); }
		}
		this.setVisible(false);
		cancelWithRightClick = false;
		message.setVisible(false);
		yesButton.setText( MSG_YES );
		noButton.setText( MSG_NO );
		yesButton.setVisible(false);
		noButton.setVisible(false);
		return MSG_YES.equals(result);
	}
	/** 詢問Yes/No with default Yes/No的字串. */
	public boolean queryYesNo(String msg){
		System.err.println("Query:[" + msg + "]" );
		// 這些到時候要改成正常的...
		return queryYesNo(msg, MSG_YES, MSG_NO);
	}
	/** 詢問多個選項.
	 * 點選某個選項回傳他的Index. */
	public int queryMultiChoice( String msg, String[] options ){
		System.err.println("Query:[" + msg + "]" );
		result = null;
		selIndex = -1;
		qlist = new JList(options);
		qpane = new JScrollPane(qlist);
		qlist.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
		qpane.setVisible(true);
		qlist.addListSelectionListener(this);
		qlist.setSelectedIndex(0);
		qlist.addMouseListener(this);
		message.setText( toHTML(msg) );
		message.setVisible(true);
		this.add(qpane);
		yesButton.setText( toHTML("確定") );
		yesButton.setVisible(true);
		adjustComponents();
		this.setVisible(true);
		synchronized(this){
			while(result == null)
				try{this.wait();}catch(InterruptedException e){ System.err.println(e.toString()); }
		}
		this.setVisible(false);
		this.remove(qpane);
		yesButton.setText( MSG_YES );
		yesButton.setVisible(false);
		qlist = null;
		qpane = null;
		cancelWithRightClick = false;
		if( MSG_CANCEL.equals(result) )
			selIndex = -1;
		return selIndex;
	}
	/** 詢問多個選項,並且有取消的選項(包含在GUI裡點右鍵?).
	 * 點選某個選項回傳他的Index. 取消則return -1.*/
	public int queryMultiChoiceWithCancel( String msg, String[] options ){
		System.err.println("Query:[" + msg + "]" );
		cancelWithRightClick = true;
		cancelButton.setVisible(true);
		int ret = queryMultiChoice( msg, options );
		cancelButton.setVisible(false);
		cancelWithRightClick = false;
		return ret;
	}
	/** 詢問多個選項,並且有取消的選項(包含在GUI裡點右鍵?).
	 * 點選某個選項回傳他的Index. 取消則return -1.*/
	public int queryMultiChoiceWithCancel( String msg, String[] options , String cancelMsg ){
		System.err.println("Query:[" + msg + "]" );
		cancelWithRightClick = true;
		cancelButton.setText( toHTML(msg) );
		int ret = queryMultiChoiceWithCancel(msg, options);
		cancelButton.setText( MSG_CANCEL );
		cancelWithRightClick = false;
		return ret;
	}
	/** TODO 輸入數字的選項. */
	public int queryInteger(String msg){
		return 0;
	}
	/** TODO 輸入數字的選項. 例如遙控骰子, 或簽樂透之類, 可以在 start 到 end (包含) 之間選一個數字. */
	public int queryInteger(String msg,int start,int end){
		return start;
	}
	//Object of = new Object();
	/** 顯示玩家的道具欄 */
	public Item showPlayerItemBox(Player p, boolean cancelWithRightClick){		
		iBox = new GUIItemBox(p, new JPanel(), this);
		this.add(iBox);
		iBox.setVisible(true);
		iBox.validate();
		message.setText("請選擇使用一個道具，或按右鍵取消。");
		message.setVisible(true);
		adjustComponents();
		result = null;
		this.cancelWithRightClick = cancelWithRightClick;
		this.setVisible(true);
		this.validate();
		this.getParent().validate();
		//System.err.println("before wait " + this.getParent() + "\n" + iBox);
		synchronized(this){
			while(result == null && ((GUIItemBox) iBox).getChoosedItem() == null){
				//System.err.println("waiting... ");
				try{ this.wait(); }catch(Exception e){System.err.println( e.toString() );}
			}
		}
		//System.err.println("GUIOptionPanel: HERE");
		this.setVisible(false);
		this.cancelWithRightClick = false;
		this.remove(iBox);
		message.setVisible(false);
		Item ret = ((GUIItemBox) iBox).getChoosedItem();
		iBox = null;
		return ret;
	}
	/** 顯示玩家目前的所有資訊 */
	public void showPlayerStatusBox(Player nowPlayer){
		ArrayList<LandEmpty> allLands = GameTool.defaultGameTool.getAllLandsOwnedByPlayer(nowPlayer);
		String[][] oarr = new String[ allLands.size() ][];
		String[] cname = {"", "所在位置", "軟體名稱", "等級", "目前價值", "過路費", "升級花費"};
		for(int i=0;i<allLands.size(); i++){
			oarr[i] = new String[7];
			oarr[i][0] = Integer.toString(i+1);
			oarr[i][1] = allLands.get(i).getStreet().getName();
			if(allLands.get(i).getBuilding() != null){
				//TODO: 目前只考慮BuildingSmall
				BuildingSmall b = (BuildingSmall) allLands.get(i).getBuilding();
				oarr[i][2] = b.getBuildingData().name;
				oarr[i][3] = Integer.toString(b.getLevel());
				oarr[i][6] = Integer.toString(b.calcUpgradeCost(null));
			}else{
				oarr[i][2] = "(空地)";
				oarr[i][3] = "0";
				oarr[i][6] = "(尚未決定)";
			}
			oarr[i][5] = Integer.toString(allLands.get(i).calcToll(null));
			oarr[i][4] = "-"; //TODO:這裡
		}
		JTable jTable = new JTable(oarr, cname);
		jTable.addMouseListener(this);
		iBox = new JScrollPane(jTable);
		iBox.setPreferredSize(new Dimension(600, 400));
		iBox.addMouseListener(this);
		this.add(iBox);
		iBox.setVisible(true);
		iBox.validate();
		message.setText( toHTML( "[" + nowPlayer.getName() + "] 授權軟體開發園地列表。\n請按右鍵取消。"));
		message.setVisible(true);
		adjustComponents();
		result = null;
		this.cancelWithRightClick = true;
		this.setVisible(true);
		this.validate();
		this.getParent().validate();
		synchronized(this){
			while(result == null){
				try{ this.wait(); }catch(Exception e){System.err.println( e.toString() );}
			}
		}
		this.setVisible(false);
		this.cancelWithRightClick = false;
		this.remove(iBox);
		message.setVisible(false);
		iBox = null;
	}
}
