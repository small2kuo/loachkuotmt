package proj.ui;

import proj.*;
import proj.player.*;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.*;
import java.util.HashMap;


public class GUIPlayerStatusViewerSimple extends JPanel{
	private GameEnv env;
	private HashMap<Player, Image> playerAvatar = new HashMap<Player, Image>();
	public GUIPlayerStatusViewerSimple(GameEnv env){
		this.env = env;
		this.setLayout(null);
		this.setBounds(0, 0, 300, 200);
		this.setPreferredSize(new Dimension(300, 200));
		this.setOpaque(false);
		this.setFont(new Font("LiHei pro", 0, 28));
	}

	private Image getAvatar(Player p){
		//System.err.println("Player " + p);
		if(playerAvatar.get(p) != null)
			return playerAvatar.get(p);
		if(p.getData().avatarPath != null){
			Image pimg = GUIGameTool.getDefaultGameTool().getImage( p.getData().avatarPath );
			playerAvatar.put(p, pimg);
			return pimg;
		}
		return null;
	}

	/** 取得現在玩家資訊並顯示出來 */
	static int w=0;
	public void paint(Graphics g){
		super.paint(g);
		//g.setColor(new Color(155, 255, 155, 155) );
		//g.fillRect(0, 0, getWidth(), getHeight()); 
		//g.setFont(g.getFont().deriveFont(28.f));
		//Player p = env.getNowPlayer();
		//g.drawString(p.getName());
		if(env.getNowPlayer() == null) return;
		String str = env.getNowPlayer().getName();
		Player p = env.getNowPlayer();
		//System.err.println( "name: " + str );
		//System.err.println(this.getFont().getFamily());
		Image pimg = getAvatar(p);
		if(pimg == null) return;
		g.drawImage(pimg, 25, 80, 100, 100, this); //TODO: 取得玩家頭像字串
		//最好不要用gif =__________=

		//System.err.println(++w);
		g.setColor(env.getNowPlayer().getData().pColor); //TODO: 取得玩家色彩
		g.fill3DRect(135, 139, 100, 8, true);

		g.setColor(new Color(130,130,130,188));
		g.drawString(str, 142, 142);
		g.setColor(Color.BLACK);
		g.drawString(str, 140, 140);

		String money = "$" + env.getNowPlayer().getCash();
		g.setColor(new Color(130,130,130,188));
		g.drawString(money, 162, 182);
		g.setColor(Color.BLACK);
		g.drawString(money, 160, 180);
	}
	/*synchronized public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height){
		boolean ret = super.imageUpdate(img, infoflags,x,y,width,height);
		if(w>100)try{Thread.sleep(500);}catch(Exception e){}
		return ret;
	}*/
}
