package proj.ui;

import proj.map.*;
import proj.*;

import java.awt.*;
import javax.swing.*;

public class GUIQueryingLabel extends JLabel{
	int realX=-1, realY=-1;
	boolean isMoving = true;
	Shape coveringShape = null;
	Object coveringObj = null;
	Object sourceObj = null;
	Icon deactiveIcon = null;
	Icon activeIcon = null;
	//=============== ICON 相關 =======================
	public void setChangingIcon(Icon dIcon, Icon aIcon){
		setDeactiveIcon(dIcon);
		setActiveIcon(aIcon);
	}
	public void setChangingIcon(Icon icon){
		setDeactiveIcon(icon);
		setActiveIcon(icon);
	}
	private void setActiveIcon(Icon icon){
		activeIcon = icon;
	}
	private void setDeactiveIcon(Icon icon){
		deactiveIcon = icon;
		setIcon(icon); //順便設定icon
	}
	/** 不一定要有這個method */
	public void changeIcon(){
		if(getIcon() == activeIcon)
			setIcon(deactiveIcon);
		else
			setIcon(activeIcon);
	}
	private void changeIcon(boolean isActive){
		setIcon(isActive? activeIcon: deactiveIcon);
	}
	//=============== 其他 ==========================
	public void setSource(Object o){
		sourceObj = o;
	}
	public Object getSource(){
		return sourceObj;
	}
	public void setCovering(Object o, Shape s){
		coveringObj = o;
		coveringShape = s;
		changeIcon(true);
	}
	public void resetCovering(){
		coveringShape = null;
		coveringObj = null;
		changeIcon(false);
	}
	public Shape getCoveringShape(){
		return coveringShape;
	}
	public Object getCoveringObj(){
		return coveringObj;
	}
	public void setXY(int realX,int realY){
		this.realX = realX;
		this.realY = realY;
	}
	public void setMoving(boolean moving){
		isMoving = moving;
	}
	public void relocate(int topLeftCornerX, int topLeftCornerY){
		this.setLocation(realX - topLeftCornerX, realY - topLeftCornerY);
	}
	public GUIQueryingLabel(){
		super();
	}
	@Override
		public void setIcon(Icon icon){
			if(icon != null){
				this.setSize(icon.getIconWidth(), icon.getIconHeight());
			}
			super.setIcon(icon);
		}
}
