package proj.ui;
/** 當視角改變位置的時候, 東西要跟著動 */
interface GUIRelocatable{
   public void relocate(int topLeftX,int topLeftY);
}
