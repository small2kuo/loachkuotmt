package proj.ui;

import proj.*;
import proj.map.*;
import proj.map.building.*;

import proj.item.*;
import proj.event.*;
import proj.player.*;

import java.awt.GraphicsDevice;
import java.awt.Window;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.LayoutManager;
import javax.swing.*;
import java.awt.event.*;


/** 負責實行所有<b>遊戲進行中</b> UI 介面的首腦級 GUI 物件 */
public class GUIRoot implements UserInterface{
   private GameEnv env;
   private GUIGameFrame gameFrame;
   private GUIMapPanel mapPanel;
   private GUIMenuBarPanel menuBarPanel;
   private GUIOptionPanel optionPanel;
   private JPanel middlePanel;
   private JWindow loadingWindow;

   private UI_EventHandler ehandle;
   private JLayeredPane popupPane;

   private UserInterfaceAnimator animator;
   /** 傳入 GameEnvironment 進行設定. */
   public GUIRoot(GameEnv env){
      this.env = env;

      loadingWindow = new JWindow(){
         public void paint(Graphics g){
            Image img = GUIGameTool.getDefaultGameTool().getImage( "data/sysimg/loading.gif" );
            g.drawImage(img, 0, 0, this);
         }
      };
      loadingWindow.setBounds(100, 100, 640, 480);
      loadingWindow.setVisible(true);

      ehandle = new UI_EventHandler();
      GUIGameTool.getDefaultGameTool().setDefaultEventHandler(ehandle);
      gameFrame = new GUIGameFrame(env);
      popupPane = new JLayeredPane();
      popupPane.addComponentListener(new GUIPopupPaneComponentListener());
      gameFrame.add(popupPane, BorderLayout.CENTER);
      mapPanel = new GUIMapPanel(env, ehandle, popupPane);

      popupPane.add(mapPanel, JLayeredPane.DEFAULT_LAYER);
      menuBarPanel = new GUIMenuBarPanel(env, ehandle);
      menuBarPanel.setPreferredSize(new Dimension(800, 200));
      middlePanel = new JPanel(new BorderLayout());
      middlePanel.setOpaque(false);
      middlePanel.add(menuBarPanel, BorderLayout.SOUTH);
      popupPane.add(middlePanel, new Integer(900)); //位置僅次於跳出選單

      optionPanel = new GUIOptionPanel(mapPanel);
      System.err.println("optionPanel = " + optionPanel);
      GUIGameTool.getDefaultGameTool().setDefaultOptionPanel(optionPanel);
      popupPane.add(optionPanel, new Integer(1000)); //位置在最上面
      //放一隻空的 Thread, 理論上這邊可以是跑音樂?
      new Thread(){
         public void run(){
            while(true){
               try{
                  Thread.sleep(1000);
               }catch(InterruptedException e){}
            }
         }
      }.start();

      popupPane.add(u, new Integer(-100)); //將顯示玩家用的 Panel 放到 popupPane 最下面藏好XD
      //popupPane.add(diceL, new Integer(-100)); //將顯示骰子用的 Panel 放到 popupPane 最下面藏好
      popupPane.add(itemLabel, new Integer(-100)); //將追蹤用的 Label 放到最下面藏好XD
      popupPane.validate();
      System.err.println("載入GameRoot最後階段...");

      mapPanel.paint(loadingWindow.createImage(10, 10).getGraphics());
      menuBarPanel.paint(loadingWindow.createImage(10, 10).getGraphics());
      //updateNewRound();

      //先載入大部份的圖片
      Player[] plist = env.getAllPlayers();
      for(int i = 0; i < plist.length; i++){
         GUIGameTool.getDefaultGameTool().getImage(plist[i].getData().avatarPath);
         GUIGameTool.getDefaultGameTool().getImage(plist[i].getData().charSetPath);
      }

      //GraphicsDevice gd = gameFrame.getGraphicsConfiguration().getDevice();
      //System.err.println("gd = " + gd);
      //gd.setFullScreenWindow(gameFrame);
	  
	  animator = new GUIAnimator(this, env, gameFrame, mapPanel, menuBarPanel,
			  optionPanel, middlePanel, loadingWindow, ehandle, popupPane, u);

      gameFrame.setVisible(true);
      try{Thread.sleep(500);}catch(InterruptedException e){}
      loadingWindow.setVisible(false);
   }
   //============== ComponentListener ===============
   /** 處理遊戲畫面大小改變的問題: 重新設定 popupPane 上面各個東西的大小 */
   public class GUIPopupPaneComponentListener implements ComponentListener{
      public void componentResized(ComponentEvent e){
         mapPanel.setSize( popupPane.getWidth(), popupPane.getHeight() );
         mapPanel.updateUI();
         middlePanel.setSize( popupPane.getWidth(), popupPane.getHeight() );
         middlePanel.updateUI();
         optionPanel.setSize( popupPane.getWidth(), popupPane.getHeight() );
         optionPanel.updateUI();
      }
      public void componentMoved(ComponentEvent e){}
      public void componentShown(ComponentEvent e){}
      public void componentHidden(ComponentEvent e){}
   }

   //TODO===========UserInterface==============
   public UserInterfaceAnimator getAnimator(){return this.animator;}
   /** 儲存檔案選項.
    * call by {@link EventSaveGame}
    * @param geis 所有檔案紀錄的GameEnvInfo, geis[]的順序與選項順序無關,
    *             geis[].num才是選項的index. 
    * @return 選項的index,不是geis的index ; -1代表Cancel. */
   public int querySaveGame( GameEnvInfo[] geis ){
	   //先這樣.
	   String[] opts = new String[geis.length];
	   for(int i = 0; i < geis.length; i++){
		   if(geis[i] == null)
			   opts[i] = "這個記錄是空的";
		   else
			   opts[i] = "存檔時間: " + geis[i].saveTime + "/ 地圖: " + geis[i].mapInfo.getName() + "/ 遊戲人數: " + geis[i].pInfo.length;
		}
	   int retv = queryMultiChoiceWithCancel( "請選擇要儲存的檔案",  opts);
      return retv;
   }
   /** 讀取檔案選項.
    * call by {@link EventLoadGame}
    * @param geis 所有檔案紀錄的GameEnvInfo, geis[]的順序與選項順序無關,
    *             geis[].num才是選項的index. 
    * @return geis的index,不是選項的的index ; -1代表Cancel. */
   public int queryLoadGame( GameEnvInfo[] geis ){
	   //先這樣.
	   String[] opts = new String[geis.length];
	   for(int i = 0; i < geis.length; i++){
		   if(geis[i] == null)
			   opts[i] = "這個記錄是空的";
		   else
			   opts[i] = "存檔時間: " + geis[i].saveTime + "/ 地圖: " + geis[i].mapInfo.getName() + "/ 遊戲人數: " + geis[i].pInfo.length;
	   }
	   int retv = queryMultiChoiceWithCancel("請選擇要讀取的檔案", opts);
      return retv;
   }
   /** 結束一個 UI */
   public void turnOff(){
	   gameFrame.dispose();
   }
   /** call by {@link EventThrowDices}. */
   public void showThrowDices( Player user , int dice_num ){
      System.err.println("玩家: " + user.getName() + " 骰到了 " + dice_num + " 步 XD");
		animator.showThrowDices(user, dice_num);
   }
   //================== Player Moveing Label =======================
   public PlayerMovingLabel u = new PlayerMovingLabel(); //playerMovingLabel
   public class PlayerMovingLabel extends JLabel implements GUIRelocatable{
      int realX=-1, realY=-1;
      boolean isMoving = false;
      public void setXY(int realX,int realY){
         this.realX = realX;
         this.realY = realY;
      }
      public void setMoving(boolean moving){
         isMoving = moving;
      }
      public void relocate(int topLeftCornerX, int topLeftCornerY){
         if(isMoving == false)
            this.setLocation(realX - topLeftCornerX, realY - topLeftCornerY);
      }
      public PlayerMovingLabel(){
         super();
      }
      @Override
         public void setIcon(Icon icon){
            if(icon != null)
               this.setSize(icon.getIconWidth(), icon.getIconHeight());
            super.setIcon(icon);
         }
   }
   /** call by {@link EventPlayerMove}.
	* @see GUIAnimator.showPlayerMove
	*/
   public void showPlayerMove( Player user , Block fromBlock , Block toBlock){
	   animator.showPlayerMove(user, fromBlock, toBlock); //直接呼叫動畫處理.
   }
   /** 點選某個OK按鈕. */
   public void click( String msg ){ }
   /** 顯示訊息 並點任意地方繼續. */
   public void showMessage( String msg ){
      gameScreenFreeze();
      optionPanel.showMessage(msg);
      gameScreenUnfreeze();
   }
   /** 詢問Yes/No with specified Yes/No的字串. */
   public boolean queryYesNo(String msg, String yesMsg, String noMsg ){
      gameScreenFreeze();
      boolean ret = optionPanel.queryYesNo(msg, yesMsg, noMsg);
      gameScreenUnfreeze();
      return ret;
   }
   /** 詢問Yes/No with default Yes/No的字串. */
   public boolean queryYesNo(String msg){
      gameScreenFreeze();
      boolean ret = optionPanel.queryYesNo(msg);
      gameScreenUnfreeze();
      return ret;
   }
   /** 詢問多個選項.
    * 點選某個選項回傳他的Index. */
   public int queryMultiChoice( String msg, String[] options ){
      gameScreenFreeze();
      int ret = optionPanel.queryMultiChoice(msg, options);
      gameScreenUnfreeze();
      return ret;
   }
   /** 詢問多個選項,並且有取消的選項(包含在GUI裡點右鍵?).
    * 點選某個選項回傳他的Index. 取消則return -1.*/
   public int queryMultiChoiceWithCancel( String msg, String[] options ){
      gameScreenFreeze();
      int ret = optionPanel.queryMultiChoiceWithCancel(msg, options);
      gameScreenUnfreeze();
      return ret;
   }
   /** 詢問多個選項,並且有取消的選項(包含在GUI裡點右鍵?).
    * 點選某個選項回傳他的Index. 取消則return -1.*/
   public int queryMultiChoiceWithCancel( String msg, String[] options , String cancelMsg ){
      gameScreenFreeze();
      int ret = optionPanel.queryMultiChoiceWithCancel(msg, options, cancelMsg);
      gameScreenUnfreeze();
      return ret;
   }
   /** 輸入數字的選項. */
   public int queryInteger(String msg){
      return optionPanel.queryInteger(msg);
   }
   /** 輸入數字的選項. 例如遙控骰子, 或簽樂透之類, 可以在 start 到 end (包含) 之間選一個數字.
    * @return -1 if failed */
   public int queryInteger(String msg,int start,int end){
      return optionPanel.queryInteger(msg, start, end);
   }

   public GUIQueryingLabel itemLabel = new GUIQueryingLabel();
   /** 找尋可以放置物件的 Block.
    * return null代表Cancel*/
   public Block queryChooseBlock(Standable s){
      Block ret = null;
      
      if(s instanceof Item){
         Item item = (Item) s;
         Image itemPlaceImg = null;
         if(item.getData() != null)
            itemPlaceImg = GUIGameTool.getDefaultGameTool().getImage( item.getData().groundImgPath );
         else
            itemPlaceImg = GUIGameTool.getDefaultGameTool().getImage( "data/item_icons/ItemMineCard.gif" ); //這行要拿掉XD
         itemLabel.setSource(s);
		 itemLabel.setChangingIcon( new ImageIcon(itemPlaceImg) );
         itemLabel.setMoving( true ); //其實好像要設定成false?
         popupPane.setLayer( itemLabel, JLayeredPane.DRAG_LAYER );
		 menuBarPanel.freezeUI();
         ret = mapPanel.queryChooseBlock( itemLabel );
		 menuBarPanel.unfreezeUI();
         popupPane.setLayer( itemLabel, new Integer(-100) );
		 itemLabel.setSource(null);
      } //如果不是Item的話我也不知道該怎麼辦了...
      return ret;
   }
   /** 找尋可以選擇的Upgradable.(building?)
    * return null代表Cancel*/
   public Upgradable queryChooseUpgradable(){
	   Upgradable ret = null;
	   itemLabel.setChangingIcon( new ImageIcon( GUIGameTool.getDefaultGameTool().getImage( "data/sysimg/target.png" )),
			   new ImageIcon( GUIGameTool.getDefaultGameTool().getImage( "data/sysimg/ontarget.png" )) );
	   itemLabel.setMoving( false );
	   popupPane.setLayer( itemLabel, JLayeredPane.DRAG_LAYER );
	   menuBarPanel.freezeUI();
	   ret = mapPanel.queryChooseUpgradable(itemLabel);
	   menuBarPanel.unfreezeUI();
	   popupPane.setLayer( itemLabel, new Integer(-100) );
      return ret;
   }
   /** 找尋可以選擇的Player.(不包括except) except可以為null表是連自己都可以選
    * return null代表Cancel*/
   public Player queryChoosePlayer(Player except){
      Player ret = null;
	   itemLabel.setChangingIcon( new ImageIcon( GUIGameTool.getDefaultGameTool().getImage( "data/sysimg/target.png" )),
			   new ImageIcon( GUIGameTool.getDefaultGameTool().getImage( "data/sysimg/ontarget.png" )) );
	   itemLabel.setMoving( false );
	   itemLabel.setSource( except );
	   popupPane.setLayer( itemLabel, JLayeredPane.DRAG_LAYER );
	   menuBarPanel.freezeUI();
	   ret = mapPanel.queryChoosePlayer(itemLabel);
	   menuBarPanel.unfreezeUI();
	   itemLabel.setSource(null);
	   popupPane.setLayer( itemLabel, new Integer(-100) );
      return ret;
   }
   /** 找尋可以選擇的EmptyLand
    * return null代表Cancel*/
   public LandEmpty queryChooseLandEmpty(){
	   LandEmpty ret = null;
	   itemLabel.setChangingIcon( new ImageIcon( GUIGameTool.getDefaultGameTool().getImage( "data/sysimg/target.png" )),
			   new ImageIcon( GUIGameTool.getDefaultGameTool().getImage( "data/sysimg/ontarget.png" )) );
	   itemLabel.setMoving( false );
	   popupPane.setLayer( itemLabel, JLayeredPane.DRAG_LAYER );
	   menuBarPanel.freezeUI();
	   ret = mapPanel.queryChooseLandEmpty(itemLabel);
	   menuBarPanel.unfreezeUI();
	   popupPane.setLayer( itemLabel, new Integer(-100) );
      return ret;
   }
   /** 每回合使用者可以從選單取得各種事件,
    * 一直到取到的事件是EventThrowDices. */
   public Event getEvent(){
      GUIButtonThrowEvent rollDiceButton = 
         new GUIButtonThrowEvent("擲骰子", ehandle,
               new EventThrowDices(env.getNowPlayer()), env.getMap().getMapData().getShape( env.getNowPlayer().getBlock() ).getBounds()
               );
      synchronized(ehandle){
         rollDiceButton.relocate(mapPanel.getTopLeftX(), mapPanel.getTopLeftY());
         popupPane.add(rollDiceButton, JLayeredPane.POPUP_LAYER, 0);
         ehandle.resetEvent();
         ehandle.setListening();
         while(ehandle.getEvent() == null){
            System.err.println("HERE");
            try{
               ehandle.wait();
            }catch(InterruptedException e){}
         }
         System.err.println("GameRoot: GET EVENT: " + ehandle.getEvent());
         popupPane.remove(rollDiceButton);
		 popupPane.repaint();
         ehandle.resetListening();
         return ehandle.getEvent();
      }
   }
   /** 每回合的一開始, 會呼叫 updateNewRound 顯示新回合的開始 */
   public void updateNewRound(){

      menuBarPanel.updateNewRound();
      mapPanel.updateNewRound();

      //把顯示玩家的Panel加到POPUP_LAYER上面
      Image walkingImg = GUIGameTool.getDefaultGameTool().getImage(env.getNowPlayer().getData().charSetPath);
      Rectangle r = env.getMap().getMapData().getShape( env.getNowPlayer().getBlock() ).getBounds();
      u.setMoving(false);
      u.setXY((int)r.getX(), (int)r.getY() + GUIGameTool.SHOW_PLAYER_OFFSET_Y);
      u.relocate(mapPanel.getTopLeftX(), mapPanel.getTopLeftY());
      u.setIcon(new ImageIcon( walkingImg ));
      popupPane.setLayer(u, JLayeredPane.POPUP_LAYER.intValue());
   }
   /** 戳了就會回傳 Event 的 Button */
   class GUIButtonThrowEvent extends JButton implements ActionListener, GUIRelocatable{
      UI_EventHandler ehandle;
      Event event;
      int realX = -1, realY = -1;
      public GUIButtonThrowEvent(String str, UI_EventHandler ehandle, Event event){
         super(str);
         this.setSize(75, 75);
         this.ehandle = ehandle;
         this.event = event;
         this.addActionListener(this);
      }
      public GUIButtonThrowEvent(String str, UI_EventHandler ehandle, Event event, Rectangle pos){
         this(str, ehandle, event);
         realX = (int)pos.getX() + 50;
         realY = (int)pos.getY() - 50;
      }
      public void actionPerformed(ActionEvent e){
         ehandle.setEvent(event);
      }
      public void relocate(int topLeftCornerX, int topLeftCornerY){
         if(realX >= 0 && realY >= 0)
            this.setLocation(realX - topLeftCornerX, realY - topLeftCornerY);
      }
   }

   /** 整個畫面的刷新, 原則上只要有建築物和土地的任何變更都最好呼叫一下這個 method */
   @Deprecated
      public void updateView(){
         updateViewMap();
         updateViewPlayer();
      }
   /** 地圖畫面的刷新, 原則上只要有建築物和土地的任何變更都最好呼叫一下這個 method */
   @Deprecated
      public void updateViewMap(){
         mapPanel.updateViewMap();
      }
   /** 玩家畫面的刷新, 原則上只要有玩家的狀態有更動最好呼叫一下這個 method*/
   @Deprecated
      public void updateViewPlayer(){
         menuBarPanel.updateViewPlayer();
      }
   public void gameScreenFreeze(){
      mapPanel.getTimer().stop();
      mapPanel.removeMouseMotionListener(mapPanel);
      mapPanel.removeMouseListener(mapPanel);
   }
   public void gameScreenUnfreeze(){
      mapPanel.getTimer().start();
      mapPanel.addMouseMotionListener(mapPanel);
      mapPanel.addMouseListener(mapPanel);
   }
}

