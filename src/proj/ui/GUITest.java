package proj.util;

import proj.ui.*;
import proj.*;
import proj.map.*;
import proj.player.*;

/**
 * 測試 GUI畫面用, 請 make GUItest 然後 make runGUItest
 *
 * @version 1.1
 * 玩家顯示.
 */
public class GUITest{
   public static void main(String[] args){
      //GUIRoot r = new GUIRoot();
      GameEnv env = new GameEnv(new Map("mymap"), new Date(2010, 6, 15));
      env.addPlayer( new Player("小小郭", 0,
               new PlayerData(
                  null ,
                  new java.awt.Color(202, 57, 57),
                  "data/player/TESTUSEonion2.png",
                  null
                  )));
      env.next();
      GUIRoot r = new GUIRoot(env);
   }
}
