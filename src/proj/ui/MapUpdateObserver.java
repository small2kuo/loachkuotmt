package proj.ui;

/**
 * 當地圖上的一棟房子、或一個土地發生變更的情形時，
 * 透過 {@link GameTool#notifyAllMapObservers} 更新所有顯示地圖元件的物件
 */
public interface MapUpdateObserver{
	public void updateViewMap();
}

