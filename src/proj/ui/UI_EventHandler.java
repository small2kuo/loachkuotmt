package proj.ui;

import proj.*;
import proj.event.*;

public class UI_EventHandler{
	private Event e;
	private boolean listening;
	void UI_EventHandler(){
		e = null;
		listening = false;
	}
	public void setEvent(Event e){
		//System.err.println("HERE Waiting...");
		synchronized(this){
			//System.err.println("HERE Inside...");
			this.e = e;
			notifyAll();
		}
	}
	public void resetEvent(){
		this.e = null;
	}
	public Event getEvent(){
		return e;
	}
	public void setListening(){
		listening = true;
	}
	public void resetListening(){
		listening = false;
	}
	public boolean isListening(){
		return listening;
	}
}
