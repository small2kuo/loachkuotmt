package proj.ui;

public class UI_ObjHandler{
	static final String MSG_CANCEL = "CANCEL";
	private Object o = null;
	public Object getObj(){
			return o;
		}
		public void setObj(Object o){
			synchronized(this){
				this.o = o;
				notifyAll();
			}
		}
		public void resetObj(){
			o = null;
		}
	}
