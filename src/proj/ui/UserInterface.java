package proj.ui;

import proj.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/** 就是User Interface XD. */
public interface UserInterface{
   
   /** call by {@link EventPlayerMove}. */
   public void showPlayerMove( Player user , Block fromBlock , Block toBlock);
   /** call by {@link EventThrowDices}. */
   public void showThrowDices( Player user , int dice_num );
   
   
   public UserInterfaceAnimator getAnimator();
   //=================== 特殊事件. ==================== 
   /** call by {@link EventThrowDices}. */
   /** 儲存檔案選項.
    * call by {@link EventSaveGame}
    * @param geis 所有檔案紀錄的GameEnvInfo, geis[]的順序與選項順序無關,
    *             geis[].num才是選項的index. 
    * @return 選項的index,不是geis的index ; -1代表Cancel. */
   public int querySaveGame( GameEnvInfo[] geis );
   /** 讀取檔案選項.
    * call by {@link EventLoadGame}
    * @param geis 所有檔案紀錄的GameEnvInfo, geis[]的順序與選項順序無關,
    *             geis[].num才是選項的index. 
    * @return geis的index,不是選項的的index ; -1代表Cancel. */
   public int queryLoadGame( GameEnvInfo[] geis );

   /** 結束一個 UI, 告訴他說 我要關掉你啦XD */
   public void turnOff();

   //=================== 一般事件. ==================== 
   /** 點選某個OK按鈕. */
   public void click( String msg );
   /** 顯示訊息 並點任意地方繼續. */
   public void showMessage( String msg );

   /** 詢問Yes/No with specified Yes/No的字串. */
   public boolean queryYesNo(String msg, String yesMsg, String noMsg );
   /** 詢問Yes/No with default Yes/No的字串. */
   public boolean queryYesNo(String msg);

   /** 詢問多個選項.
    * 點選某個選項回傳他的Index. */
   public int queryMultiChoice( String msg, String[] options );
   /** 詢問多個選項,並且有取消的選項(包含在GUI裡點右鍵?).
    * 點選某個選項回傳他的Index. 取消則return -1.*/
   public int queryMultiChoiceWithCancel( String msg, String[] options );
   /** 詢問多個選項,並且有取消的選項(包含在GUI裡點右鍵?).
    * 點選某個選項回傳他的Index. 取消則return -1.*/
   public int queryMultiChoiceWithCancel( String msg, String[] options , String cancelMsg );

   /** 輸入數字的選項. */
   public int queryInteger(String msg);
   public int queryInteger(String msg,int start,int end);

   /** 找尋可以放置物件的 Block.
    * return null代表Cancel*/
   public Block queryChooseBlock(Standable s);

   /** 找尋可以選擇的Upgradable.(building?)
    * return null代表Cancel*/
   public Upgradable queryChooseUpgradable();

   /** 找尋可以選擇的EmptyLand
    * return null代表Cancel*/
   public LandEmpty queryChooseLandEmpty();

   /** 找尋可以選擇的Player.(不包括except) except可以為null表是連自己都可以選
    * return null代表Cancel*/
   public Player queryChoosePlayer(Player except);

   //=================== 跟事件中要顯示訊息比較無關的. ==================== 
   /** 每回合使用者可以從選單取得各種事件,
    * 一直到取到的事件是EventThrowDices. */
   public Event getEvent();

   /** 每回合的一開始, 會呼叫 updateNewRound 顯示新回合的開始 */
   public void updateNewRound();

   /** 整個畫面的刷新 */
   @Deprecated
      public void updateView();

   /** 地圖畫面的刷新, 盡量不要呼叫這個, 用 {@link updateView} */
   @Deprecated
      public void updateViewMap();

   /** 玩家畫面更新 */
   @Deprecated
      public void updateViewPlayer();
}
