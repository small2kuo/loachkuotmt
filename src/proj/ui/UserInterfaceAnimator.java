package proj.ui;

import proj.*;
import proj.event.*;
import proj.player.*;
import proj.map.*;

/** 顯示動畫的Interface. */
public interface UserInterfaceAnimator{
   /** call by {@link EventPlayerMove}. */
   public void showPlayerMove( Player user , Block fromBlock , Block toBlock);
   /** call by {@link EventThrowDices}. */
   public void showThrowDices( Player user , int dice_num );
   
   /** 遊戲節奏暫停. 這部分原本應該屬於 GameTool 的, 不過為了畫面更新效果上先寫在這裡 */
   public void gamePause( int delay_msec );

   /** 指定地圖區塊閃爍 */
   public void flash(MapComponentVisitable mcv, int times, int delay, int finaldelay);
   /** 同時閃爍多塊區域*/
   public void flash(MapComponentVisitable []mcv_arr, int times, int delay, int finaldelay);

   /** 淡出 */
   public void fadeOut(int delay_msec);
   /** 淡入 */
   public void fadeIn(int delay_msec);
}
