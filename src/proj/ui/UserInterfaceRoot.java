package proj.ui;

import proj.event.*;
import proj.player.*;
import proj.map.*;
import proj.*;

/** 就是GameRoot的UI. */
public interface UserInterfaceRoot{
   /** 最一開始的遊戲選單. */
   public int startMenu( String[] opts, String[] menuImgSrc ); 
   /** 讀入遊戲的選單.
    * TODO 要加上回前頁的選項(return -1) */
   public int showLoadGameList( GameEnvInfo[] eis );
   
   /** 在開心遊戲的選單讀入使用者所做的操作.
    * Object[0] = 操作{ Integer(0): 開始遊戲;  Integer(1): 回主選單 }<br>
    * if Object[0] == 0 then<br>
    *    Object[1] = 選擇的MapInfo<br>
    *    Object[2] = 選擇的PlayerData[]<br>
    *    Object[3] = 選擇的使用者的姓名String[]<br>
    * endif
    * */
   public Object[] getNewGameListOption( MapInfo[] maps , PlayerData[] ps );

   //TODO [Q]是不是要加一個StartGame, QuitGame之類的signal來通知ui??
}
