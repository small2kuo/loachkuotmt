package proj.util;

import proj.map.building.BuildingData;
import java.io.*;
import java.util.*;
/**
 * 這個程式把 data/building/下所有.txt的檔案
 * 全部轉換為 .dat 格式的檔案.
 * 一個 BuildingData 的輸入資料格式為
 *
 *  [名字]
 *  [最大等級 Lv]   //如果最大等級為 1 的話代表這個建築物並非可升級的建築物.
 *  [等級字串]
 *
 *  ...   //這邊有 Lv 行, 每一行代表一個等級所顯示的字串
 *  
 *  [升級花費] [路過繳費]
 *
 *  ...   //這邊有 Lv 列, 每一列代表路過的繳費, 等級為 1 的第一個數字則為 [購買(建造)費用] 
 *
 *  [等級圖示]
 *
 *  ...   //每一個 Lv 都有對應的圖片來源, 這個程式會把它讀出來並儲存在 .dat 裡面
 *
 */
public class BuildingWriter{
   public static void main(String[] args){
      String dirName = "data/building/";
      String[] fileName = getAllFiles( dirName );
      for( int i=0; i<fileName.length ; ++i )
         txt_to_dat( dirName , fileName[i] );
   }
   public static String[] getAllFiles( String dirName ){
      ArrayList<String> fileName = new ArrayList<String>(0); 
      //取得檔案列表
      String[] files = ( new java.io.File( dirName ) ).list();
      //篩選檔案
      for(int i=0;i<files.length;++i)
         if( java.util.regex.Pattern.matches(".*[.]txt", files[i]) )
            fileName.add( files[i] );
      return fileName.toArray( new String[0] );
   }
   public static void txt_to_dat( String dirName , String fileName ){
      String txtPath = dirName + fileName;
      String datPath = dirName + fileName.replaceAll( "[.]txt$" , ".dat" );
      System.out.println("轉換 "+fileName+" 中...");
      try{
         //讀取.txt檔
         Scanner input = new Scanner( new FileInputStream( txtPath ) );
         BuildingData bd = new BuildingData();
         {//=====讀資料的區塊=====
            bd.name = input.nextLine().trim(); //名字
            bd.maxLv = Integer.parseInt( input.nextLine() ); //最大等級
            bd.upgradeCost = new int[bd.maxLv+1];
            bd.msgLevel = new String[bd.maxLv+1];
            bd.passFee = new int[bd.maxLv+1];
            bd.imgPath = new String[bd.maxLv+1];
            for(int i = 1; i <= bd.maxLv; i++){
               bd.msgLevel[i] = input.nextLine(); //等級資訊
            }
            bd.passFee[0] = 0;
            for(int i = 1; i <= bd.maxLv; i++){
               bd.upgradeCost[i] = input.nextInt(); //升級花費
               bd.passFee[i] = input.nextInt();
            }
            input.nextLine();
            for(int i = 1; i <= bd.maxLv; i++){
               bd.imgPath[i] = input.nextLine().trim(); //圖片資料
            }
         }//=====資料讀完喵=====
         input.close();

         //輸出.dat檔
         ObjectOutputStream oos = new ObjectOutputStream( new FileOutputStream( datPath ) );
         oos.writeObject(bd);
         oos.close();
      }catch( Exception e ){
         System.err.println(e);
      }
   }
}
