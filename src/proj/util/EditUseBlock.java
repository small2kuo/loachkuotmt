package proj.util.editor;

import proj.util.*;
import proj.map.*;
import proj.*;
import java.util.ArrayList;
import java.awt.*;

public class EditUseBlock extends Block implements StatusViewable{
	private Shape corresponding_shape = null;
	public void setShape(Shape s){
		corresponding_shape = s;
	}
	public Shape getShape(){
		return corresponding_shape;
	}
	public EditUseBlock(Street s){
		super(s, new EditUseLand(s));
	}
	public Land getLand(){
		return land;
	}
	public void setLand(Land l){
		this.land = l;
	}

	public String checksetting(){
		if(corresponding_shape != null)
			return "";
		return "*";
	}
	public String toString(){
		return checksetting() + "Block: index(" + getStreet().indexOf(this) + ")";
	}
	//======== StatusViewable ===========
	public void viewStatus(StatusUpdatable su){
		su.updateStatus(this);
	}
}
