package proj.util.editor;

import proj.map.building.*;
import proj.util.*;
import proj.map.*;
import proj.*;
import java.util.ArrayList;
import java.awt.*;

public class EditUseLand extends LandEmpty implements StatusViewable{
	static int IDs = 0;
	private ArrayList<BuildingType> bt_arr;
	private boolean canbuild;
	private Shape shape;
	private int ID;
	public void resetIDs(){
		if(ID > IDs)
			IDs = ID;
	}
	public int getID(){
		return ID;
	}
	public void setShape(Shape s){
		shape = s;
	}
	public Shape getShape(){
		return shape;
	}
	public EditUseLand(Street s){
		super(s);
		bt_arr = new ArrayList<BuildingType>();
		canbuild = true;
		ID = ++IDs;
	}
	public void addBuildingType(BuildingType bt){
		bt_arr.add(bt);
	}
	public void removeBuildingType(BuildingType bt){
		bt_arr.remove(bt);
	}
	public boolean canBuildBuilding(){
		return canbuild;
	}
	public void setCanBuild(){
		canbuild = true;
	}
	public void setCannotBuild(){
		canbuild = false;
	}
	public String checkstatus(){
		if(canbuild)
			return "(o)" + ((shape!=null)?"":"*");
		else
			return "(x)" + ((shape!=null)?"":"*");
	}
	public String toString(){
		return checkstatus() + "Land(ID:" + ID + ")[" + bt_arr.size() + "]";
	}
	public ArrayList<BuildingType> getBuildingTypes(){
		return bt_arr;
	}
	public BuildingType[] getAvailBuildingTypes(){
		return bt_arr.toArray(new BuildingType[0]);
	}
	//======== StatusViewable ===========
	public void viewStatus(StatusUpdatable su){
		su.updateStatus(this);
	}
}
