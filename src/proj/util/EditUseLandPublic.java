package proj.util.editor;

import proj.map.building.*;
import proj.util.*;
import proj.map.*;
import proj.*;
import java.util.ArrayList;
import java.awt.*;

public class EditUseLandPublic extends LandPublic implements StatusViewable{
	static int IDs = 0;
	private Shape shape;
	private int ID;
	public void resetIDs(){
		if(ID > IDs)
			IDs = ID;
	}
	public int getID(){
		return ID;
	}
	public void setShape(Shape s){
		shape = s;
	}
	public Shape getShape(){
		return shape;
	}
	public EditUseLandPublic(Street s,Building b){
		super(s,b);
		ID = ++IDs;
	}
	public String toString(){
		return "Land Public(ID:" + ID + ")";
	}
  //======== StatusViewable ===========
	public void viewStatus(StatusUpdatable su){
		su.updateStatus(this);
	}
}
