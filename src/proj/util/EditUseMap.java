package proj.util.editor;

import proj.util.*;
import proj.map.*;
import java.util.ArrayList;

public class EditUseMap extends Map implements StatusViewable{
	public EditUseMap(){
		super("software");
	}
	/** EDITABLE: 增加一個 Street */
	public void addStreet(Street s){
		streetSet.add(s);
	}
	/** EDITABLE: 移除一個 Street */
	public void removeStreet(Street s){
		streetSet.remove(s);
	}
	/** 取得所有 Street */
	public ArrayList<Street> getStreetArrayList(){
		return streetSet;
	}
	public String toString(){
		return "Map: " + this.getName();
	}
		//======== StatusViewable ===========
	public void viewStatus(StatusUpdatable su){
		su.updateStatus(this);
	}
}
