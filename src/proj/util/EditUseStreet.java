package proj.util.editor;

import proj.util.*;
import proj.map.*;
import proj.*;
import java.util.ArrayList;
import java.awt.*;

public class EditUseStreet extends Street implements StatusViewable{
	private ArrayList<EditUseBlock> block;
	private Shape shape;
	public EditUseStreet(){
		super(0);
		block = new ArrayList<EditUseBlock>();
		shape = null;
	}
	/** 取得 shape(path形狀) */
	public Shape getShape(){
		return shape;
	}
	/** 存入 shape */
	public void setShape(Shape s){
		this.shape = s;
	}

	 /** 取得 Block */
	@Override
	public Block getBlockByIndex(int index){
      return block.get(index);
	}
	/** 增加 Block 回傳index */
	public int addBlock(EditUseBlock b){
		block.add(b);
		return indexOf(b);
	}
   /** 取得某個 block 所在的索引編號 */
	@Override
   public int indexOf(Block b){
      for(int i = 0; i < block.size(); i++){
         if(b == block.get(i))
            return i;
      }
      return -1; //Not Found
   }
   /** 取得街道上Block的數量. */
   public int getBlockSize(){
      return block.size();
   }
   /** 設定 Block, 修改用的. */
   public void setBlock(int index, EditUseBlock b){
	   block.set(index, b);
   }
   /** 移除 Block */
   public void removeBlock(Block b){
	   block.remove(b);
   }
	/** 編輯用的顯示 */
   public String toString(){
	   return "Street: " + this.getName() + "[" + block.size() + "]";
   }
   	//======== StatusViewable ===========
	public void viewStatus(StatusUpdatable su){
		su.updateStatus(this);
	}
}
