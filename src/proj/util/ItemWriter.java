package proj.util;

import proj.*;
import proj.map.building.*;
import proj.item.*;
import java.io.*;
import java.util.*;
/**
 * 這個程式把 {@value proj.GameTool#DIR_Item}下所有.txt的檔案
 * 全部轉換為 .dat 格式的檔案.
 * 一個 ItemData 的輸入資料格式為
 *  [名字(String)]
 *  [在手上的圖片path(String)]
 *  [在地上的圖片path(String)]
 *  @see ItemData
 */
public class ItemWriter{
   public static void main(String[] args){
      String dirName = GameTool.DIR_Item;
      String[] fileName = getAllFiles( dirName );
      for( int i=0; i<fileName.length ; ++i )
         txt_to_dat( dirName , fileName[i] );
   }
   public static String[] getAllFiles( String dirName ){
      ArrayList<String> fileName = new ArrayList<String>(0); 
      //取得檔案列表
      String[] files = ( new java.io.File( dirName ) ).list();
      //篩選檔案
      for(int i=0;i<files.length;++i)
         if( java.util.regex.Pattern.matches(".*[.]txt", files[i]) )
            fileName.add( files[i] );
      return fileName.toArray( new String[0] );
   }
   public static void txt_to_dat( String dirName , String fileName ){
      String txtPath = dirName + fileName;
      String datPath = dirName + fileName.replaceAll( "[.]txt$" , ".dat" );
      System.out.println("轉換 "+fileName+" 中...");
      try{
         //讀取.txt檔
         Scanner input = new Scanner( new FileInputStream( txtPath ) );
         ItemData id = new ItemData(null,null,null);

         id.name = input.nextLine().trim();
         id.handImgPath = input.nextLine().trim();
         id.groundImgPath = input.nextLine().trim();
         
         input.close();

         //輸出.dat檔
         ObjectOutputStream oos = new ObjectOutputStream( new FileOutputStream( datPath ) );
         oos.writeObject(id);
         oos.close();
      }catch( Exception e ){
         System.err.println(e);
      }
   }
}
