/** 這邊放中文的註解吧!! */
package proj.util;

import proj.*;
import proj.map.*;
import proj.map.building.*;
import proj.util.editor.*;

import java.io.*;
import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.swing.filechooser.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Enumeration;


/** 地圖編輯器，負責製作 MapFile */
public class MapEditor extends JFrame implements TreeSelectionListener{
   JFileChooser fileChooser = new JFileChooser(new File("."));
   String mapFileName = null;
   Map map;
   MapEditPanel mapPanel;
   ToolBox toolbox;
   StatusBarPanel statusBar;
   JTree tree;
   JScrollPane treePane;
   JPanel treePanel;
   DefaultMutableTreeNode selectedNode = null;
   Object selectedObj = null;
   MapSettingPanel settingPanel;
   Image bg_img = null;
   JButton[] fb = new JButton[14]; //Functional Buttons
   public static final int BUT_SAVEMAP = 0;
   public static final int BUT_LOADMAP = 1;
   public static final int BUT_ADDBLOCK = 2;
   public static final int BUT_ADDSTREET = 3;
   public static final int BUT_DRAGOBJECT = 4;
   public static final int BUT_NONE = 5;
   public static final int BUT_SETNAME = 6;
   public static final int BUT_ADDSTREETTO = 7;
   public static final int BUT_REMOVESTREETTO = 8;
   public static final int BUT_CHANGEMAPBGIMG = 9;
   public static final int BUT_DELETESTREET = 10;
   public static final int BUT_DELETEBLOCK = 11;
   public static final int BUT_CHANGELAND = 12;
   public static final int BUT_TRANSLATION = 13;

   public MapEditor(Map map){
      this.map = map;
      this.setLayout(new BorderLayout());
      this.setSize(1100, 634);
      this.setLocation(100, 100);
      this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      this.setTitle("[New Map]");
      initComponents();
      this.setVisible(true);
   }
   /** 初始化 */
   protected void initComponents(){
      //DefaultMutableTreeNode root = new DefaultMutableTreeNode(map);
      tree = new MapTree(this);
      treePane = new JScrollPane(tree);
      treePanel = new JPanel(new BorderLayout());
      treePanel.add(treePane, BorderLayout.CENTER);
      //treePane.setPreferredSize(new Dimension(300, -1));

      JPanel p = new JPanel(new BorderLayout());
      settingPanel = new MapSettingPanel();
      JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, p,
            new JSplitPane(JSplitPane.VERTICAL_SPLIT, treePanel, new JScrollPane(settingPanel)));
      this.add(splitPane, BorderLayout.CENTER);
      splitPane.setOneTouchExpandable(true);
      splitPane.setDividerLocation(800);
      ((JSplitPane) splitPane.getRightComponent()).setOneTouchExpandable(true);
      ((JSplitPane) splitPane.getRightComponent()).setDividerLocation(400);

      //.add(treePane, BorderLayout.EAST);

      loadBackgroundImage(getMapData().bgImgSrc); /*"data/map_soft/bg/bg_sky.jpg"*/
      mapPanel = new MapEditPanel(this);
      p.add(mapPanel, BorderLayout.CENTER);
      toolbox = new ToolBox(mapPanel);
      p.add(new JScrollPane(toolbox), BorderLayout.NORTH);
      statusBar = new StatusBarPanel();
      p.add(statusBar, BorderLayout.SOUTH);

   }
   public String getMapFileName(){
      if(mapFileName == null)
         return "[New Map]";
      else
         return mapFileName;
   }
   public void loadBackgroundImage(String filename){

      if(filename!=null){
         bg_img = Toolkit.getDefaultToolkit().createImage(filename);
         System.err.println("filename: " + filename);
         setTitle("Image Loading...");
         /* 這段Code檢查圖片是否已經完全載入了!!!!! */
         while(!Toolkit.getDefaultToolkit().prepareImage(bg_img, -1, -1, mapPanel)){
            try{Thread.sleep(50);}catch(Exception e){}
            setTitle(getTitle() + ".");
         }
         setTitle(mapFileName);
      }else{
         bg_img = null;
      }
   }
   public MapSettingPanel getSettingPanel(){
      return settingPanel;
   }
   public StatusBarPanel getStatusBar(){
      return statusBar;
   }
   public MapEditPanel getMapPanel(){
      return mapPanel;
   }
   /** 顯示狀態用的 Panel */
   protected class StatusBarPanel extends JPanel{
      JLabel loc;
      MapEditPanel m;
      public StatusBarPanel(){
         loc = new JLabel("This is the line.");
         this.setLayout(new FlowLayout());
         this.add(loc);
         m = getMapPanel();
      }
      public void update(){
         loc.setText("(" + m.mouseRealX + "," + m.mouseRealY + ")");
      }
   }
   public MapData getMapData(){
      return map.getMapData();
   }

   /** 顯示與編輯地圖 Panel */
   protected class MapEditPanel extends JPanel implements MouseListener, MouseMotionListener{
      MapEditor editor;
      int cntX, cntY;
      int mouseRealX, mouseRealY;
      boolean mouseOn = false;
			
      public String drawMode = "NONE";
      static public final String NONE = "NONE";
      static public final String SAVE = "save";
      static public final String LOAD = "load";
      static public final String ADDSTREET = "add street";
      static public final String ADDBLOCK = "add block";
      static public final String ADDLAND = "add land";
      static public final String DRAGOBJECT = "drag object";
      static public final String SETNAME = "set name";
      static public final String ADDSTREETTO = "add streetTo";
      static public final String REMOVESTREETTO = "remove streetTo";
      static public final String CHANGEMAPBGIMG = "change map bg_img";
      static public final String DELETESTREET = "delete street";
      static public final String DELETEBLOCK = "delete block";
      static public final String CHANGELAND = "change land";
      static public final String TRANSLATION = "translation the whole map";
      public MapEditPanel(MapEditor editor){
         this.editor = editor;
         this.setBounds(0, 0, 800, 600);
         this.setOpaque(false);
         cntX = 1500;
         cntY = 1200;
         this.addMouseListener(this);
         this.addMouseMotionListener(this);
      }
      int RS = 0;
      synchronized public void paint(Graphics g){

         //System.err.println(bg_img + " " + cntX + " " + cntY + " " + getWidth() + " " + getHeight() + " " + (++RS));
         //((Graphics2D) g).setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.7f));
         int LX = cntX - getWidth()/2;
         int LY = cntY - getHeight()/2;
         if(LX < 0) LX = 0;
         if(LY < 0) LY = 0;
         if(bg_img != null){
            if(LX > bg_img.getWidth(this)-getWidth()) LX = bg_img.getWidth(this)-getWidth();
            if(LY > bg_img.getHeight(this)-getHeight()) LY = bg_img.getHeight(this)-getHeight();

            g.drawImage(bg_img,
                  0, 0, getWidth(), getHeight(),
                  LX, LY, LX +getWidth(), LY + getHeight(),
                  this);
         }
         //DRAW ALL ELEMENTS HERE
         Graphics2D g2 = (Graphics2D) g.create();

         java.util.ArrayList<Shape> allShapes = getMapData().allShapes;
         //System.err.println("allshapes: " + getMapData().allShapes.size());
         for(int i = 0; i < allShapes.size(); i++){
            //g2.setColor( new Color(0, 0, 255));
            //g2.fill( AffineTransform.getTranslateInstance(-(cntX-getWidth()/2), -(cntY-getHeight()/2)).
            //		createTransformedShape(allShapes.get(i)) );
            Object o = getMapData().hashmap.get(allShapes.get(i));
            if(o instanceof EditUseLand){
               g2.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10.0f, new float[]{7.0f, 7.0f}, 0.0f));
               g2.setColor( new Color(55, 55, 55, 155));
               g2.draw( AffineTransform.getTranslateInstance(-(cntX-getWidth()/2)+2, -(cntY-getHeight()/2)+2).
                     createTransformedShape(allShapes.get(i)) );
               g2.setColor( new Color(185, 185, 155));
               g2.draw( AffineTransform.getTranslateInstance(-(cntX-getWidth()/2), -(cntY-getHeight()/2)).
                     createTransformedShape(allShapes.get(i)) );
            }else if(o instanceof EditUseLandPublic){
               g2.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10.0f, new float[]{7.0f, 7.0f}, 0.0f));
               g2.setColor( new Color(55, 55, 55, 155));
               g2.draw( AffineTransform.getTranslateInstance(-(cntX-getWidth()/2)+2, -(cntY-getHeight()/2)+2).
                     createTransformedShape(allShapes.get(i)) );
               g2.setColor( new Color(250, 55, 55));
               g2.draw( AffineTransform.getTranslateInstance(-(cntX-getWidth()/2), -(cntY-getHeight()/2)).
                     createTransformedShape(allShapes.get(i)) );
            }else if(o instanceof EditUseBlock){
               g2.setStroke(new BasicStroke(2.0f));
               g2.setColor(new Color(55,55,255));
               g2.fill( AffineTransform.getTranslateInstance(-(cntX-getWidth()/2), -(cntY-getHeight()/2)).
                     createTransformedShape(allShapes.get(i)) );
            }else if(o instanceof EditUseStreet){
               g2.setStroke(new BasicStroke(10.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 10.0f, new float[]{15.0f, 25.0f}, 0.0f));
               g2.setColor(new Color(55,55,255));
               g2.draw( AffineTransform.getTranslateInstance(-(cntX-getWidth()/2), -(cntY-getHeight()/2)).
                     createTransformedShape(allShapes.get(i)) );
            }
            if(o == selectedObj || o == allShapes.get(i)){
               g2.setColor( new Color(255, 255, 0));
               g2.setStroke(new BasicStroke(5.0f));
               g2.draw( AffineTransform.getTranslateInstance(-(cntX-getWidth()/2), -(cntY-getHeight()/2)).
                     createTransformedShape(allShapes.get(i)) );
            }

         }

         if(mouseOn){
            if(getSelectedObj() instanceof EditUseLand){
               g.setColor(new Color(0, 0, 255, 155));
               g.fillRect(mouseX-32, mouseY-32, 64, 64);
            }else if(getSelectedObj() instanceof EditUseLandPublic){
               g.setColor(new Color(0, 255, 0, 155));
               g.fillRect(mouseX-32, mouseY-32, 64, 64);
            }else if(getSelectedObj() instanceof EditUseBlock){
               g.setColor(new Color(255, 0, 0, 155));
               g.fillOval(mouseX-24, mouseY-24, 48, 48);
            }
         }

         super.paint(g2);
         g2.dispose();
      }
      public void changeDrawMode(String s){
         drawMode = s;
         editor.setTitle("DrawMode: " + s);
      }
      //========= MouseListener ===========
      private int mouseX, mouseY;
      /** 當滑鼠點擊時可以把一個 Block 或 Land 畫到地圖上 */
      synchronized public void mouseClicked(MouseEvent e){
         //1是滑鼠左鍵
         if(e.getButton() == MouseEvent.BUTTON1){
            if(getSelectedObj() instanceof EditUseLand){

               //Land 是一個 Rectangle 的 Shape
               EditUseLand e_land = (EditUseLand) getSelectedObj();
               if(e_land.getShape() != null){
                  Shape s = e_land.getShape();
                  getMapData().allShapes.remove(s);
                  getMapData().hashmap.remove(s);
               }
               Rectangle r = new Rectangle(mouseRealX - 32, mouseRealY - 32, 64, 64);
               getMapData().allShapes.add(r);
               getMapData().hashmap.put(r, e_land);
               e_land.setShape(r);
               updateTree();
               //在這個位置放一個 Land
            }else if(getSelectedObj() instanceof EditUseLandPublic){

               //Land 是一個 Rectangle 的 Shape
               EditUseLandPublic e_land = (EditUseLandPublic) getSelectedObj();
               if(e_land.getShape() != null){
                  Shape s = e_land.getShape();
                  getMapData().allShapes.remove(s);
                  getMapData().hashmap.remove(s);
               }
               Rectangle r = new Rectangle(mouseRealX - 32, mouseRealY - 32, 64, 64);
               getMapData().allShapes.add(r);
               getMapData().hashmap.put(r, e_land);
               e_land.setShape(r);
               updateTree();
               //在這個位置放一個 Land
            }else if(getSelectedObj() instanceof EditUseBlock){

               EditUseBlock e_block = (EditUseBlock) getSelectedObj();
               if(e_block.getShape() != null){
                  Shape s = e_block.getShape();
                  getMapData().allShapes.remove(s);
                  getMapData().hashmap.remove(s);
               }
               //Block 是一個 Circle
               Ellipse2D.Double r = new Ellipse2D.Double(mouseRealX-24, mouseRealY-24, 48, 48);
               getMapData().allShapes.add(r);
               getMapData().hashmap.put(r, e_block);
               e_block.setShape(r);
               updateTree();

               makeRoutes((EditUseStreet) e_block.getStreet());
               //在這個位置放一個 Block
            }
         }else if(e.getButton() == MouseEvent.BUTTON3){
            clearTree();
         }
      }
      /** 如果有滑鼠按到了某個東西, 就會科科 */
      synchronized public void mousePressed(MouseEvent e){
         if(getSelectedNode() != null)
            return;
         ArrayList<Shape> allShapes = getMapData().allShapes;
         for(int i = 0; i < allShapes.size(); i++)
            if(allShapes.get(i).contains(mouseRealX, mouseRealY)){
               Object o = getMapData().hashmap.get(allShapes.get(i));
               if(o instanceof EditUseBlock || o instanceof EditUseLand || o instanceof EditUseLandPublic){
                  drawMode = DRAGOBJECT;
                  selectedObj = allShapes.get(i);
                  return;
               }
            }
      }
      /** 重新調整地圖圖片的位置 */
      synchronized public void mouseReleased(MouseEvent e){
         if(cntX < getWidth()/2) cntX = getWidth()/2;
         if(cntY < getHeight()/2) cntY = getHeight()/2;
         if(bg_img != null){
            if(cntX > bg_img.getWidth(this)-getWidth()/2) cntX = bg_img.getWidth(this)-getWidth()/2;
            if(cntY > bg_img.getHeight(this)-getHeight()/2) cntY = bg_img.getHeight(this)-getHeight()/2;
         }
         if(drawMode != NONE){
            selectedObj = (selectedNode == null)? null: selectedNode.getUserObject(); //還原 DRAGOBJECT
            drawMode = NONE;
         }
      }
      public void mouseEntered(MouseEvent e){mouseOn = true;repaint();}
      public void mouseExited(MouseEvent e){mouseOn = false;repaint();}

      //========= MouseMotionListener ==========
      /** 重新計算地圖中心位置 */
      synchronized public void mouseDragged(MouseEvent e){
         if(drawMode == NONE){
            cntX -= e.getX()-mouseX;
            cntY -= e.getY()-mouseY;
            mouseX = e.getX();
            mouseY = e.getY();
            this.repaint();
         }else if(drawMode == DRAGOBJECT){
            //TODO 拉到邊邊的時候會有Bug...要注意
            AffineTransform at = AffineTransform.getTranslateInstance(e.getX()-mouseX, e.getY()-mouseY);
            mouseX = e.getX();
            mouseY = e.getY();
            Shape s = (Shape) getSelectedObj();
            MapComponentVisitable o = getMapData().hashmap.get(s);
            //System.err.println("s = " + s + ", o = " + o);
            getMapData().hashmap.remove(s);
            getMapData().allShapes.remove(s);

            Shape news = at.createTransformedShape(s);
            getMapData().allShapes.add(news);
            getMapData().hashmap.put(news, o);
            if(o instanceof EditUseBlock){
               //System.out.println("HERE");
               ((EditUseBlock) o).setShape(news);
               makeRoutes((EditUseStreet) ((EditUseBlock) o).getStreet());
            }else if(o instanceof EditUseLand){
               ((EditUseLand) o).setShape(news);
            }else if(o instanceof EditUseLandPublic){
               ((EditUseLandPublic) o).setShape(news);
            }
            //System.err.println("shape " + getMapData().allShapes.size());
            selectedObj = news;

            this.repaint();
         }else if(drawMode == TRANSLATION){
				 //平移所有shape
            AffineTransform at = AffineTransform.getTranslateInstance(e.getX()-mouseX, e.getY()-mouseY);
            mouseX = e.getX();
            mouseY = e.getY();
						for(int i=0;i<getMapData().allShapes.size();i++){
						  Shape s = (Shape) getMapData().allShapes.get(i);
              MapComponentVisitable o = getMapData().hashmap.get(s);
              if( o instanceof EditUseStreet)continue;
							getMapData().hashmap.remove(s);
              getMapData().allShapes.remove(s);							
							
							Shape news = at.createTransformedShape(s);
							getMapData().allShapes.add(news);
              getMapData().hashmap.put(news, o);
              if(o instanceof EditUseBlock){
                 ((EditUseBlock) o).setShape(news);
                 makeRoutes((EditUseStreet) ((EditUseBlock) o).getStreet());
              }else if(o instanceof EditUseLand){
                 ((EditUseLand) o).setShape(news);
              }else if(o instanceof EditUseLandPublic){
                 ((EditUseLandPublic) o).setShape(news);
              }
						}
            updateTree();
            makeRoutes();
            updateAll();
            this.repaint();
				 }
      }
      /** 計算滑鼠相對與絕對位置 */
      public void mouseMoved(MouseEvent e){
         mouseX = e.getX();
         mouseY = e.getY();
         mouseRealX = (cntX-getWidth()/2+e.getX());
         mouseRealY = (cntY-getHeight()/2+e.getY());
         getStatusBar().update();
         if(getSelectedObj() instanceof EditUseLand) this.repaint();
         if(getSelectedObj() instanceof EditUseLandPublic) this.repaint();
         if(getSelectedObj() instanceof EditUseBlock) this.repaint();
      }
      public Map getMap(){
         return map;
      }
   }
   /** 工具籃 */
   protected class ToolBox extends JPanel{
      private MapEditPanel mapPanel;
      public void createButton(int index, String str, String actionCommand, ButtonActionListener l, boolean visible){
         fb[index] = new JButton(str);
         fb[index].setActionCommand(actionCommand);
         fb[index].addActionListener(l);
         fb[index].setVisible(visible);
         this.add(fb[index]);
      }
      public ToolBox(MapEditPanel m){
         this.mapPanel = m;
         this.setLayout(new FlowLayout());
         this.setBounds(900, 100, 100, 300);

         ButtonActionListener l = new ButtonActionListener();
         createButton(BUT_SAVEMAP, "SaveMap", MapEditPanel.SAVE, l, true);
         createButton(BUT_LOADMAP, "LoadMap", MapEditPanel.LOAD, l, true);
         createButton(BUT_ADDBLOCK, "Add Block", MapEditPanel.ADDBLOCK, l, false);
         createButton(BUT_ADDSTREET, "Add Street", MapEditPanel.ADDSTREET, l, false);
         createButton(BUT_NONE, "None", MapEditPanel.NONE, l, true);
         createButton(BUT_SETNAME, "SetName", MapEditPanel.SETNAME, l, false);
         createButton(BUT_ADDSTREETTO, "增加出口街道", MapEditPanel.ADDSTREETTO, l, false);
         createButton(BUT_REMOVESTREETTO, "移除出口街道", MapEditPanel.REMOVESTREETTO, l, false);
         createButton(BUT_CHANGEMAPBGIMG, "更換背景", MapEditPanel.CHANGEMAPBGIMG, l, false);
         createButton(BUT_DELETESTREET, "移除street", MapEditPanel.DELETESTREET, l, false);
         createButton(BUT_DELETEBLOCK, "移除block", MapEditPanel.DELETEBLOCK, l, false);
         createButton(BUT_CHANGELAND, "變更Land屬性", MapEditPanel.CHANGELAND, l, false);
         createButton(BUT_TRANSLATION, "平移整張地圖", MapEditPanel.TRANSLATION, l, true);

         this.setVisible(true);
         this.revalidate();
         this.repaint();
      }
      protected class ButtonActionListener implements ActionListener{
         synchronized public void actionPerformed(ActionEvent e){
            //getMapPanel().changeDrawMode( e.getActionCommand() );

            Object o = getSelectedNode()==null? null:getSelectedNode().getUserObject();
            if(e.getActionCommand() == MapEditPanel.NONE){
               clearTree();
               //DO NOTHING
            }else if(e.getActionCommand() == MapEditPanel.ADDSTREET){
               if(o instanceof EditUseMap){
                  EditUseMap e_map = (EditUseMap) o;
                  EditUseStreet s = new EditUseStreet();
                  e_map.addStreet(s);
                  DefaultMutableTreeNode node = new DefaultMutableTreeNode(s);
                  getSelectedNode().add(node);
                  DefaultMutableTreeNode lastnode = getSelectedNode();
                  updateTree(node);
                  updateTree(lastnode);
               }
            }else if(e.getActionCommand() == MapEditPanel.ADDLAND){
            }else if(e.getActionCommand() == MapEditPanel.ADDBLOCK){
               if(o instanceof EditUseStreet){
                  EditUseStreet e_street = (EditUseStreet) o;
                  EditUseBlock b = new EditUseBlock(e_street);
                  e_street.addBlock(b);
                  DefaultMutableTreeNode node = new DefaultMutableTreeNode(b);
                  getSelectedNode().add(node);

                  if(b.getLand() != null){
                     DefaultMutableTreeNode node2 = new DefaultMutableTreeNode(b.getLand());
                     node.add(node2);
                  }
                  DefaultMutableTreeNode lastnode = getSelectedNode();
                  updateTree(node);
                  updateTree(lastnode);
               }
            }else if(e.getActionCommand() == MapEditPanel.DRAGOBJECT){
            }else if(e.getActionCommand() == MapEditPanel.SETNAME){
               if(o != null){
                  Object ret;
                  ret = JOptionPane.showInputDialog(getMapPanel(), "Fill it please: ", "Set Name", JOptionPane.PLAIN_MESSAGE, null, null, "");
                  if(ret != null){
                     String rets = (String) ret;
                     if(o instanceof EditUseMap){
                        EditUseMap e_map = (EditUseMap) o;
                        e_map.setName(rets);
                        updateTree();
                     }else if(o instanceof EditUseStreet){
                        EditUseStreet e_street = (EditUseStreet) o;
                        e_street.setName(rets);
                        updateTree();
                     }
                  }
               }
            }else if(e.getActionCommand() == MapEditPanel.SAVE){
               FileNameExtensionFilter filter = new FileNameExtensionFilter("Editing Game Map Files", "emap");
               fileChooser.setFileFilter(filter);
               if(mapFileName != null){
                  fileChooser.setSelectedFile(new File(mapFileName));
               }
               int returnVal = fileChooser.showSaveDialog(getMapPanel());
               if(returnVal == JFileChooser.APPROVE_OPTION) {
                  System.err.println("You chose to save the file to: " +
                        fileChooser.getSelectedFile().getName());
                  String filename = fileChooser.getSelectedFile().getName();
                  if(filename.indexOf(".") == -1)
                     filename = filename + ".emap";
                  File file = new File(fileChooser.getSelectedFile().getParent() + File.separator + filename);
                  saveMap(file);
               }
               fileChooser.removeChoosableFileFilter(filter);
            }else if(e.getActionCommand() == MapEditPanel.LOAD){
               FileNameExtensionFilter filter = new FileNameExtensionFilter("Editing Game Map Files (*.emap)", "emap");
               fileChooser.setFileFilter(filter);
               int returnVal = fileChooser.showOpenDialog(getMapPanel());
               if(returnVal == JFileChooser.APPROVE_OPTION) {
                  System.err.println("You chose to load this file: " +
                        fileChooser.getSelectedFile().getName());
                  loadMap(fileChooser.getSelectedFile());
               }
               fileChooser.removeChoosableFileFilter(filter);
            }else if(e.getActionCommand() == MapEditPanel.ADDSTREETTO){
               if(o instanceof EditUseStreet){
                  EditUseStreet f = (EditUseStreet) JOptionPane.showInputDialog(
                        getMapPanel(), "請選一個新的出口街道", o.toString(), JOptionPane.INFORMATION_MESSAGE, null,
                        map.getStreets(), null);
                  if(f != null){
                     ((EditUseStreet) o).addToStreet(f);
                     f.addFromStreet((EditUseStreet) o);
                     makeRoutes((EditUseStreet) o);
                  }
               }
            }else if(e.getActionCommand() == MapEditPanel.REMOVESTREETTO){
               if(o instanceof EditUseStreet){
                  EditUseStreet e_street = (EditUseStreet) o;
                  Street[] s_arr = new Street[ e_street.getStreetToSize() ];
                  for(int i = 0; i < s_arr.length; i++)
                     s_arr[i] = e_street.getStreetTo()[i];
                  EditUseStreet f = (EditUseStreet) JOptionPane.showInputDialog(
                        getMapPanel(), "請選擇刪除一個出口街道", o.toString(), JOptionPane.INFORMATION_MESSAGE, null,
                        s_arr, null);
                  if(f != null){
                     e_street.removeToStreet(f);
                     f.removeFromStreet(e_street);
                     makeRoutes(e_street);
                  }
               }
               //TODO 移除出口街道
            }else if(e.getActionCommand() == MapEditPanel.CHANGEMAPBGIMG){
               //更換背景圖片.
               if(getMapData().bgImgSrc != null){
                  fileChooser.setSelectedFile(new File(getMapData().bgImgSrc));
               }
               FileNameExtensionFilter filter = new FileNameExtensionFilter("Picture/Images (*.png, *.jpg, *.bmp)", "png", "jpg", "bmp");
               fileChooser.setFileFilter(filter);
               int returnVal = fileChooser.showOpenDialog(getMapPanel());
               if(returnVal == JFileChooser.APPROVE_OPTION){
                  String fname = fileChooser.getSelectedFile().toString();
                  String dirname = new File(".").getAbsoluteFile().getParent() + File.separator;
                  fname = fname.substring( dirname.length() ); //取得相對路徑.
                  fname = fname.replace( File.separatorChar, '/');
                  getMapData().bgImgSrc = fname;
                  loadBackgroundImage(getMapData().bgImgSrc);
               }
               fileChooser.removeChoosableFileFilter(filter);
            }else if(e.getActionCommand() == MapEditPanel.DELETESTREET){
              //移除Street
              if(o instanceof EditUseStreet){
                System.out.println("移除"+(EditUseStreet)o);
                Object parent=((DefaultMutableTreeNode)getSelectedNode().getParent()).getUserObject();
                getSelectedNode().removeFromParent();
                if(parent instanceof EditUseMap){
                  EditUseMap e_map = (EditUseMap) parent;
                  System.out.println("從"+parent+"中移除");
                  EditUseStreet s = (EditUseStreet)o;
                  e_map.removeStreet(s);
                  ArrayList<Street> streetArray=e_map.getStreetArrayList();
                  for(int i=0;i<streetArray.size();i++){
                    streetArray.get(i).removeToStreet(s);
                    streetArray.get(i).removeFromStreet(s);
                  }
                  for(int i=0;i<s.getBlockSize();i++){
                    EditUseBlock e_block = (EditUseBlock)s.getBlockByIndex(i);
                    if(e_block == null) continue;
                    Shape shape = e_block.getShape();
                    getMapData().allShapes.remove(shape);
                    getMapData().hashmap.remove(shape);
                    
                    if(e_block.getLand() instanceof EditUseLand){
                      EditUseLand e_land=(EditUseLand)e_block.getLand();
                      shape = e_land.getShape();
                      getMapData().allShapes.remove(shape);
                      getMapData().hashmap.remove(shape);
                    }else if(e_block.getLand() instanceof EditUseLandPublic){
                      EditUseLandPublic e_land=(EditUseLandPublic)e_block.getLand();
                      shape = e_land.getShape();
                      getMapData().allShapes.remove(shape);
                      getMapData().hashmap.remove(shape);
                    }
                  }
                  getMapData().allShapes.remove(s.getShape());
                  getMapData().hashmap.remove(s.getShape());

                  selectedNode=null;
                  selectedObj=null;
                  
                  updateTree();
                  makeRoutes();
                  updateAll();
                }
              }
            }else if(e.getActionCommand() == MapEditPanel.DELETEBLOCK){
              //移除Block
              if(o instanceof EditUseBlock){
                System.out.println("移除"+(EditUseBlock)o);
                Object parent=((DefaultMutableTreeNode)getSelectedNode().getParent()).getUserObject();
                getSelectedNode().removeFromParent();
                if(parent instanceof EditUseStreet){
                  EditUseStreet e_street = (EditUseStreet) parent;
                  System.out.println("從"+parent+"中移除");
                  EditUseBlock e_block = (EditUseBlock)o;
                  e_street.removeBlock(e_block);
                  
                  Shape shape = e_block.getShape();
                  getMapData().allShapes.remove(shape);
                  getMapData().hashmap.remove(shape);
                  
                  
                  if(e_block.getLand() instanceof EditUseLand){
                    EditUseLand e_land=(EditUseLand)e_block.getLand();
                    shape = e_land.getShape();
                    getMapData().allShapes.remove(shape);
                    getMapData().hashmap.remove(shape);
                  }else if(e_block.getLand() instanceof EditUseLandPublic){
                    EditUseLandPublic e_land=(EditUseLandPublic)e_block.getLand();
                    shape = e_land.getShape();
                    getMapData().allShapes.remove(shape);
                    getMapData().hashmap.remove(shape);
                   }
                  selectedNode=null;
                  selectedObj=null;
                  
                  updateTree();
                  makeRoutes();
                  updateAll();
                }
              }
            }else if(e.getActionCommand() == MapEditPanel.CHANGELAND){
              //更改land性質
              if(o instanceof EditUseLand){
                BuildingType []bta=getPublicBuilding();
                BuildingType bt = (BuildingType) JOptionPane.showInputDialog(
                        getMapPanel(), "請選擇一種公共建築", null, JOptionPane.INFORMATION_MESSAGE, null,
                        bta, null);
                if(bt!=null){
                  DefaultMutableTreeNode parent = (DefaultMutableTreeNode)getSelectedNode().getParent(); //block
                  EditUseBlock e_block = (EditUseBlock)parent.getUserObject();
                  
                  //從tree中先刪除這個land
                  getSelectedNode().removeFromParent();
                  Shape s = ((EditUseLand)o).getShape();
                  //getMapData().allShapes.remove(s);
                  getMapData().hashmap.remove(s);
                  
                  //建立此公共土地上的公共建築
                  Building b = GameTool.getNewBuildingByType(bt,null,null);
                  EditUseLandPublic landPublic = new EditUseLandPublic(e_block.getStreet(),b);
                  b.setLand(landPublic);
                  landPublic.setShape(s);
                  
                  getMapData().hashmap.put(s,landPublic);
                  parent.add(new DefaultMutableTreeNode(landPublic));
                  e_block.setLand(landPublic);
                  

                  selectedNode=null;
                  selectedObj=null;
                  
                  updateTree();
                  makeRoutes();
                  updateAll();                  
                }
              }else if(o instanceof EditUseLandPublic){
                  DefaultMutableTreeNode parent = (DefaultMutableTreeNode)getSelectedNode().getParent(); //block
                  EditUseBlock e_block = (EditUseBlock)parent.getUserObject();
                  
                  //從tree中先刪除這個land
                  getSelectedNode().removeFromParent();
                  Shape s = ((EditUseLandPublic)o).getShape();
                  //getMapData().allShapes.remove(s);
                  getMapData().hashmap.remove(s);
                  
                  EditUseLand land = new EditUseLand(e_block.getStreet());
                  land.setShape(s);
                  
                  getMapData().hashmap.put(s,land);
                  parent.add(new DefaultMutableTreeNode(land));
                  e_block.setLand(land);
                  
                  selectedNode=null;
                  selectedObj=null;
                  
                  updateTree();
                  makeRoutes();
                  updateAll();
              }
					  }else if(e.getActionCommand() == MapEditPanel.TRANSLATION){
						  if(mapPanel.drawMode == MapEditPanel.TRANSLATION){
							  mapPanel.drawMode = MapEditPanel.NONE;
							}else{
							  mapPanel.drawMode = MapEditPanel.TRANSLATION;
							}
            }
         }
      }
      
      private ArrayList<BuildingType> publicBuilding;
      //回傳所有的publicBuildingType以供選擇
      private BuildingType[] getPublicBuilding(){
        if(publicBuilding!=null)
          return publicBuilding.toArray(new BuildingType[0]);
          
        publicBuilding = new ArrayList<BuildingType>(0);
        try{
          Scanner scan = new Scanner( new FileInputStream("data/publicBuilding.txt"));
          String buildingName=null;
          while(scan.hasNextLine()){
            buildingName = scan.nextLine();
            publicBuilding.add(new BuildingType(buildingName,buildingName));
          }
        }catch(Exception e){}
        return publicBuilding.toArray(new BuildingType[0]);
      }
      
      public MapEditPanel getMapPanel(){
         return mapPanel;
      }
   }
   /** 右下角的顯示視窗 */
   protected class MapSettingPanel extends JPanel implements StatusUpdatable{
      JTextArea me;
      public MapSettingPanel(){
         super(new BorderLayout());
         me = new JTextArea();
         add(me, BorderLayout.CENTER);
         me.setEditable(false);
         me.setTabSize(2);
      }
      public void updateStatus(){
         if(getSelectedObj() instanceof StatusViewable){
            ((StatusViewable) getSelectedObj()).viewStatus(this);
         }else if(me != null)
            me.setText("");
      }
      public void updateStatus(EditUseMap e_map){
         me.setText("");
         me.append(e_map + "\n");
         me.append("#of Streets: " + e_map.getStreetArrayList().size() + "\n");
         for(int i = 0; i < e_map.getStreetArrayList().size(); i++){
            me.append("\t" + e_map.getStreetArrayList().get(i) + "\n");
         }
      }
      public void updateStatus(EditUseStreet e_street){
         me.setText("");
         me.append(e_street + "\n");
         me.append("#of Blocks: " + e_street.getBlockSize() + "\n");
         for(int i = 0; i < e_street.getBlockSize(); i++){
            me.append("\t" + e_street.getBlockByIndex(i) + "\n");
         }
         me.append("來源街道:\n");
         for(int i = 0; i < e_street.getStreetFromSize(); i++)
            me.append("\t" + e_street.getStreetFrom()[i] + "\n");
         me.append("出口街道:\n");
         for(int i = 0; i < e_street.getStreetToSize(); i++)
            me.append("\t" + e_street.getStreetTo()[i] + "\n");
      }
      public void updateStatus(EditUseBlock e_block){
         me.setText(""); //TODO 還沒寫
         me.append(e_block + "\n");
         me.append("Contains Land: " + e_block.getLand() + "\n");
         if(e_block.getShape() == null)
            me.append("還沒有決定地圖位置!!\n");
      }
      public void updateStatus(EditUseLand e_land){
         me.setText(""); //TODO 還沒寫
         me.append(e_land + "\n");
         BuildingType[] bt_arr = e_land.getAvailBuildingTypes();
         for(int i=0;i<bt_arr.length;i++)
           me.append("\t" + bt_arr[i] + "\n");
         if(e_land.getShape() == null)
            me.append("還沒有決定地圖位置!!\n");
      }
      public void updateStatus(EditUseLandPublic e_landPublic){
         me.setText(""); //TODO 還沒寫
         me.append(e_landPublic + "\n");
         me.append("\t" + e_landPublic.getBuilding() + "\n");
         if(e_landPublic.getShape() == null)
            me.append("還沒有決定地圖位置!!\n");
      }
   }

   //================= TreeSelectionListener ============
   public void valueChanged(TreeSelectionEvent e){
      selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
      if(selectedNode != null)
         selectedObj = selectedNode.getUserObject();
      updateAll();
   }
   public void showHideButtons(){
      if(fb[BUT_ADDBLOCK]!=null)
         fb[ BUT_ADDBLOCK ].setVisible(
               (selectedObj instanceof EditUseStreet)
               /*|| (selectedObj instanceof EditUseBlock) || (selectedObj instanceof EditUseLand)*/
               );
      if(fb[BUT_ADDSTREET]!=null)
         fb[ BUT_ADDSTREET ].setVisible(
               (selectedObj instanceof EditUseMap) 
               /*|| (selectedObj instanceof EditUseStreet) || (selectedObj instanceof EditUseBlock) || (selectedObj instanceof EditUseLand)*/
               );
      if(fb[BUT_SETNAME]!=null)
         fb[ BUT_SETNAME ].setVisible(
               (selectedObj instanceof EditUseMap) || (selectedObj instanceof EditUseStreet)
               );
      if(fb[BUT_ADDSTREETTO]!=null)
         fb[ BUT_ADDSTREETTO ].setVisible(
               (selectedObj instanceof EditUseStreet)
               );
      if(fb[BUT_REMOVESTREETTO]!=null)
         fb[ BUT_REMOVESTREETTO ].setVisible(
               (selectedObj instanceof EditUseStreet)
               );
      if(fb[BUT_CHANGEMAPBGIMG]!=null)
         fb[ BUT_CHANGEMAPBGIMG ].setVisible(
               (selectedObj instanceof EditUseMap)
               );
      if(fb[BUT_DELETESTREET]!=null)
         fb[ BUT_DELETESTREET ].setVisible(
               (selectedObj instanceof EditUseStreet)
               );
      if(fb[BUT_DELETEBLOCK]!=null)
         fb[ BUT_DELETEBLOCK ].setVisible(
               (selectedObj instanceof EditUseBlock)
               );
      if(fb[BUT_CHANGELAND]!=null)
         fb[ BUT_CHANGELAND ].setVisible(
               (selectedObj instanceof EditUseLand||selectedObj instanceof EditUseLandPublic)
               );
      
   }
   public DefaultMutableTreeNode getSelectedNode(){
      return selectedNode;
   }
   public Object getSelectedObj(){
      return selectedObj;
      /*
         if(getSelectedNode()!=null)
         return getSelectedNode().getUserObject();
         return null;
         */
   }
   public void updateTree(DefaultMutableTreeNode node){
      tree.setSelectionPath(new TreePath(node.getPath()));
      tree.updateUI();
   }
   public void updateTree(){
      tree.updateUI();
   }
   public void clearTree(){
      tree.clearSelection();
      selectedNode = null;
      selectedObj = null;
      showHideButtons();
      tree.updateUI();
   }
   /** 幫 Street 建立一條 Path2D, 並寫入 MapData 當中 */
   public void makeRoutes(EditUseStreet s){
      makeRoutes();
   }
   public void makeRoutes(/*EditUseStreet street*/){
      Street[] sr = map.getStreets();
      for(int si = 0; si < sr.length; si++){
         EditUseStreet street = (EditUseStreet) sr[si];
         Shape s = street.getShape();
         if(s != null){
            getMapData().allShapes.remove(s);
            getMapData().hashmap.remove(s);
         }
         // Routes 是一個 Path2D
         Path2D p = new Path2D.Double();
         boolean flagPathComplete = true;
         double lastx=-1, lasty=-1;
         for(int i = 0; i < street.getBlockSize(); i++){
            s = ((EditUseBlock) street.getBlockByIndex(i)).getShape();
            if(s == null){
               flagPathComplete = false;
               break;
            }
            if(i>0)
               p.lineTo((s.getBounds().getX() + s.getBounds().getWidth()/2), (s.getBounds().getY() + s.getBounds().getHeight()/2));
            p.moveTo(s.getBounds().getX() + s.getBounds().getWidth()/2, s.getBounds().getY() + s.getBounds().getHeight()/2);
            lastx=s.getBounds().getX() + s.getBounds().getWidth()/2;
            lasty=s.getBounds().getY() + s.getBounds().getHeight()/2;
         }
         if(flagPathComplete && street.getBlockSize() > 0){
            for(int i = 0; i < street.getStreetToSize(); i++){
               Street nxt = street.getStreetTo()[i];
               if(nxt.getBlockSize() > 0){
                  s = ((EditUseBlock) nxt.getBlockByIndex(0)).getShape();
                  if(s != null){
                     p.lineTo((s.getBounds().getX() + s.getBounds().getWidth()/2), (s.getBounds().getY() + s.getBounds().getHeight()/2));
                     p.moveTo(lastx, lasty);
                  }
               }
            }
         }

         getMapData().allShapes.add(p);
         getMapData().hashmap.put(p, street);
         street.setShape(p);
      }
   }
   /** 讀取地圖時要設定地圖 */
   public void loadMap(File file){
      try{
         ObjectInputStream ois = new ObjectInputStream( new FileInputStream( file ));
         map = (EditUseMap) ois.readObject();
         //GameTool.setMap(map)
         ois.close();
         tree = new MapTree(this);
         treePane = new JScrollPane(tree);
         treePanel.removeAll();
         treePanel.add(treePane, BorderLayout.CENTER);
         loadBackgroundImage(getMapData().bgImgSrc);
         mapFileName = file.getName();
         setTitle(mapFileName);
      }catch(Exception e){
         System.err.println(e);
      }
   }
   /** 存地圖檔 */
   public void saveMap(File file){
      try{
         ObjectOutputStream oos = new ObjectOutputStream( new FileOutputStream( file ));
         oos.writeObject(map);
         oos.close();
         //Save MapInfo files 這code看了有點不舒服orz
         String pathname = file.getParent();
         String fn = file.getName();
         String fnew = "";
         for(int i = fn.length()-1; i >= 0; i--){
            if(fn.charAt(i) == '.'){
               fnew = fn.substring(0, i);
               break;
            }
         }
         pathname = pathname + File.separator + fnew + ".info";
         oos = new ObjectOutputStream( new FileOutputStream( pathname ));
         oos.writeObject( new MapInfo(map) );
         oos.close();
         //Save MapInfo files end

         mapFileName = file.getName();
         setTitle(mapFileName);
      }catch(Exception e){
         System.err.println(e);
      }
   }

   /** 畫面右邊的樹狀元件 */
   protected class MapTree extends JTree implements KeyListener, MouseListener, MouseMotionListener{
      DefaultMutableTreeNode root;
      DefaultMutableTreeNode bt_root;
      public MapTree(MapEditor me){
         super(new DefaultMutableTreeNode("Root"));
         this.addKeyListener(this);
         addTreeSelectionListener(me);
         setDragEnabled(false); //自己寫Drag
         root = (DefaultMutableTreeNode) getPathForRow(0).getLastPathComponent();
         init(root);
         /*for(int i = 0; i < getRowCount(); i++){
         //System.out.println("here " + i);
         expandRow(i);
         }*/
         this.addMouseListener(this);
         this.addMouseMotionListener(this);
         expandRow(0);
      }
      public void updateUI(){
         updateAll();
         super.updateUI();
      }
      /** 初始化，展開一株樹 */
      public void init(DefaultMutableTreeNode x){
         DefaultMutableTreeNode node = new DefaultMutableTreeNode(map);
         x.add(node);
         init(node, map);
         bt_root = new DefaultMutableTreeNode("Resource: BuildingTypes");
         x.add(bt_root);
         appendAllBuildingTypeResources(bt_root);

      }
      public void init(DefaultMutableTreeNode x, Map map){
         Street[] sarr = map.getStreets();
         for(int i = 0; i < sarr.length; i++){
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(sarr[i]);
            x.add(node);
            init(node, sarr[i]);
         }
      }
      public void init(DefaultMutableTreeNode x, Street s){
         for(int i = 0; i < s.getBlockSize(); i++){
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(s.getBlockByIndex(i));
            x.add(node);
            init(node, s.getBlockByIndex(i));
         }
      }
      public void init(DefaultMutableTreeNode x, Block block){
         EditUseBlock b = (EditUseBlock) block;
         if(b.getLand() != null){
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(b.getLand());
            x.add(node);
            if(b.getLand() instanceof EditUseLand)
              init(node, (EditUseLand)b.getLand());
            else if(b.getLand() instanceof EditUseLandPublic)
              init(node, (EditUseLandPublic)b.getLand());
         }
      }
      public void init(DefaultMutableTreeNode x, EditUseLand l){
      //public void init(DefaultMutableTreeNode x, Land land){
      //   EditUseLand l = (EditUseLand) land;
         l.resetIDs();
         ArrayList<BuildingType> bt_arr = l.getBuildingTypes();
         for(int i = 0; i < bt_arr.size(); i++){
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(bt_arr.get(i));
            x.add(node);
            //need to initial BuildingTypes? (不太需要吧:D)
         }
      }
      public void init(DefaultMutableTreeNode x, EditUseLandPublic l){
         l.resetIDs();
         x.add(new DefaultMutableTreeNode(l.getBuilding()));
      }
      DefaultMutableTreeNode copyingNode = null;

      //==================== KeyListener ========================
      /** 原來已經有內建的 KeyListener 了 XD 這個是額外功能 XD */
      public void keyPressed(KeyEvent e){
         int r = getMinSelectionRow();
         if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
            clearTree();
         }else if(e.getKeyCode() == KeyEvent.VK_SPACE){
            if(r<0) return;
            if(isExpanded(r) == true)
               collapseRow(r);
            else
               expandRow(r);
         }else if(e.getKeyCode() == KeyEvent.VK_C && e.getKeyModifiersText(InputEvent.CTRL_MASK).equals("Ctrl")){
            //Ctrl+C 複製
            if(r<0) return;
            copyingNode = (DefaultMutableTreeNode) getPathForRow(r).getLastPathComponent();
            setEditorTitle( getMapFileName() + " Copied: " + copyingNode);
         }else if(e.getKeyCode() == KeyEvent.VK_V && e.getKeyModifiersText(InputEvent.CTRL_MASK).equals("Ctrl")){
            if(r<0) return;

         }
      }
      public void keyReleased(KeyEvent e){}
      public void keyTyped(KeyEvent e){}

      int offsetX = 0, offsetY = 0;
      boolean dragging = false;
      boolean dragging_street = false;
      DefaultMutableTreeNode draggingNode = null;
      Rectangle droppingBounds = null;
      Rectangle draggingBounds = null;
      //=================== Mouse Listener =====================
      public void mouseClicked(MouseEvent e){}
      synchronized public void mousePressed(MouseEvent e){
         //if(e.getMouseModifiersText(MouseEvent.CTRL_DOWN_MASK).equals("Ctrl") == false) return;
         int x = e.getX(), y = e.getY();
         TreePath tp = getPathForLocation(x, y);
         if(tp == null) return;
         DefaultMutableTreeNode node = (DefaultMutableTreeNode) tp.getLastPathComponent();
         if(node == null) return;         
         Object o = node.getUserObject();
         if(o instanceof BuildingType){
            dragging = true;
            draggingNode = node;
            draggingBounds = getRowBounds( getRowForLocation(x, y) );
            offsetX = x - (int)draggingBounds.getX();
            offsetY = y - (int)draggingBounds.getY();
            return;
           //另外如果是 Street/ Block/ Land 的話也考慮可以 Dragging 
         } 
         
         //資料夾(整條street)         
         if(node.getUserObject() instanceof String){
           TreeNode leafnode=node.getFirstChild();
           if(leafnode == null) return;         
           o = ((DefaultMutableTreeNode)leafnode).getUserObject();
           if(o instanceof BuildingType){
              dragging_street = true;
              draggingNode = node;
              draggingBounds = getRowBounds( getRowForLocation(x, y) );
              offsetX = x - (int)draggingBounds.getX();
              offsetY = y - (int)draggingBounds.getY();
           } 
         }
      }
      public void mouseReleased(MouseEvent e){
         if(dragging){
            int x = e.getX(), y = e.getY();
            TreePath tp = getPathForLocation(x, y);
            if(tp != null){
               DefaultMutableTreeNode droppingNode = (DefaultMutableTreeNode) tp.getLastPathComponent();
               Object d = droppingNode.getUserObject();
               Object o = draggingNode.getUserObject();
               if(o instanceof BuildingType && d instanceof EditUseLand){
                  //新增一種 BuildingType 到 Land 裡面
                  EditUseLand e_land = (EditUseLand) d;
                  BuildingType b = (BuildingType) o;
                  e_land.addBuildingType(b);
                  DefaultMutableTreeNode node = new DefaultMutableTreeNode(b);
                  droppingNode.add(node);
                  updateTree();
               }
            }
            dragging = false;
            draggingNode = null;
            draggingBounds = null;
            droppingBounds = null;
            repaint();
         }else if(dragging_street){ //處理整條街
            int x = e.getX(), y = e.getY();
            TreePath tp = getPathForLocation(x, y);
            if(tp != null){
               DefaultMutableTreeNode droppingNode = (DefaultMutableTreeNode) tp.getLastPathComponent();
               Object d = droppingNode.getUserObject();
               if((d instanceof EditUseLand)){
                 fillAllElement(draggingNode,droppingNode);
                 updateTree();
               }else if(d instanceof EditUseStreet){
                 Enumeration children = droppingNode.children();
                 while(children.hasMoreElements()){
                   DefaultMutableTreeNode block=(DefaultMutableTreeNode)children.nextElement();
                   DefaultMutableTreeNode land=block.getFirstLeaf();
                   
                   fillAllElement(draggingNode,land);
                 }
                 updateTree();
               }
            }
            dragging_street = false;
            draggingNode = null;
            draggingBounds = null;
            droppingBounds = null;
            repaint();
         }
      }
      
      //把每個在draggingNode裡面的buildingType增加到land
      private void fillAllElement(DefaultMutableTreeNode draggingNode,DefaultMutableTreeNode droppingNode){
        Enumeration children = draggingNode.children();
        Object o = null;
        Object d = droppingNode.getUserObject();
        while(children.hasMoreElements()){
            o = ((DefaultMutableTreeNode)children.nextElement()).getUserObject();
            if(o instanceof BuildingType && d instanceof EditUseLand){
            //新增一種 BuildingType 到 Land 裡面
            EditUseLand e_land = (EditUseLand) d;
            BuildingType b = (BuildingType) o;
            e_land.addBuildingType(b);
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(b);
            droppingNode.add(node);            
          }
        }
      }
      
      public void mouseEntered(MouseEvent e){}
      public void mouseExited(MouseEvent e){}
      //=================== MouseMotionListener =================
      public void mouseDragged(MouseEvent e){
         if(dragging||dragging_street){
            int x = e.getX(), y = e.getY();
            draggingBounds.setLocation(x-offsetX, y-offsetY);
            TreePath tp = getPathForLocation(x, y);
            int r = getRowForLocation(x, y);
            if(r >= 0){
               droppingBounds = getRowBounds(r);
               this.repaint();
            }
         }
      }
      public void mouseMoved(MouseEvent e){}
      public void paint(Graphics g){
         super.paint(g);
         if(dragging||dragging_street){
            if(draggingBounds != null){
               g.setColor( new Color(99, 99, 255, 100) );
               ((Graphics2D) g).fill( draggingBounds );
            }
            if(droppingBounds != null){
               g.setColor( new Color(0, 0, 155, 100) );
               ((Graphics2D) g).draw( droppingBounds );
            }
         }
      }
   }
   /** 畫面更新 */
   public void updateAll(){
      if(getSettingPanel() != null)
         getSettingPanel().updateStatus();
      if(getMapPanel() != null)
         getMapPanel().repaint();
      showHideButtons();
   }

   /** TODO: 這裡要載入所有能用的 BuildingType 資源, 理論上將會是分門別類的. */
   public void appendAllBuildingTypeResources(DefaultMutableTreeNode bt_root){
      try{
        Scanner sflc = new Scanner( new FileInputStream("data/streetfilelist.txt"));
        String streetName = "testing";
        while(sflc.hasNextLine()){
          streetName =sflc.nextLine();
          Scanner sc = new Scanner( new FileInputStream("data/street/"+streetName+".txt"));
          DefaultMutableTreeNode streetNode=new DefaultMutableTreeNode(streetName);
          bt_root.add(streetNode);
          String fileName = "testing";   
          while(sc.hasNextLine()){
            fileName =sc.nextLine();
            streetNode.add(new DefaultMutableTreeNode(new BuildingType(fileName, "BuildingSmall")));
            System.out.println("增加\t"+streetName+"\t"+fileName);
          }         
        }
      }catch(Exception e){
        e.printStackTrace();
        System.err.println(e);
      }
   }
   public void setEditorTitle(String str){
      setTitle(str);
   }
   public String getEditorTitle(){
      return getTitle();
   }
   public static void main(String[] args){
      MapEditor me = new MapEditor(new EditUseMap());
   }
}

