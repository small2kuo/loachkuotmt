package proj.util.editor;

public interface StatusUpdatable{
	public void updateStatus(EditUseMap e_map);
	public void updateStatus(EditUseStreet e_street);
	public void updateStatus(EditUseBlock e_block);
	public void updateStatus(EditUseLand e_land);
	public void updateStatus(EditUseLandPublic e_landPublic);
}
